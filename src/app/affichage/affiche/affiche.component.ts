import { Component, OnInit } from '@angular/core';
import { ListService, AladinService, ObservableserviceService } from 'src/app/core';
import { decompress } from 'compress-json';
@Component({
  selector: 'app-affiche',
  templateUrl: './affiche.component.html',
  styleUrls: ['./affiche.component.scss']
})
export class AfficheComponent implements OnInit {
  disps:any;
  CacheCrea=false
 cacheClothes=true;
 hideprice:Boolean=true
 hidepromo:Boolean=false
list_of_prod:any
 details:any={};
 showdetail=false;
  url="/editor/disps/";
  imagepreview:any;
  showchoices=false;
  text="Je veux imprimer ma créa!!!";
  estbache=false;
  estvinyle=false;
  estkake=false;
  estmicro=false;
  data_prod:any;
  list:any;
  types=["Bache","Kakémono","micro-perforé","Vynile"];
  myfile="myfile"
  data="Grand format : l'impression grand format est une impression réalisée sur de grands supports tels que: le kakemono, la bâche, le vinyle et le micro-perforé. La qualité de l'impression est meilleure avec des couleurs riches et vibrantes et une superbe reproduction des images."
  constructor( private l :ListService, private uplod:AladinService,private o:ObservableserviceService) { }
 affichage=true
 importcrea=false
 editor=false
  ngOnInit(): void {
    this.l.getDesigns("affichages").subscribe(
      res=>{
      this.list_of_prod=res
      console.log( this.list_of_prod)
      if(this.list_of_prod.status){
        this.list_of_prod=this.list_of_prod.data
        for (let item of this.list_of_prod){
          this.list_of_prod[this.list_of_prod.indexOf(item)].design_title= JSON.parse(item.design_title);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls= JSON.parse(item.urls);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].urls)
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=JSON.parse(item.product_design_data);
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data)

        }

      }
      },
      err=>{
        console.log(err)
      }
    );

    this.o.d_m.subscribe(res=>{
      if(res){
        this.editor=false

      }
    })
  this.uplod.ShowEditor.subscribe(res=>{
    console.log(res,5635)
    if(res.data.show){
      this.data_prod=res.data;
      this.editor=res.data.show;
      this.list= res.list;
    }
      

    

  })
  }
  showimportcrea(){
  this.affichage=false;
  this.importcrea=true;
  }

  changeComponent(value:boolean){
    this.affichage=value;
    this.importcrea=!value;
    this.estvinyle=false;
    this.estbache=false;
    this.estkake=false;
    this.estmicro=false;
    this.text="Je veux imprimer ma créa...!!!"
  }


  Upload(event:any){
   
    let file =event.target.files[0]
    if(!this.uplod.UpleadImage(file)){
    const reader = new FileReader();
  reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(file);
   this.showimportcrea()
 
    console.log(event)
  }else{
    
  }
  }


  showmechoices(){
  this.showchoices=true;
  }

  OnclickVinyle(){
    if(this.estvinyle==false){
      this.text="J'importe mon visuel de vinyle";
      this.showchoices=true;
      this.estmicro=false;
      this.estbache=false;
      this.estkake=false;
      this.estvinyle=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  
  }

  OnclickMicro(){
    if(this.estmicro==false){
      this.text="J'importe mon visuel de micro-perforé";
      this.showchoices=true;
      this.estvinyle=false;
      this.estbache=false;
      this.estkake=false;
      this.estmicro=true
    
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  
  }

  OnclickKake(){
    if(this.estkake==false){
      this.text="J'importe mon visuel de kakémono";
      this.showchoices=true;
      this.estvinyle=false;
      this.estbache=false;
      this.estmicro=false;
      this.estkake=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  
  }

 
OnclickBache(){
  if(this.estbache==false){
    this.text="J'importe mon visuel de bache";
    this.showchoices=true;
    this.estvinyle=false;
    this.estmicro=false;
    this.estkake=false;
    this.estbache=true
  }else{
    this.showchoices=true;
    this.text="Je veux imprimer ma créa!!";

  }

}

showDetails(data:any){
  console.log(data)
  Object.assign(data,{show:true})
  this.details=data
  this.affichage=false
  this.importcrea=false;
  
 }
 


}
