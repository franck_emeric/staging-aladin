import { SharedModule } from './../shared/shared.module';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AffichageRoutingModule } from './affichage-routing.module';
import { AfficheComponent } from './affiche/affiche.component';
import { InsertCreaModule } from '../insert-crea/insert-crea.module';
import { SharededitorModule } from '../sharededitor/sharededitor.module';


@NgModule({
  declarations: [
    AfficheComponent,
  ],
  imports: [
    CommonModule,
    AffichageRoutingModule,
    SharedModule,
    InsertCreaModule,
    SharededitorModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AffichageModule { }
