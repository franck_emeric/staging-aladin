import { SharedModule } from '../shared';
import { CommonModule } from '@angular/common';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PrintedRoutingModule } from './printed-routing.module';
import { ListprintComponent } from './listprint/listprint.component';
import { InsertCreaModule } from '../insert-crea/insert-crea.module';
import { SharededitorModule } from '../sharededitor/sharededitor.module';



@NgModule({
  declarations: [
    ListprintComponent,
   
  ],
  imports: [
    CommonModule,
    PrintedRoutingModule,
    SharedModule,
    InsertCreaModule,
    SharededitorModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class PrintedModule { }
