import { Component, OnInit } from '@angular/core';
import { HttpService, ListService, ObservableserviceService } from 'src/app/core';
import { AladinService } from 'src/app/core';
import {decompress} from "compress-json"

@Component({
  selector: 'app-listprint',
  templateUrl: './listprint.component.html',
  styleUrls: ['./listprint.component.scss']
})
export class ListprintComponent implements OnInit {
packs:any;
shw=true
propacks:any={}
uploadedurl:any;
details:any={};
crea=false;
showchoices=false;
estsac=false;
estsachet=false;
models:any
produit:any=[]
hide=false
text="Je veux imprimer ma créa !!"
hideprice:Boolean=true
hidepromo:Boolean=false
list_of_prod:any
list_dt:any;
dt:any;
editor=false;
jday= new Date().getDay()

  constructor(private l:ListService, private uplod: AladinService, private http:HttpService,private o:ObservableserviceService) { }

  ngOnInit(): void {
    if(this.jday==5){
      this.hideprice=false
      this.hidepromo=true
    }

    this.uplod.ShowEditor.subscribe(res=>{
     if(res.data.show){
       this.editor= true;
       this.list_dt= res.list;
       this.dt= res.data;
     }
    })
    
    this.o.p_m.subscribe(res=>{
      if(res){
        this.editor=false
      }
    })

    this.l.getDesigns("emballages").subscribe(
      res=>{
      this.list_of_prod=res
      if(this.list_of_prod.status){
        this.list_of_prod=this.list_of_prod.data
        for (let item of this.list_of_prod){
          this.list_of_prod[this.list_of_prod.indexOf(item)].design_title= JSON.parse(item.design_title);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls= JSON.parse(item.urls);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].urls)
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=JSON.parse(item.product_design_data);
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data)

        }

      }
      },
      err=>{
        console.log(err)
      }
    )



  }


  

  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }


   OnclickSac(){
    if(this.estsac==false){
      this.text="J'importe mon visuel de sac";
      this.showchoices=true;
      this.estsachet=false;
      this.estsac=true
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
  

   }

displaychoices(){
  this.showchoices=true;
}

   OnclickSachet(){
    if(this.estsachet==false){
      this.text="J'importe mon visuel de sachet";
      this.showchoices=true;
      this.estsachet=true;
      this.estsac=false
    }else{
      this.showchoices=true;
      this.text="Je veux imprimer ma créa!!";
  
    }
   }

   ChangeComponent(value:boolean){
     this.shw=value;
     this.showchoices=false;
     this.text="Je veux imprimer ma créa !!";
     this.estsac=false;
     this.estsachet=false;
   }

   Upload(event:any){
    let file =event.target.files[0]
    if(!this.uplod.UpleadImage(file)){
    const reader = new FileReader();
  reader.onload = () => {
  
   this.uploadedurl = reader.result;
   
   };
   reader.readAsDataURL(file);

   this.show()
    console.log(event)
  }else{
    
  }
  }

   Changecomponent(value:boolean){
    this.shw=value;
 }

show(){
  this.crea=true;
  this.shw=false;
}
  
letchange(value:boolean){
  this.shw=value
  this.crea=!value
}

showDetails(data:any){
  Object.assign(data,{show:true,aladin:false})
  this.details=data
  this.crea=false;
  this.shw=false;
  this.hide=true
  
 }
Showaladin(data:any){
  Object.assign(this.details,data)  
}

}
