import { Component, OnInit } from '@angular/core';
import { ListService ,LocalService, AladinService, HttpService} from 'src/app/core';
import {decompress} from "compress-json"
import { ObservableserviceService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
var $ = require("jquery");
@Component({
  selector: 'app-vetement',
  templateUrl: './vetement.component.html',
  styleUrls: ['./vetement.component.scss']
})
export class VetementComponent implements OnInit {
 cltobj:any=[]
 file:any
 imagepreview:any
 CacheCrea=false
 cacheClothes=true;
 details:any={};
 showdetail=false;
 models:any
 obj:any=[]
 objf:any=[]
 cart=false;
 canvas:any
 products:any
 produit:any=[]
 prod:any={}
 prodcloths:any={}
 hideprice:Boolean=true
 hidepromo:Boolean=false
list_of_prod:any
jday= new Date().getDay()
urls:any=[]
data_prod=[]
my_list:any
editor=false
 data="Vêtement : Aladin vous propose des vêtements personnalisés de très bonne qualité avec des finitions qui respectent vos goûts. Vous serez fière de les porter partout quel que soit le lieu et l’événement."
 constructor(private p:ListService,private local:LocalService, private uplod:AladinService, private http:HttpService,private O:ObservableserviceService) { }

  ngOnInit(): void {

    this.O.search.subscribe(res=>{
      this.showDetails(res)
    })
  this.uplod.ShowEditor.subscribe(
    res=>{
      if(res.data.show){
        this.data_prod=res.data;
       this.my_list=res.list;
       this.editor= res.data.show;
      }
    }
  )

  this.O.c_m.subscribe(res=>{
    if(res){
      this.editor=false;
    }
  })


   
    if(this.jday==5){
      this.hideprice=false
      this.hidepromo=true
    }
    this.p.getDesigns("vetements").subscribe(
      res=>{
      this.list_of_prod=res
      console.log(this.list_of_prod)
      if(this.list_of_prod.status){
        this.list_of_prod=this.list_of_prod.data
        for (let item of this.list_of_prod){
          this.list_of_prod[this.list_of_prod.indexOf(item)].design_title= JSON.parse(item.design_title);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls= JSON.parse(item.urls);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].urls)
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=JSON.parse(item.product_design_data);
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data)

        }

      }
      },
      err=>{
        console.log(err)
      }
    )
    
    
  }

  

 Upload(event:any){
   let file =event.target.files[0]
   if(!this.uplod.UpleadImage(file)){
   const reader = new FileReader();
 reader.onload = () => { 
  this.imagepreview = reader.result;
  
  };
  
  reader.readAsDataURL(file);
  this.affichecrea()
 
   console.log(event)
   }else{
     
   }
 }

showDetails(data:any){
 
 Object.assign(data,{show:true})
 this.details=data
 this.CacheCrea=false
 this.cacheClothes=false;
 
}

affichecrea(){
  this.CacheCrea=true
  this.cacheClothes=false
}


getComponent(value:any){
  this.cacheClothes=value;
  this.CacheCrea=!this.CacheCrea;

}

ChangeComponent(value:boolean){
  this.cacheClothes=value
}

}
