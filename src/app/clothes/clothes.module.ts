import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { ClothesRoutingModule } from './clothes-routing.module';
import { SharedModule } from '../shared';
import { VetementComponent } from './vetement/vetement.component';
import { InsertCreaModule } from '../insert-crea/insert-crea.module';
import { SharededitorModule } from '../sharededitor/sharededitor.module';

@NgModule({
  declarations: [VetementComponent],
  imports: [
    ClothesRoutingModule,
    SharedModule,
    InsertCreaModule,
    SharededitorModule
  ],
  
  schemas:[CUSTOM_ELEMENTS_SCHEMA]

})
export class ClothesModule { }
