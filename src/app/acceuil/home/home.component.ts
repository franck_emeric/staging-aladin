import { Component, OnInit ,OnChanges, OnDestroy} from '@angular/core';
import { AladinService,LocalService, ObservableserviceService } from 'src/app/core';
declare var require: any;
var $ = require("jquery");
declare  var require:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit ,OnChanges,OnDestroy{
  name:any;
  id:any;
  hide=false
   hidepack=false
   hidebody=true
   details:any={}
  show: boolean = true;
  private subscription:any
  data={}
  cart=false
  editor=false;
  sho=true
  fromhome=true
  list:any
  constructor(private aladin:AladinService,private l:LocalService,private o:ObservableserviceService) { }

  ngOnInit(): void {
    this.aladin.ShowEditor.subscribe(mess=>{
      if(mess.data.show){
        this.list=mess.list
        this.data=mess.data;
        this.sho=!mess.data.show
        this.show= !mess.data.show;
        this.editor= mess.data.show;
        this.cart= !mess.data.show;

        $(document).ready(function () {
          $('html, body').animate({
              scrollTop: $('#editor').offset().top
          }, 'slow');
          
      });
      }
     
      
  })
this.o.home.subscribe(h=>{
  this.editor=false
  this.cart=false
  this.show= true

})

   this.l.activatecart.subscribe(res=>{
     if(res){
       this.sho=res;
      this.cart=res;
      this.show=!res;
      this.editor=!res;
      $(document).ready(function () {
        $('html, body').animate({
            scrollTop: $('#cart').offset().top
        }, 'slow');
    });

     }else{
      this.cart=res
      this.sho=!res
      this.show=!res;
      this.editor=res;
      
     }

   })




  }

ngOnChanges(){
 

}


ngOnDestroy(){
  this.subscription.unsubscribe()
}
  toggleSpinner(){
    this.show = !this.show;   
  }


afficher(){
  
}
}
