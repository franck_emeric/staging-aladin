import { Component, OnInit } from '@angular/core';
import { AladinService, ListService, ObservableserviceService } from 'src/app/core';
import { decompress } from 'compress-json';
import scrollIntoView from 'smooth-scroll-into-view-if-needed'

@Component({
  selector: 'app-listgadget',
  templateUrl: './listgadget.component.html',
  styleUrls: ['./listgadget.component.scss']
})
export class ListgadgetComponent implements OnInit {
gadgets:any;
details:any={}
list_of_prod:any
list_gadget:any;
editor=false;
editor_data:any; 

  constructor(private l :ListService, private uplod:AladinService,private o:ObservableserviceService) { }

  ngOnInit(): void {
    this.l.getDesigns("gadgets").subscribe(
      res=>{
      this.list_of_prod=res
      console.log( this.list_of_prod)
      if(this.list_of_prod.status){
        this.list_of_prod=this.list_of_prod.data
        for (let item of this.list_of_prod){
          this.list_of_prod[this.list_of_prod.indexOf(item)].design_title= JSON.parse(item.design_title);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls= JSON.parse(item.urls);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].urls)
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=JSON.parse(item.product_design_data);
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data)

        }

      }
      console.log(this.list_of_prod)
      },
      err=>{
        console.log(err)
      }
    )

   this.uplod.ShowEditor.subscribe(
     res=>{
       if(res.data.show){
         this.editor_data=res.data;
         this.list_gadget= res.list;
         this.editor=res.data.show;
       }
     }
   )

   this.o.g_m.subscribe(
     res=>{
       if(res){ 
         this.editor=false;
       }
     }
   )
  }

  
  View(){
    let view:any = document.getElementById('view');
    scrollIntoView(view,{behavior:"smooth"})
   }

 

}
