import { Component, OnInit,Input,ViewChild,OnChanges,SimpleChanges } from '@angular/core';
import { fabric } from 'fabric';
import { AladinService,CartService,ListService, LocalService, ObservableserviceService } from 'src/app/core';
import Swal from 'sweetalert2';
import { ColorEvent } from 'ngx-color';
import { ContextMenuComponent } from 'ngx-contextmenu';
import {decompress,compress} from 'compress-json';

@Component({
  selector: 'app-aladin',
  templateUrl: './aladin.component.html',
  styleUrls: ['./aladin.component.scss']
})
export class AladinComponent implements OnInit {
@Input() data:any;
 default=true;
 @ Input()user:any
  canvas:any
  pallette=false;
  produite=true;
  modeles=false
  formes=false
  design=false
  
  passive=true
  divider=true
  cpt=0;
  usr:any;
  cptr=0;
  isRedoing=false;
  state:any=[]
  text_width=30
  cart_items=0;
  textalign=["left","center","justify","right"]
  url1="/assets/images/indx.png";
  text=""
  list:any
  public items = { name: 'John', otherProperty: 'Foo' }
  canvaspages:any=[]
   currentItem:number|any;
   custom:any=[]
   @Input() models:any;
    Items:any
   backUrl=[{url:"/assets/images/kakemono.jpg"},{url:"/assets/images/kake.jpeg"},{url:"/assets/images/6.png"},{url:"/assets/images/7.png"},{url:"/assets/images/index.jpeg"},{url:"/assets/images/bgd.jpeg"},{url:"/assets/images/gd.jpg"},{url:"/assets/images/about.jpeg"},{url:"/assets/images/abt.jpeg"},{url:"/assets/images/abtt.jpeg"},{url:"/assets/images/gbd.jpeg"},{url:"/assets/images/aff.jpg"},{url:"/assets/images/card.jpg"},{url:"/assets/images/acrdv.jpeg"},{url:"/assets/images/c.jpeg"},{url:"/assets/images/flr.jpeg"},{url:"/assets/images/1.png"},{url:"/assets/images/2.png"},{url:"/assets/images/3.png"},{url:"/assets/images/4.png"}]
   colors=["blue","white","black","red","orange","yellow","green","#999999","#454545","#800080","#000080","#00FF00","#800000","#008080","#324161","#fab91a","brown"]
   shape=[`https://img.icons8.com/color/96/000000/address--v1.png`,`https://img.icons8.com/office/80/000000/facebook-new.png`,`https://img.icons8.com/color/96/000000/fragile.png`,`https://img.icons8.com/office/80/000000/linkedin-circled--v2.png`,`https://img.icons8.com/color/96/000000/delivery--v2.png`,`https://img.icons8.com/color/48/000000/shipped.png`,`https://img.icons8.com/color/96/000000/manual-handling.png`,`https://img.icons8.com/color/48/000000/warranty.png`,`https://img.icons8.com/flat-round/96/000000/warranty.png`,`https://img.icons8.com/color/96/000000/sale--v1.png`,`https://img.icons8.com/color/48/000000/shopaholic.png`] 
   fonts = [
   {name:"Flowerheart",url:"./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf"},
   {name:"HussarBold",url:"./assets/fonts/HussarBoldWebEdition-xq5O.otf"},
   {name:'Branda',url:"./assets/fonts/Branda-yolq.ttf"},
   {name:"MarginDemo",url:"./assets/fonts/MarginDemo-7B6ZE.otf"},
   {name:"Kahlil",url:"./assets/fonts/Kahlil-YzP9L.ttf"},
   {name:"FastHand",url:"./assets/fonts/FastHand-lgBMV.ttf"},
   {name:"BigfatScript",url:"./assets/fonts/BigfatScript-jE96G.ttf"},
   {name:"Atlane",url:"./assets/fonts/Atlane-PK3r7.otf"},
   {name:"HidayatullahDemo",url:"./assets/fonts/Bismillahscript-4ByyY.ttf"},
   {name:"Robus",url:"./assets/fonts/HidayatullahDemo-mLp39.ttf"},
   {name:"Backslash",url:"./assets/fonts/Robus-BWqOd.otf"},
   {name:"ChristmasStory",url:"./assets/fonts/ChristmasStory-3zXXy.ttf"},
  ];
  currentPage=""
  editor=true
 @ViewChild(ContextMenuComponent) public basicMenu:ContextMenuComponent | any
 @ViewChild(ContextMenuComponent) public PageMenu:ContextMenuComponent | any
  
 
 constructor(private aladin:AladinService,private o:ObservableserviceService,private l:ListService,private shoping_cart:LocalService,private cart_info:CartService) { 

  }

  ngOnInit(): void {
   
   this.default=false
   this.list= JSON.parse(this.models);
  this.canvas= new fabric.Canvas('aladin',{
      width:400,
      height:400,
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false,
    }); 
     this.init();
  }


homepage() {

  if(this.data.category_label==="affichages"){
    this.o.d_m.next(true)
    this.o.home.next(true)
  }
  if(this.data.category_label==="vetements"){
    this.o.c_m.next(true)
    this.o.home.next(true)

  }

  if(this.data.category_label==="imprimes"){
    this.o.pr_m.next(true)
    this.o.home.next(true)

  }
  if(this.data.category_label==="gadgets"){
    this.o.g_m.next(true)
    this.o.home.next(true)

  }

  if(this.data.category_label==="emballages"){
    this.o.p_m.next(true)
    this.o.home.next(true)

  }


}


  showpallette(){
    this.design=false
    this.pallette=true;
     this.produite=false;
     this.modeles=false
     this.formes=false
  }

  update_cart(){
    this.cart_info.cartUpdated.subscribe(
      cart_length=>{
          this.cart_items=cart_length
      
      }
    )
  }

  mouseActions=()=>{
    this.canvas.on("object:added",()=>{
 
      if(!this.isRedoing){
        this.state = [];
      }
      this.isRedoing = false;
     
      
    })
  
    this.canvas.on("object:created",(e:any)=>{
     this.state.push(this.canvas.getActiveObject())
  

    })

    this.canvas.on("mouse:down",(e:any)=>{
      if(this.currentItem){
        this.currentPage=this.canvas.toDataURL({format:"image/jpeg"})
        this.canvaspages[this.currentItem-1].url= this.currentPage 
         this.canvaspages[this.currentItem-1].obj= this.canvas.toJSON()
        this.canvas.renderAll()
        this.canvas.requestRenderAll()
  
      }
    })


   this.canvas.on("mouse:move",(e:any)=>{
    if(!this.isRedoing){
      this.state = [];
    }
    this.isRedoing = false;
     if(this.currentItem){
      this.currentPage=this.canvas.toDataURL({format:"image/jpeg"})
      this.canvaspages[this.currentItem-1].url= this.currentPage 
       this.canvaspages[this.currentItem-1].obj= this.canvas.toJSON()
      this.canvas.renderAll()
      this.canvas.requestRenderAll()

    }


   })
  
 
   this.canvas.on("after:render",(e:any)=>{
    if(this.canvas.getActiveObject()){
      if(this.canvas.getActiveObject().type==="textbox"){
        this.showpallette()
      }
       if(this.canvas.getActiveObject().type==="image") {
        this.showforme()
      }   
    }

   })
  }
  
duplicatepage(){
  this.canvaspages.push(
    {
      url:this.canvas.toDataURL("jpg"),
      id:this.canvaspages.length+1,
      obj:this.canvas.toJSON()
    }
  )
  this.currentItem=this.canvaspages[this.canvaspages.length-1].id
}

removePage(id:any){
  this.canvaspages.splice(id,1)
  if(this.canvaspages.length>0){

  }
}

  showforme(){
    this.design=false
    this.pallette=false;
     this.produite=false;
     this.modeles=false
     this.formes=!this.formes
  }
  showmodeles(){
    this.design=false
    this.pallette=false;
     this.produite=false;
     this.modeles=!this.modeles
     this.formes=false
  }
  showproduite(){
    this.design=false
    this.pallette=false;
     this.produite=!this.produite;
     this.modeles=false
     this.formes=false
  }
  showdesign(){
    this.design=!this.design
    this.pallette=false;
     this.produite=false;
     this.modeles=false
     this.formes=false
  }

 menupro=()=>{
   setTimeout(() => {
    let items = document.querySelectorAll('li');
    items.forEach(item => {
      item.addEventListener('click', (e) => {
      e.preventDefault()
         items.forEach(item => item.classList.remove('active'))
          item.classList.add('active')
        
      })
    })
   }, 100);
 }


 makeNewPage(){
  if(this.cptr==0){
    this.canvaspages.push({url:this.canvas.toDataURL({format:"image/jpeg"}),id:this.canvaspages.length+1,obj:this.canvas.toJSON()})
    this.canvas.clear();
    this.canvaspages.push({url:this.canvas.toDataURL({format:"image/jpeg"}),id:this.canvaspages.length+1,obj:this.canvas.toJSON()})
    this.cptr=this.cptr+1
    this.currentItem= this.canvaspages[this.canvaspages.length-1].id
  }
  else{
    this.canvas.clear();    
    this.canvaspages.push({url:this.canvas.toDataURL({format:"image/jpeg"}),id:this.canvaspages.length+1,obj:this.canvas.toJSON()})
    this.currentItem= this.canvaspages[this.canvaspages.length-1].id

  }

 }

load_custom(item:any){

  if(item.obj==null){
    this.setItem(item.url)
  }
  if(item.obj!=null){
  console.log(item.obj.objects)
    fabric.util.enlivenObjects(item.obj.objects, (objects:any)=> {
      objects.forEach((o:any)=>{
        this.canvas.add(o);

      })

      this.canvas.renderAll()
      this.canvas.requestRenderAll();
    },"fabric");


  }
}


init(){
  this.canvas.setWidth(this.canvas.width)
  this.canvas.setHeight(this.canvas.height) 
  this.mouseActions()
  this.menupro()
   this.Items=this.data
  this.update_cart()
  this.load()

  this.l.getCustom().subscribe(
    res=>{
      var rsp:any=res
      if(rsp.status){
        for(let item of rsp.data){
          item.urls= JSON.parse(item.urls)
          item.urls= decompress(item.urls)
          item.product_design_data=JSON.parse(item.product_design_data)
          item.product_design_data=decompress(item.product_design_data)
         
          for(let i=0;i<item.urls.length;i++){
            this.custom.push({url:item.urls[i],obj:item.product_design_data[i]})
          }
        }
      }
    }
  )

  var cart :any= this.shoping_cart.cart_items
  this.cart_items= cart.length

}


cart(){
  
  location.href ="/cart"

}

load(item:any=null){
   
  if(item!=null){
    this.data=item;
    this.canvas.setWidth(this.canvas.width)
    this.canvas.setHeight(this.canvas.height)
    this.canvaspages=[]
    this.canvas.loadFromJSON(this.data.product_design_data[0],()=>
    {
      
     for(let i=0;i<this.data.urls.length;i++){
       if(i==0){
        this.canvaspages.push({url:this.canvas.toDataURL("jpeg"),id:i+1,obj:this.canvas.toJSON()})
        this.currentItem=i+1
  
       }else{
        this.canvaspages.push({url:this.data.urls[i],id:i+1,obj:this.data.product_design_data[i]})
  
       }
  
     }
     
     this.canvas.renderAll.bind(this.canvas)
    });
  }
  
  if(item==null){
    this.canvas.setWidth(this.canvas.width)
    this.canvas.setHeight(this.canvas.height)
    this.canvas.loadFromJSON(this.data.product_design_data[0],(obj:any)=>
  {
   
   for(let i=0;i<this.data.urls.length;i++){
     if(i==0){
      this.canvaspages.push({url:this.canvas.toDataURL(),id:i+1,obj:this.canvas.toJSON()})
      this.currentItem=i+1

     }else{
      this.canvaspages.push({url:this.data.urls[i],id:i+1,obj:this.data.product_design_data[i]})

     }

   }
   
   this.canvas.requestRenderAll(this.canvas)
  });
  }
}

 changeBackground(url:any){
   if(this.canvas.backgroundImage){
    Swal.fire({
      title: 'vous etes sur le point de changer l\'arrière plan?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui, je veux le remplacer !',
      cancelButtonText:"Non"
    }).then((result) => {
      if (result.isConfirmed) {
        this.aladin.makeBackImage(url,this.canvas)
  
      }
    })
   }else{
    this.aladin.makeBackImage(url,this.canvas)

   }
 

 }

 reactivatePage(item:any){
  if(item.obj==null){
    this.aladin.loadpages(this.canvas,item.url).then(
     (res)=>{
       this.currentItem=item.id
       this.canvas.requestRenderAll()
     }
   );
  
  }else if(item.obj!=null){

   this.canvas.loadFromJSON(item.obj,()=>
   {
     this.currentItem=item.id;
     this.canvas.requestRenderAll()
   });

  }


   
}


 makeBackground(url:any){
   this.changeBackground(url)
 }

 ChangeItem(item:any){
  this.data=item;
  Swal.fire({
    title: 'vous etes sur le point de changer de modèle ',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Oui, je veux le remplacer !',
    cancelButtonText:"Non"
  }).then((result) => {
    if (result.isConfirmed) {
      this.load(item);
    }
  })
 }




 showMessage(param:any){
   console.log(param)
 }
 
 duplicate(){
  this.copy();
  this.paste()
}

copy(){
  this.aladin.copy(this.canvas)
}

paste(){
  this.aladin.paste(this.canvas)
}

setItem(url:any){
  this.aladin.makeItem(url,this.canvas)
}

textfont(item:any){
  let data= item.target.value
  this.aladin.textfont(Object.assign({},{name:data.substr(0,data.indexOf("*")),url:data.substr(data.indexOf("*")+1,data.length-1)}),this.canvas)
}


InputChange(){
  if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){
    if(this.cpt==0){
      this.text=this.canvas.getActiveObject().text+" "+ this.text;
      this.cpt=this.cpt+1
      this.canvas.getActiveObject().text= this.text;
      this.canvas.requestRenderAll();
    }else{
      if(this.text!=this.canvas.getActiveObject().text){
        this.canvas.getActiveObject().text= this.text;
        this.canvas.requestRenderAll();
      }
    
    }

  }else{  
    let text= new fabric.Textbox(this.text,{
      top:200,
      left:200,
      fill:"blue",
      fontSize:30,
      fontStyle:'normal',
      cornerStyle:'circle',
      selectable:true,
      borderScaleFactor:1,
      overline:false,
      lineHeight:1.5
    });
    this.canvas.add(text).setActiveObject(text);
    this.canvas.renderAll(text);
    this.canvas.requestRenderAll();
    this.canvas.centerObject(text);
  }
}


texteclor($event:ColorEvent){
  this.aladin.textcolor($event.color.hex,this.canvas);
  }


textwidth(){
  if(this.canvas.getActiveObject().text){
    console.log(this.canvas.getActiveObject())
    this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height+1})
    this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width+1})
    this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize+1})
    this.text_width= this.canvas.getActiveObject().fontSize
    this.canvas.renderAll()
  }
  
  }
  
  minus(){
    if(this.canvas.getActiveObject().text){
      console.log(this.canvas.getActiveObject())
      this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height-1})
      this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width-1})
      this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize-1})
      this.text_width= this.canvas.getActiveObject().fontSize
      this.canvas.renderAll()
    }
    
  }

  Redo(){
    if(this.state.length>0){
      this.isRedoing = true;
     this.canvas.add(this.state.pop());
    }
    
    }
    
    Undo(){
    if(this.canvas._objects.length>0){
      this.state.push(this.canvas._objects.pop());
      this.canvas.renderAll();
     }
  }
  sendBack(){
    this.aladin.sendBack(this.canvas)
  }
  sendForward(){
    this.aladin.sendForward(this.canvas)
  }
  
  makeBold(){
    this.aladin.bold(this.canvas)
  }
  makeItalic(){
    this.aladin.italic(this.canvas)
  }

  textAlign(val:any){
   this.aladin.textAlign(this.canvas,val)
  }

  save(){
    let url :any=[]
    let obj:any=[]
    console.log(this.data)
    for(let item of this.canvaspages){
      if(item.obj!=null){
        url.push(item.url),
        obj.push(item.obj)
      }
    }
    var data:Object={
       urls:url,
       obj:obj,
       type_product:"editor",
       pelicule: this.data.pelicule || null,
       format:this.data.format || null,
       bordure:this.data.bordure || null,
       type_impr: this.data.type_impr||null,
       t:this.Items.t,
       category:this.data.category_label,
       name:this.data.design_title,
      size:this.Items.size||null,
      dimension:this.data.dimension||null,
      qty:this.Items.qty,
      type:this.Items.type||null,
      size_type:this.Items.size_type||null,
      price:this.Items.price||null,
      vendor_email:this.data.admin_email,
      design:this.data.product_design_id,
    
    };

    try{
      data= compress(data)
      console.log(data)
      this.shoping_cart.adtocart(data).then((res)=>{
        Swal.fire(
          {
            icon:"success",
            text:"votre design a été placé dans le panier"
          }
        )
      }).catch((err)=>{
        console.log(err)
      })
     

    }catch(err){
      console.log(err)
    }

  }

  underline(){
    this.aladin.underline(this.canvas)
  }

  overline(){
    this.aladin.overline(this.canvas)
  }

  remove(){
    this.aladin.remove(this.canvas)
  }




}
