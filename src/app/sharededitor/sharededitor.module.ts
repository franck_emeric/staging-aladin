import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AladinComponent } from './aladin/aladin.component';
import { ContextMenuModule } from 'ngx-contextmenu';

import { ColorCircleModule } from 'ngx-color/circle'; 
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AladinComponent,
    
  ],
  exports:[
    AladinComponent,
  ],
  imports: [
    CommonModule,
    ColorCircleModule,
    FormsModule,
    ContextMenuModule.forRoot(),
    SharedModule
  ],


  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]

})
export class SharededitorModule { }
