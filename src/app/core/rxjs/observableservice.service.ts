import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ObservableserviceService {
  
subject= new Subject<{}>();
cloth=new Subject<{}>();
upload_cloth= new Subject<any>()
upload_gadget= new Subject<any>()
upload_print= new Subject<any>()
upload_pack= new Subject<any>()
upload_display= new Subject<any>()
show_button=new Subject<any>();
search=new Subject<any>()
detail= new Subject<{}>();
out_detail= new Subject<any>()
searchResult=new Subject<any>()
onload=new Subject<any>()
home= new Subject<boolean>();
cart = new Subject<boolean>()
c_m=new Subject<boolean>()
p_m=new Subject<boolean>();
pr_m=new Subject<boolean>();
d_m=new Subject<boolean>();
g_m=new Subject<any>();

om= new Subject<boolean>()
mtn = new Subject<boolean>()
  constructor() { }

  
newcartItem=()=>{
  let notify=new Subject<number>();
 let cart:any = localStorage.getItem("cart")
 cart = JSON.parse(cart)
  notify.next(cart.length+1)
}




}
