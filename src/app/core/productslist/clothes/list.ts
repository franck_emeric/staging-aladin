import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import {Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ListService {
url="cloths/items";

  constructor(private http:HttpClient) { }

setCode(data:any){
  return this.http.patch(environment.apiBaseUrl+'users/code',data)
}


triggerMouse(event:any){
  var evt = new MouseEvent("click", {
    view: window,
    bubbles: true,
    cancelable: true,
});
event.dispatchEvent(evt);

}







saveOrder(data:any):Observable<any>{
  return this.http.post<any>(environment.apiBaseUrl+"orders",data)
 
}
saveOrderProducts(data:any):Observable<any>{
  return this.http.post<any>(environment.apiBaseUrl+"orders/products",data)

}
getUserOrder(id:any):Observable<any>{
return this.http.get<any>(environment.apiBaseUrl+"orders/customers/"+id)
}

UpdateUser(id:any,data:any):Observable<any>{
  return this.http.put(<any>environment.apiBaseUrl+"users/"+id,data);
}


ChangePassword(data:any,id:any):Observable<any>{
  let url ="users/change/password/"+id;
  return this.http.put<any>(environment.apiBaseUrl+url,data);
}






getDesigns(param:any=null){
  if(param){
    return this.http.get(environment.baseUrlDev+"products/posted-designs/"+param)

  }else{
    return this.http.get(environment.baseUrlDev+"products/posted-designs")

  }
}

getCustom(){
  return this.http.get(environment.baseUrlDev+"products/custom")
}

getprinting=(category:any):Observable<any>=>{
   return this.http.get(environment.baseUrlDev+"products/printing/"+category)
}

getFormat=(category:any):Observable<any>=>
{
  return this.http.get(environment.baseUrlDev+"products/format/"+category)
}
getmode=(type:any):Observable<any>=>
{
  return this.http.get(environment.baseUrlDev+"products/mode/"+type)
}

getAdress():Observable<any>{
  return this.http.get(environment.apiBaseUrl+"orders")
}

getNotify(url:any){
  return this.http.get(url)
}

uploads(data:any){
   return this.http.post(environment.apiBaseUrl+"uploads",data)
}

}


 
