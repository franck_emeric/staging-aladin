import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError,BehaviorSubject } from 'rxjs';
import { tap ,catchError, filter, switchMap, take} from 'rxjs/operators';
import { TokenService } from '../tokens/token.service';{}
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private tokenservice:TokenService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.getAuthToken();
    const p_token= this.tokenservice.get_pay_token()
    
    if(token) {
      if(!req.url.includes('momo')&&!req.url.includes('orange')){
        req = req.clone({
          setHeaders: {"x-access-token": `${token}`}
       });
      }
   }

   if(p_token){
    if(req.url.includes('momo') && !req.url.includes("/token")){
      req = req.clone({
        setHeaders: {"authorization":"Bearer "+ `${p_token}`}
     });
    }

    if(req.url.includes('orange')){
      req = req.clone({
        setHeaders: {"authorization":"Bearer "+`${p_token}`}
     });
    }

   }


   return next.handle(req).pipe(
     tap(event=>{
     },
     err=>{
       if(!req.url.includes('api/v1/users/login') && err.status === 401){
         location.href="/"
         this.handle401Error(req,next)
       }
      
     }
     )
   )
  }


  getAuthToken():string {
    const token:any= this.tokenservice.getToken()
    return token
    }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
      if (!this.isRefreshing) {
        this.isRefreshing = false;
        this.refreshTokenSubject.next(null);
        const token = this.getAuthToken();
        if(token){
          if(!request.url.includes('api/v1/momo')&&!request.url.includes('api/v1/orange')){
            this.tokenservice.refresh(token).subscribe(
              res=>{
                this.isRefreshing = true;
                this.tokenservice.saveToken(res.access_token);
                this.refreshTokenSubject.next(res.access_token);
                return next.handle(this.addTokenHeader(request,res.access_token));
              },
              err=>{
                this.isRefreshing = false;    
                this.tokenservice.signOut();
                return throwError(err);
              }

            )
          }
        }
      }

      return this.refreshTokenSubject.pipe(
        filter(token => token !== null),
        take(1),
        switchMap((token) => next.handle(this.addTokenHeader(request, token)))
      );
    }

    private addTokenHeader(request: HttpRequest<any>, token: string) {
      return request.clone({ headers: request.headers.set("x-access-token", token) });
    }
  
}
