import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginData } from '../model';
import {map} from 'rxjs/operators'
import {HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
 url="users/login";
  constructor(private http:HttpClient) { }
  
  login(data:LoginData):Observable<any>{
    return this.http.post<any>(environment.apiBaseUrl+this.url,data).pipe(map(res=>res));

  }

  isAuthenticated():Boolean{
   let access:any= localStorage.getItem('access_token');
  if(access=="0"){
    return true;
  }
 return false;
 
  }


  
isauthaspartner(){
  let access:any= localStorage.getItem('access_token');
  if(access=="1"){
    return true;
  }
 return false;
}

  getUser(id=null){
    let t = localStorage.getItem('t');
    var headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': "Bearer "+t});
        const httpOptions = {
          headers: headers_object
        };

    return this.http.get(environment.apiBaseUrl+"users/"+id,httpOptions);
  }



  getToken(mode:any=null):Observable<any>{
    if(mode=="orange"){
      return this.http.get<any>(environment.apiBaseUrl+"orange")
    }else{
      return this.http.get<any>(environment.apiBaseUrl+"momo/token")
    }


    }




  pay(data:any,mode:any=null):Observable<any>{
      var body:any={amount:data.amount,phone:data.phone,order:data.order}
      if(mode=="orange"){
        return this.http.post<any>(environment.apiBaseUrl+"orange",body);

      }else{
        return this.http.post<any>(environment.apiBaseUrl+"momo",body);

      }
    
    }


    getpaymentStatus(ref:any):Observable<any>{
      return this.http.get<any>(environment.apiBaseUrl+"momo/status/"+ref);
    }



    checkMail(email:any):Observable<any>{
      return this.http.post(environment.apiBaseUrl+"users/checkemail",{email:email})
    }

}

