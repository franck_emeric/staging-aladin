import { Injectable } from '@angular/core';
import { LoginData,RegisterData } from '../model';
@Injectable({
  providedIn: 'root'
})
export class FormvalidationService {

  constructor() { }

  validateEmail(email:string):boolean
  {
      var re = /\S+@\S+\.\S+/;
      return re.test(email);
  }


  validatePassword(password:string,pwd:string):boolean{
    if(password!=pwd){
      return false;
    }else{
   return true
    }

  }

  validatePhone(phone:string):boolean{
    if(phone.length<10){
      return false;
    }else{
      return true;
    }

  }
  cleanData(data:RegisterData){
    data.user_last_name=data.user_last_name.trim().toUpperCase();
    data.user_email = data.user_email.trim();
    data.user_password = data.user_password.trim();
    data.user_phone= data.user_phone.trim();
    return data;

  }






}
