import { Injectable } from '@angular/core';
import { compress,decompress } from 'compress-json';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
const TOKEN_KEY = 'auth-token';
const REFRESHTOKEN_KEY = 'auth-refreshtoken';
const PAY_TOKEN_KEY="p-auth-token";
const PAY_REFRESHTOKEN_KEY="p-refreshtoken"
const USER_KEY="user"
const NOTIF_TOKEN_KEY="notif-token"

const BaseUrl= environment.baseApi

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private http:HttpClient) { }


  signOut(): void {
    try{
     // window.localStorage.clear();
      localStorage.removeItem(TOKEN_KEY);
      localStorage.removeItem(REFRESHTOKEN_KEY);
      localStorage.removeItem(USER_KEY)
      location.href="/"

    }catch(err){
      console.log(err)
    }
  }

  public saveToken(token: string): void {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.localStorage.getItem(TOKEN_KEY);
  }


  public saveRefreshToken(token: string): void {
    window.localStorage.removeItem(REFRESHTOKEN_KEY);
    window.localStorage.setItem(REFRESHTOKEN_KEY, token);
  }

  public getRefreshToken(): string | null {
    return window.localStorage.getItem(REFRESHTOKEN_KEY);
  }

  public saveUser(data:object){
    let compressed:any = compress(data)
    window.localStorage.setItem(USER_KEY,JSON.stringify(compressed))
  }

  public async getUser(){
    var compressed:string|any= localStorage.getItem(USER_KEY);
    var decompressed= decompress(JSON.parse(compressed))
    return decompressed
  }

  public refresh(token:any):Observable<any>
  {
    return this.http.post(BaseUrl+"users/jwt",token);
  }

  public get_pay_token(){
    return  window.localStorage.getItem(PAY_TOKEN_KEY);

  }
  public get_pay_refresh_token(){
    return window.localStorage.getItem(PAY_REFRESHTOKEN_KEY)
  }

  public save_pay_refresh_token(token:any){
     window.localStorage.removeItem(PAY_REFRESHTOKEN_KEY)
     window.localStorage.setItem(PAY_REFRESHTOKEN_KEY,token)

  }

  public save_pay_token(token:any){
    window.localStorage.removeItem(PAY_TOKEN_KEY)
    window.localStorage.setItem(PAY_TOKEN_KEY,token)

 }

 public save_notif_token(token:any){
  window.localStorage.removeItem(NOTIF_TOKEN_KEY)
  window.localStorage.setItem(NOTIF_TOKEN_KEY,token)

}

}
