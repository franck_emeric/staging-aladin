import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PasswordforData} from '../model';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
declare  var require:any;
  var myalert=require('sweetalert2');
  import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class ForgotpwdService {

  constructor(private http:HttpClient) { }

  passwordforgot(data:any){
    this.veryfyemail(data.email).subscribe(res=>{
      let dat:any=res
      if(dat.user.status){
        this.http.put(environment.apiBaseUrl+'users/password/'+dat.user.user.user_id,data).subscribe(rep=>{
          let mess:any=rep
          if(mess.status){
            myalert.fire({
              title:'Félicition!!!',
              icon:'success',
              text:'Votre mot de pass a été changé avec succès',
              showCloseButton: true,
              onfirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Ok',
            
            
            }).then((result:any)=>{
              if(result.isConfirmed){
               window.location.href="/home"
              }
            })
          }
        },err=>{
          console.log(err)
        })
      }else{
        
      }
      console.log(res)

      return res
    },
    err=>{
      console.log(err)
    })
    //return this.http.post(environment.apiBaseUrl+user,data);

  }

veryfyemail(email:string){

return this.http.post(environment.apiBaseUrl+'users/checkemail',{email:email})
  
}
}