import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http:HttpClient) { }
  BaseUrl= environment.baseUrlDev

  get(){
    return this.http.get<any>(environment.baseApi+"api/v1/")
  }


 getOrders():Observable<any>
 {
 return this.http.get<any>(this.BaseUrl+"users/orders")
  
 } 

 getOrdersByStatus(status:string):Observable<any>{
  return this.http.get<any>(this.BaseUrl+"users/orders/"+status)
 }

 setStatus(id:string|any,status:string):Observable<any>{
   return this.http.patch<any>(this.BaseUrl+"users/orders/"+id,{status:status})
 }

 saveDesigns(data:any):Observable<any>{
  return  this.http.post<any>(this.BaseUrl+"products/designs",data)

 }

 upDateProduct(data:any):Observable<any>{
  return this.http.patch(this.BaseUrl+"products",data)

 }

 upDateUser(data:any){
   return this.http.patch<any>(this.BaseUrl+"admin",data)
 }


 saveUser(data:any):Observable<any>{
   return this.http.post<any>(this.BaseUrl+"admin",data)
 }

 setUserRole(data:any):Observable<any>{
   return this.http.patch(this.BaseUrl+"roles",data)
 }

 getProducts():Observable<any>{
   return this.http.get<any>(this.BaseUrl+"products")
 }


 saveProduct(data:any):Observable<any>{
  return  this.http.post<any>(this.BaseUrl+"products",data)
 }

getProductType():Observable<any>{
 return this.http.get<any>(this.BaseUrl+"products/type")
}

getType=(category:string):Observable<any>=>{
  return this.http.get<any>(this.BaseUrl+"products/type/"+category)
}

getDesigns(param:any=null){
  if(param){
    return this.http.get(environment.baseUrlDev+"products/designs/"+param)

  }else{
    return this.http.get(environment.baseUrlDev+"products/designs")

  }
}

}


