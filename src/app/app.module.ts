import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule, LoginService} from './core';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CGModule } from './cg/cg.module';
import { CUModule } from './cu/cu.module';
//import {ContextMenuModule} from
import { ContextMenuModule } from 'ngx-contextmenu';
import { TokenService } from './core/auth/tokens/token.service';
import { TokenInterceptorService } from './core/auth/interceptor/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PagenotfoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CoreModule,
    CGModule,
    CUModule,
    ContextMenuModule.forRoot()
  ],

  providers: [TokenService,LoginService,{useClass:TokenInterceptorService,provide:HTTP_INTERCEPTORS,multi:true}],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
