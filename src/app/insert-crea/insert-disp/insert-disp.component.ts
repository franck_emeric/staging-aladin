import { Component, OnInit,Input,EventEmitter,Output } from '@angular/core';
import { compress } from 'compress-json';
import { isEmpty } from 'rxjs/operators';
import { AladinService, ListService, LocalService, ObservableserviceService } from 'src/app/core';
declare  var require:any;
import { environment } from 'src/environments/environment.prod';
const host = environment.baseImagePath
var myalert=require('sweetalert2');

@Component({
  selector: 'app-insert-disp',
  templateUrl: './insert-disp.component.html',
  styleUrls: ['./insert-disp.component.scss']
})
export class InsertDispComponent implements OnInit {
@Input() url:any;
@Input() estbache:boolean=true;
@Input() estkake:boolean=true;
@Input() estvinyle:boolean=true;
@Input() estmicro:boolean=true;
changeit=true;
@Output() ChangeIt=new EventEmitter<boolean>()
quantite:any=1
t:any
errond=""
Price:any;
pricestandbache=3000
pricesupbache=7000;
pricestandmicro=5000
pricesupmicro=10000
pricestandPkake=35000
pricesupPkake=45000
pricestandLkake=70000
pricesupLkake=80000
pricestandSD=3500
pricestandD=4500
pricesupSD=7000
pricesupD=8000
perimetre:any=1
longueur:any
largeur:any
null=null
vide=null
impressionbache=false
impressionmicro=false
kakemono=false
vynile=false
petibas=false
largebas=false
dimension=false
hidestandvine=false
hidesupvine=false
file3:any
viewimage:any
filename:any
//bache
standbache=false
supbache=false
//micro
standmicro=false
supmicro=false
//kakemono
standPkake=false
supPkake=false
standBkake=false
supBkake=false
//vynile
standSD=false
standD=false
supSD=false
supD=false
error=""
err=""
dimerr=""
erreur=""
cmpt=0
cpt=0
opaque=false
transp=false
haut:any
large:any
errkake=true
erimport=true
dimetiq=false
format=false
rectangle=false
rond=false
diametre=false
Diam:any
errdiam=""
name="Imprimer affichages"
showme=false
category= {disp:true,name:this.name}
file_type:any;
type_print:any
printing:any=[]
kakemono_print:any=[]
bache_print:any=[]
micro_print:any=[]
id:any
  constructor(private http:ListService,private localservice:LocalService, private uplod: AladinService,private o:ObservableserviceService ) { }

  ngOnInit(): void {
    this.http.getprinting("affichages").subscribe(
      res=>{
      var data:any=res;
      console.log(data)
      for(let item of data.data){
        if(item.productype_label=="vinyle"){
          this.printing.push(item)
        }
        if(item.productype_label=="kakemono"){
          this.kakemono_print.push(item)
        }
        if(item.productype_label=="bache"){
          this.bache_print.push(item)
        }
        if(item.productype_label=="micro-perfore"){
          this.micro_print.push(item)
        }
      }
    

      },
     err=>{
      console.log(err) 
     } 
    )


    this.o.upload_display.subscribe(res=>{
      this.url= res;
      this.showme=true
     if(this.file_type==="bache"){
       this.estkake=false
       this.estmicro=false
       this.estvinyle=false
       this.estbache=true
     } 
     if(this.file_type==="vynile"){
      this.estkake=false
      this.estmicro=false
      this.estvinyle=true
      this.estbache=false
    } 
    if(this.file_type==="kakemono"){
      this.estkake=true
      this.estmicro=false
      this.estvinyle=false
      this.estbache=false
    } 
    if(this.file_type==="micro perforé"){
      this.estkake=false
      this.estmicro=true
      this.estvinyle=false
      this.estbache=false
    } 
    })
  }

 
  OnRadioClick(event:any){
    this.file_type=event
    console.log(event)
  }


  showrond(event:any){
    this.dimetiq=false
    this.rectangle=false
    this.diametre=true
    this.rond=true
  }
  showrectangle(event:any){
    this.dimetiq=true
    this.rectangle=true
    this.diametre=false
    this.rond=false
  }
  Uplade(event:any){
    
    this.file3 =event.target.files
    const urls:any=[]
    const formdata:any= new FormData();
    for(let i=0; i<this.file3.length; i++){
      if(this.file3[i].type=="application/pdf"){
        urls.push(host+this.file3[i].name)
      formdata.append("file",this.file3[i])
      }
    }
    this.http.uploads(formdata).subscribe(res=>{
      var file:any=res
      this.filename=this.file3[0].name
      if(file.status){
        this.viewimage=urls[0]
      }
    },
    err=>{
      console.log(err)
    }
    ) 
   this.erimport=false
   
  
 }
//vynile
showopaque(event:any){
  this.opaque=true
  this.transp=false
  
} 
showtransp(event:any){
  this.opaque=false
  this.transp=true
  
}
//vynile
onchange(event:any){
  this.id=event.target.value
  for(let item of this.printing){
    if(item.printing_id==this.id){
      this.Price=item.printing_price
      this.type_print=item.printing_type_label
      console.log( this.Price,   this.type_print )
      if(item.printing_type_label=="Superieur avec decoupe"){
        this.format=true
      }
      if(item.printing_type_label=="Superieur sans découpe"){
        this.format=false
      }
      if(this.perimetre){
        this.Price = item.printing_price * this.perimetre
      }
    }
  }
}
//kakemono
onchange1(event:any){
  this.id=event.target.value
  for(let item of this.kakemono_print){
    if(item.printing_id==this.id){
      this.Price=item.printing_price
      this.type_print=item.printing_type_label
      console.log( this.Price,   this.type_print )
      
    }
  }
}
//bache
onchange2(event:any){
  this.id=event.target.value
  for(let item of this.bache_print){
    if(item.printing_id==this.id){
      this.Price=item.printing_price
      this.type_print=item.printing_type_label
      if(this.perimetre){
        this.Price= item.printing_price * this.perimetre
      }
      
    }
  }
}
//micro
onchange3(event:any){
  this.id=event.target.value
  for(let item of this.micro_print){
    if(item.printing_id==this.id){
      this.Price=item.printing_price
      this.type_print=item.printing_type_label
      if(this.perimetre){
        this.Price= item.printing_price * this.perimetre
      }
      
    }
  }
}
//vynyle
Change2(event:any){
  for(let item of this.printing){
    if(item.printing_id==this.id){
      if(this.longueur==null && this.largeur!=null)
      this.perimetre=Math.ceil(this.largeur/100)
      this.Price=item.printing_price * this.perimetre  
    }
    if(this.longueur==null && this.largeur==null){
      this.Price=item.printing_price * Math.ceil(this.perimetre)
    }
    if(this.longueur!=null && this.largeur!=null){
      this.perimetre =Math.ceil((this.longueur/100) * (this.largeur/100))
      this.Price=this.Price=item.printing_price * this.perimetre  
    }
  }
}
  
Change(event:any){
  for(let item of this.printing){
    if(item.printing_id==this.id){
      if(this.longueur!=null && this.largeur==null)
      this.perimetre=Math.ceil(this.longueur/100)
      this.Price=item.printing_price * this.perimetre
     
    }
    if(this.longueur==null && this.largeur==null){
    this.Price=item.printing_price * Math.ceil(this.perimetre)
  }
  if(this.longueur!=null && this.largeur!=null){
    this.perimetre = Math.ceil((this.longueur/100) * (this.largeur/100))
    this.Price=this.Price=item.printing_price * this.perimetre  
  }
  }
}
//fin



//espace kakemono

showkakemono(){
    this.dimension=false
    this.vynile=false
    this.kakemono=true
    this.impressionbache=false
    this.impressionmicro=false
  
  }

  //fin
showbache(){
  this.dimension=true
  this.vynile=false
  this.impressionbache=true
  this.impressionmicro=false
   this.kakemono=false
}
showmicro(){
  this.dimension=true
  this.vynile=false
  this.impressionbache=false
  this.impressionmicro=true
  this.kakemono=false
}
showpetitbas(){
this.petibas=true
this.largebas=false
}

showlargebas(){
  this.petibas=false
  this.largebas=true
}
//espace vynyle
showvynile(){
this.dimension=true
this.vynile=true
this.kakemono=false
this.impressionbache=false
this.impressionmicro=false

}



//fin
ChangeComponent(value:boolean){
  this.ChangeIt.emit(value)

}

AddTocart=()=>{
  //var data:any={face1:this.url,};
let cart:any

cart={

  face1:this.url,
  f3:this.viewimage,
  price:this.Price,
  qty: this.quantite,
  category:"affichage",
  type_product:"crea",
  t:this.Price * (parseInt(this.quantite)),
  name:this.file_type || null,
  nome:this.type_print || null,
  hauteur:this.longueur || null,
  large:this.largeur || null
  
}
if(this.opaque){
  Object.assign(cart,{
    vinile:"Vinyle Opaque" || null
  })
}
if(this.transp){
  Object.assign(cart,{
    vinile:"Vinyle Opaque" || null
  })
}
if(this.rond){
  Object.assign(cart,{
    forme:"Etiquette rond" || null,
    diametre:this.Diam || null
  })
}
if(this.rectangle){
  Object.assign(cart,{
    forme:"Etiquette rectangulaire" || null,
    large:this.large || null,
    haut:this.haut || null
  })
}
try {
  cart=compress(cart)
    if(this.file_type=="vynile" && this.type_print!=undefined && this.file3!=undefined && (this.opaque || this.transp) && (this.longueur!=undefined && this.largeur!=undefined))
    {
      this.localservice.adtocart(cart);
        myalert.fire({
          title:'<strong>produit ajouté</strong>',
          icon:'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a>' 
            ,
          showCloseButton: true,
          focusConfirm: false,
         
        })
    }
  if(this.file3==undefined){
    alert("importer votre maquette")
  }
  if(this.file_type==="vynile" && this.rond && this.Diam==undefined){
    alert("remplissez le champs")
  }
  if(this.file_type==="vynile" && this.rectangle && (this.haut==undefined || this.large==undefined)){
    alert("remplissez tous les champs")
  }
  if(this.type_print==undefined){
    myalert.fire({
      title:'<strong>Désolé</strong>',
      icon:'error',
      html:
        'veillez choisir le type de support ou impression'
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
  }
  if(this.file_type==="kakemono" && this.type_print!=undefined && this.file3!=undefined){
    this.localservice.adtocart(cart);
    myalert.fire({
      title:'<strong>produit ajouté</strong>',
      icon:'success',
      html:
        '<h6 style="color:blue">Felicitation</h6> ' +
        '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
        '<a href="/cart">Je consulte mon panier</a>' 
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
  }

if(this.file_type=="bache" && this.type_print!=undefined && this.longueur!=undefined && this.largeur!=undefined)
{
  this.localservice.adtocart(cart);
  myalert.fire({
    title:'<strong>produit ajouté</strong>',
    icon:'success',
    html:
      '<h6 style="color:blue">Felicitation</h6> ' +
      '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
      '<a href="/cart">Je consulte mon panier</a>' 
      ,
    showCloseButton: true,
    focusConfirm: false,
   
  })
}
if(this.file_type=="micro perforé" && this.type_print!=undefined && this.longueur!=undefined && this.largeur!=undefined)
{
  this.localservice.adtocart(cart);
  myalert.fire({
    title:'<strong>produit ajouté</strong>',
    icon:'success',
    html:
      '<h6 style="color:blue">Felicitation</h6> ' +
      '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
      '<a href="/cart">Je consulte mon panier</a>' 
      ,
    showCloseButton: true,
    focusConfirm: false,
   
  })
}
} catch (e:any) {
  console.log(e)
}
}

//Bache
Change4(event:any){
  for(let item of this.bache_print){
    if(item.printing_id==this.id){
      if(this.longueur==null && this.largeur!=null)
      this.perimetre=Math.ceil(this.largeur/100)
      this.Price=item.printing_price * this.perimetre  
    }
    if(this.longueur==null && this.largeur==null){
      this.Price=item.printing_price * Math.ceil(this.perimetre)
    }
    if(this.longueur!=null && this.largeur!=null){
      this.perimetre =Math.ceil((this.longueur/100) * (this.largeur/100))
      this.Price=this.Price=item.printing_price * this.perimetre  
    }
  }
} 
Change3(event:any){
  for(let item of this.bache_print){
    if(item.printing_id==this.id){
      if(this.longueur!=null && this.largeur==null)
      this.perimetre=Math.ceil(this.longueur/100)
      this.Price=item.printing_price * this.perimetre
     
    }
    if(this.longueur==null && this.largeur==null){
    this.Price=item.printing_price * Math.ceil(this.perimetre)
  }
  if(this.longueur!=null && this.largeur!=null){
    this.perimetre = Math.ceil((this.longueur/100) * (this.largeur/100))
    this.Price=this.Price=item.printing_price * this.perimetre  
  }
  }
}
//fin


//Micro
Change5(event:any){
  for(let item of this.micro_print){
    if(item.printing_id==this.id){
      if(this.longueur==null && this.largeur!=null)
      this.perimetre=Math.ceil(this.largeur/100)
      this.Price=item.printing_price * this.perimetre  
    }
    if(this.longueur==null && this.largeur==null){
      this.Price=item.printing_price * Math.ceil(this.perimetre)
    }
    if(this.longueur!=null && this.largeur!=null){
      this.perimetre =Math.ceil((this.longueur/100) * (this.largeur/100))
      this.Price=this.Price=item.printing_price * this.perimetre  
    }
  }
} 
Change6(event:any){
  for(let item of this.micro_print){
    if(item.printing_id==this.id){
      if(this.longueur!=null && this.largeur==null)
      this.perimetre=Math.ceil(this.longueur/100)
      this.Price=item.printing_price * this.perimetre
     
    }
    if(this.longueur==null && this.largeur==null){
    this.Price=item.printing_price * Math.ceil(this.perimetre)
  }
  if(this.longueur!=null && this.largeur!=null){
    this.perimetre = Math.ceil((this.longueur/100) * (this.largeur/100))
    this.Price=this.Price=item.printing_price * this.perimetre  
  }
  }
}
//fin
}
