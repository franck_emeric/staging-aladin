import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertDispComponent } from './insert-disp.component';

describe('InsertDispComponent', () => {
  let component: InsertDispComponent;
  let fixture: ComponentFixture<InsertDispComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertDispComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertDispComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
