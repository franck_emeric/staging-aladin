import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertPackComponent } from './insert-pack.component';

describe('InsertPackComponent', () => {
  let component: InsertPackComponent;
  let fixture: ComponentFixture<InsertPackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertPackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
