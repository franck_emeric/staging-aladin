import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { AladinService, ListService, LocalService, ObservableserviceService } from 'src/app/core';
declare var require: any
var myalert=require('sweetalert2')
import * as FilePond from "filepond";
var FilePondPluginPdfPreview =require("filepond-plugin-pdf-preview");
import { environment } from 'src/environments/environment.prod';
import { compress } from 'compress-json';
const host = environment.baseImagePath
@Component({
  selector: 'app-insert-pack',
  templateUrl: './insert-pack.component.html',
  styleUrls: ['./insert-pack.component.scss']
})
export class InsertPackComponent implements OnInit {

  constructor(private localservice:LocalService, private uplod:AladinService,private O:ObservableserviceService, private http:ListService) {

  }
   url:any;
  @Output() changecomponent=new EventEmitter<boolean>();

 hiderow=false;
 hide=false;
 visible=false
 oui=false;
 imagepreview:any;
 viewimage:any;
 ViewImg:any;
 file2:any;
 file3:any;
 file4:any;
 Price:any;
chanecpt=true;
t:any;
@Input() sachet:boolean=false;
@Input() sac:boolean=false;
swhsac=true;
showme=false
swhsachet=true;
message="veuillez importer votre marquete svp.!";
err=false;
size_1=["15/20","20/25","25/30","28/35","35/45","45/50","50/55"];
price_1=[5000,5000,7000,7000,10000,15000,15000];
promo_1=[4000,4000,6000,6000,8000,10000,10000];
price_duo=[5000,10000,20000];
nbcolor=[1,2,3,4];
CHANGEPRICE_1=5000;
size_2=["20/30","30/30","33/40","40/45","50/60"];
price_2=[3000,5000,7000,15000,15000];
promo_2=[2500,4000,6000,10000,10000]
Cost:any;
new_cost:any;
mynbc:any;
ncolor=false
name="Imprimer emballages"

category= {pack:true,name:this.name}

current_size_1=""
current_size_2=""
choice_1=false;
choice_2=false;
choice_3=false;
choice_4=false;

choice_5=false;
choice_6=false;
choice_7=false;
choice_8=false;
mycpt=0;
shwpr_2=false;
hascolor=false;
hasnotcolor:any;
shwpr=false;
haschangetosac=false;
haschangetosachet=false;
bagqty=100;
filename1:any
filename2:any
file_type:any
p_sizes:any=[]
t_sizes:any=[]
id:any
nbclor:any
  ngOnInit(): void {

    this.O.upload_pack.subscribe(
      res=>{
    
          this.showme=true;
          this.url=res;
          setTimeout(() => {
          var elm:any=document.getElementById("mkr")
          FilePond.registerPlugin(FilePondPluginPdfPreview );
          const pond:any = FilePond.create();
       
          console.log(pond)
       //  elm.appendChild(pond.element);   
          }, 100);
        if(this.file_type=='sac'){
          this.Isac()
        }
        if(this.file_type=='sachet'){
          this.Isachet()
        }
      }
    )
    this.http.getFormat("emballages").subscribe(
      res=>{
        var data:any=res
        console.log(data)
        if(data.status){
          for(let item of data.data){
            if(item.productype_label=="sac"){
              this.p_sizes.push(item)
            }
            if(item.productype_label=="sachet"){
              this.t_sizes.push(item)
              console.log(this.t_sizes)
            }
          }
         
        }
      }
    )

  }

  OnRadioClick(event:any){
    this.file_type=event
    console.log(event)
  }

  Isachet(){
    this.current_size_1="";
    this.current_size_2="";
    this.bagqty=100;
    this.shwpr_2=false;
    this.hascolor=false;
    this.haschangetosachet=false
    this.haschangetosac=false
    if(this.sachet){
      this.sac=false;
      this.sachet=true;
      this.swhsac=true;
      this.swhsachet=true;

    }else{
      this.sachet=true;
      this.sac=false;
      this.swhsac=false;
      this.swhsachet=true;

    }

   }

   Isac(){
    this.bagqty=100
    this.current_size_1=""
    this.current_size_2=""
    this.shwpr=false
    this.haschangetosachet=false
    this.haschangetosac=false
     if(this.sac){
      this.sac=false;
      this.sachet=false;
      this.swhsac=true
      this.swhsachet=true;

     }else{
      this.sac=true;
      this.sachet=false;
      this.swhsac=true
      this.swhsachet=false;


     }
   }


   Uplode(event:any){
    this.file2 =event.target.files[0]
    if(!this.uplod.UpleadImage(this.file2)){
    const reader = new FileReader();

    reader.onload = () => {

    this.imagepreview = reader.result;
    
   };

   reader.readAsDataURL(this.file2);
    console.log(event);
  }else{
    
  }

   }
  View(){
    this.visible=true;
    this.oui=!this.oui

  }
  view2(){
    this.visible=false
  }



  Uplade(event:any){
    this.file3 =event.target.files
    const urls:any=[]
    const formdata:any=new FormData()
    for(let i=0; i<this.file3.length;i++){
      if(this.file3[i].type=="application/pdf"){
        urls.push(host+this.file3[i].name)
        formdata.append("file",this.file3[i])
       }else{
         myalert.fire({
           title: "Désolé!!!!!!",
           text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
           icon: "error",
            button: "Ok"
           });
           setTimeout(() => {
            location.reload()
          }, 300);
       }
       
    }
    this.http.uploads(formdata).subscribe(res=>{
      var file:any=res
      this.filename1=this.file3[0].name
      if(file.status){
        this.viewimage=urls[0]
      }
    },
    err=>{
      console.log(err)
    }
    ) 
   }
   Upladeimage(event:any){
    this.file4 =event.target.files
    const urls:any=[];
    const formdata:any=new FormData()
    for(let i=0; i<this.file4.length; i++){
      if(this.file4[i].type=="application/pdf"){
        urls.push(host+this.file4[i].name)
        formdata.append("file", this.file4[i])
      }else{
        myalert.fire({
          title: "Désolé!!!",
          text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
          icon: "error",
           button: "Ok"
          });
      }
      
    }
    this.http.uploads(formdata).subscribe(res=>{
      var file:any=res
      this.filename2=this.file4[0].name
      if(file.status){
        this.ViewImg=urls[0]
      }
    },
    err=>{
      console.log(err)
    }
    ) 
   


   }


  Addtocart = ()=>{

    var cart:any={};


console.log(this.Price)
    if(this.url!=undefined && this.viewimage!=undefined && this.Cost){
      cart ={
        name:this.file_type,
        type_product:"crea",
        t: this.Cost * (this.bagqty/100),
        category:"emballage",
        face1:this.url,
        face2:this.imagepreview || null,
        f3:this.viewimage || null,
        f4:this.ViewImg || null,
        qty:this.bagqty,
        price:this.Cost,
        color:this.mynbc || null,
        size:this.current_size_1 || null

      };
         try{
           console.log(cart)
           cart=compress(cart)
        this.localservice.adtocart(cart);
           location.href="cart/"

      }catch(e:any){
        console.log(e)
      }
      console.log(cart);




    }





  }

ChangeComponent(value:boolean){
  this.changecomponent.emit(value);

}

onchangesac(event:any){
  this.id=event.target.value;
  this.shwpr=true
  console.log(this.id)
  console.log(this.p_sizes)
  for(let item of this.p_sizes){
    if(item.product_format_id==this.id){
      this.current_size_1= item.product_width+ "cm"+"/"+item.product_height +"cm"
      console.log()
     if(this.nbclor==undefined){
      this.Cost= item.product_price_format 
     }else{
      if(this.nbclor==this.nbcolor[0]){
        this.Cost= item.product_price_format + this.price_duo[0]
      }
      if(this.nbclor==this.nbcolor[1]){
        this.Cost= item.product_price_format + this.price_duo[1]
      }
      if(this.nbclor==this.nbcolor[2]){
        this.Cost= item.product_price_format + this.price_duo[2]
      }
      if(this.nbclor==this.nbcolor[3]){
        this.Cost= item.product_price_format + this.price_duo[2]
      } 
     }
    }
  
  }
 console.log(this.nbclor)
 
}
onchangesachet(event:any){
  this.id=event.target.value;
  console.log(this.id)
  console.log(this.t_sizes)
  this.shwpr_2=true
  for(let item of this.t_sizes){
    if(item.product_format_id==this.id){ 
      this.current_size_1= item.product_width+ "cm"+"/"+item.product_height +"cm"
      this.Cost=item.product_price_format
      if(this.hascolor==false){
        this.Cost=item.product_price_format
        console.log(this.Cost)
      }
      if(this.hascolor==true){
        this.Cost=item.product_price_format + this.CHANGEPRICE_1
        console.log(this.Cost)
      }

     }
    }
 
  }

color(){
  this.hascolor=!this.hascolor
    for(let item of this.t_sizes){
      if(item.product_format_id==this.id){
        if(this.hascolor==true){
          this.Cost=item.product_price_format + this.CHANGEPRICE_1
          console.log(this.Cost)
        }
       if(this.hascolor==false){
        this.Cost=item.product_price_format
        console.log(this.Cost)
       }

       }
      }
  console.log(this.ncolor)
}

colorchange(event:any){
  this.nbclor= event.target.value
  for(let item of this.p_sizes){
    if(item.product_format_id==this.id){
      if(this.nbclor==this.nbcolor[0]){
        this.mynbc=this.nbcolor[0]
        this.Cost= item.product_price_format + this.price_duo[0]
      }
      if(this.nbclor==this.nbcolor[1]){
        this.mynbc=this.nbcolor[1]
        this.Cost= item.product_price_format + this.price_duo[1]
      }
      if(this.nbclor==this.nbcolor[2]){
        this.mynbc=this.nbcolor[2]
        this.Cost= item.product_price_format + this.price_duo[2]
      }
      if(this.nbclor==this.nbcolor[3]){
        this.mynbc=this.nbcolor[3]
        this.Cost= item.product_price_format + this.price_duo[2]
      }
    }
  }
 

}
plus(){
  this.bagqty=this.bagqty + 100
}
minus(){
  if(this.bagqty>100){
  this.bagqty=this.bagqty-100
}else{
this.bagqty=this.bagqty
}
}
}
