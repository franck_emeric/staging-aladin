import { Component, OnInit,Input } from '@angular/core';
import { compress } from 'compress-json';
import { typedArrayFor } from 'pdf-lib';
import { elementAt } from 'rxjs/operators';
import { LocalService,AladinService, ObservableserviceService, ListService } from 'src/app/core';
import { environment } from 'src/environments/environment.prod';
const host = environment.baseImagePath
declare  var require:any;
var myalert=require('sweetalert2');
@Component({
  selector: 'app-insert-print',
  templateUrl: './insert-print.component.html',
  styleUrls: ['./insert-print.component.scss']
})
export class InsertPrintComponent implements OnInit {

  Price:any
  recu:any
  err1=""
  erreur1=""
  
  error="";
  err=""
  erreur=""
  promo=false
  promo1=true
  A42S=false
  A43S=false
  A44S=false
  A52S=false
  A53S=false
  A54S=false
  A62S=false
  A63S=false
  A64S=false
   url: any
  priceSD=3000
  priceD=4000
  largeur:any
  longueur: any
  quantite:any=2
  hideetiquette=false
  carnet=false
  file3:any
  viewimage:any
  priceA4_2S=6500
  newprice:any
  priceA4_3S=9000
  priceA4_4S=11500
  name="Imprimer imprimés"

category= {print:true,name:this.name}
  
  
  etiquette=false
  carnete=false
  sansdecouper=false
  avecdecoupe=false
  qtyetiq=10
  qtyA6:any
  qteti=false
  qtA5=false
  QtA6=false
  QtA4=false
  nptm=1
  npmtma5=1
  npmtma6=1
  cmpt=1
  totale:any
  Qtytotal:any
  newtotale:any
  filename:any
  oui=true
  non=false
  showme=false;
  file_type:any
  carnet_format:any=[]
  etiquette_format:any=[]
  numero_recu:any
  format:any

    constructor(private http:ListService, private localservice:LocalService, private uplod: AladinService,private o:ObservableserviceService) { }
  
    ngOnInit(): void {

      this.http.getFormat("imprimes").subscribe(
        res=>{
          var data:any=res
          console.log(data)
          if(data.status){
            for(let item of data.data){
              if(item.productype_label=="carnet"){
                this.carnet_format.push(item)
              }
              if(item.productype_label=="etiquette"){
                this.etiquette_format.push(item)
              }
            }
           
          }
        }
      )
      this.o.upload_print.subscribe(
        res=>{
          this.url=res;
          this.showme=true
          if(this.file_type=="carnet"){
            this.showcarnet()
            this.qtyA6=2
          }
          if(this.file_type=="etiquette"){
            this.showhideetiquette()
            this.qtyA6=10
          }
        }
      )
    }

onchange(event:any){
  let id=event.target.value
   for(let item of this.carnet_format){
     if(item.product_format_id==id){
       this.Price=item.product_price_format
        this.format=item.other_format
     }
   }
}
onchange1(event:any){
  let id=event.target.value
   for(let item of this.etiquette_format){
     if(item.product_format_id==id){
       this.Price=item.product_price_format
        this.format=item.other_format
     }
   }
}
plus(){
  if(this.file_type=="carnet"){
    this.qtyA6=this.qtyA6 + 2
    this.Qtytotal=this.qtyA6/2
  }
  if(this.file_type=="etiquette"){
    this.qtyA6=this.qtyA6 + 10
    this.Qtytotal=this.qtyA6/10
  }
}
    shownon(){
      this.non=true
      this.oui=false
    }
    showoui(){
      this.non=false
      this.oui=true
    }

    OnRadioClick(event:any){
      this.file_type=event
      console.log(event)
    }

 
  showhideetiquette(){
    this.hideetiquette=true
    this.carnet=false
    this.carnete=false
    this.etiquette=true
    this.qteti=true
    this.qtA5=false
    this.QtA6=false
    this.QtA4=false
  }
  showcarnet(){
    this.carnete=true
    this.hideetiquette=false
    this.carnet=true
    this.qteti=false
    this.etiquette=false
    this.QtA4=false
  }
  Uplade(event:any){
    this.file3 =event.target.files
    const urls:any=[]
    const formdata= new FormData();
    for (let i = 0; i < this.file3.length; i++) {
      if(this.file3[i].type=="application/pdf"){
       urls.push(host+this.file3[i].name)
       formdata.append("file",this.file3[i])
      }else{
       myalert.fire({
         title: "Désolé!!!",
         text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
         icon: "error",
          button: "Ok"
         });
         setTimeout(() => {
           location.reload()
         }, 300);
     }   
      
    }
    this.http.uploads(formdata).subscribe(res=>{
      var file:any=res
      this.filename=this.file3[0].name
      if(file.status){
        this.viewimage=urls[0]
      }
    },
    err=>{
      console.log(err)
    }
    )  
     
   }
   //quantite de A4 carnet
 
  addcart=()=>{
  let cart:any
 
  cart ={ 
    type_product:"crea",
    t:this.Price * this.Qtytotal,
    category:"imprimer",
    face1:this.url,
    f3:this.viewimage,
    qty: this.qtyA6 ,
    price:this.Price,
    type:this.file_type,
    nom:this.format ,
    hauteur:this.longueur || null,
    largeur:this.largeur || null,
    recu:this.recu || null
   }
  
 
  try{
    console.log(cart, "fait")
   
    if((this.file_type=="carnet") && (this.format!=undefined) && (this.file3!=undefined)){
      cart=compress(cart)
        this.localservice.adtocart(cart);
        myalert.fire({
          title:'<strong>produit ajouté</strong>',
          icon:'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a>' 
            ,
          showCloseButton: true,
          focusConfirm: false,
         
        })
      
       console.log(cart)
      
        
      }
      if( this.file_type=="carnet" && this.format==null){
        myalert.fire({
          title:'<strong>Désolé !!!!</strong>',
          icon:'error',
          html:
            '<h6 style="color:blue">Veuillez choisir un format</h6> ' 
            ,
          showCloseButton: true,
          focusConfirm: false,
         
        })
      }
      if( this.file_type=="carnet" && this.file3==null){
        myalert.fire({
          title:'<strong>Désolé !!!!</strong>',
          icon:'error',
          html:
            '<h6 style="color:blue">Veillez importer votre maquette</h6> ' 
            ,
          showCloseButton: true,
          focusConfirm: false,
         
        })
      } 
    if((this.file_type=="etiquette") && this.longueur!=undefined && this.largeur!=undefined && this.file3!=undefined){
      cart=compress(cart)
      this.localservice.adtocart(cart);
      myalert.fire({
        title:'<strong>produit ajouté</strong>',
        icon:'success',
        html:
          '<h6 style="color:blue">Felicitation</h6> ' +
          '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
          '<a href="/cart">Je consulte mon panier</a>' 
          ,
        showCloseButton: true,
        focusConfirm: false,
       
      })
    
     console.log(cart)
    
    }
   if((this.file_type=="etiquette") &&((this.longueur==undefined && this.largeur!=undefined)||(this.longueur!=undefined && this.largeur==undefined))){
    myalert.fire({
      title:'<strong>Désolé !!!!</strong>',
      icon:'error',
      html:
        '<h6 style="color:blue">Veillez remplir les champs de dimension</h6> ' 
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
   }
   if(this.file_type=="etiquette" && this.file3==undefined){
    myalert.fire({
      title:'<strong>Désolé !!!!</strong>',
      icon:'error',
      html:
        '<h6 style="color:blue">Veillez importer votre maquette svp!!!</h6> ' 
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
   }
  }catch(e:any){
    console.log(e)
  }
  
  }
}
