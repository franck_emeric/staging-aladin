import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertPrintComponent } from './insert-print.component';

describe('InsertPrintComponent', () => {
  let component: InsertPrintComponent;
  let fixture: ComponentFixture<InsertPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
