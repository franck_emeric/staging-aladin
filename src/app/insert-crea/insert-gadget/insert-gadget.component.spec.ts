import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertGadgetComponent } from './insert-gadget.component';

describe('InsertGadgetComponent', () => {
  let component: InsertGadgetComponent;
  let fixture: ComponentFixture<InsertGadgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertGadgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertGadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
