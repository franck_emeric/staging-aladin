import { Component, OnInit, Input,Output, EventEmitter, OnChanges, SimpleChanges, IterableDiffers } from '@angular/core';
import { AladinService, ListService, LocalService, ObservableserviceService } from 'src/app/core';
declare  var require:any;
import * as FilePond from "filepond";
var FilePondPluginPdfPreview =require("filepond-plugin-pdf-preview");
var myalert=require('sweetalert2');
import { environment } from 'src/environments/environment.prod';
import { compress } from 'compress-json';
const host = environment.baseImagePath
@Component({
  selector: 'app-insert-gadget',
  templateUrl: './insert-gadget.component.html',
  styleUrls: ['./insert-gadget.component.scss']
})
export class InsertGadgetComponent implements OnInit {

  url:any
  err=""
  errfile=""
  errfile4=""
  error=""
  
  @Output() changecomponent=new EventEmitter<boolean>();
  @Input() stylo:boolean=false
  @Input() tasse:boolean=false
  @Input() portcle:boolean=false
  file2:any
  file3:any
  file4:any
  imagepreview:any
  viewimage:any
  ViewImg:any
  oui=false
  er=true
  ert=true
  qnte:any=50
  qtyunit:any
  quantitetasse:any=1
  Price:any
  publicprice=150
  seriprice=150
  trftprice=100
  uvprice=350
  priceporte=300
  t:any
  magprice=4000
  ordiprice=3000
  //
  pub=false
  serie=false
  transfert=false
  uv=false
  porte=false
  magique=false
  ordinaire=false
  filename1:any
  filename2:any
  showme=false;
  name="Imprimer  gadget"
  gadget_type:any
  printing:any=[]
  tasse_print:any=[]
  porte_print:any=[]
  impr:any
  category= {gadget:true,name:this.name}
 
  constructor(private http:ListService, private localservice:LocalService, private uplod:AladinService,private O:ObservableserviceService) { }

  ngOnInit(): void {
    this.http.getprinting("gadgets").subscribe(
      res=>{
      var data:any=res;
      console.log(data)
      for(let item of data.data){
        if(item.productype_label=="stylot"){
          this.printing.push(item)
        }
        if(item.productype_label=="tasse"){
          this.tasse_print.push(item)
        }
        if(item.productype_label=="Porte-clé"){
          this.porte_print.push(item)
        }
      }
     

      },
     err=>{
      console.log(err) 
     } 
    )

    this.O.upload_gadget.subscribe(
      res=>{
    
          this.showme=true;
          this.url=res;
          setTimeout(() => {
          var elm:any=document.getElementById("mkr")
          FilePond.registerPlugin(FilePondPluginPdfPreview );
          const pond:any = FilePond.create();
       
          console.log(pond)
       //  elm.appendChild(pond.element);   
          }, 100);
        if(this.gadget_type=="tasse"){
          this.tasse=true
          this.stylo=false
          this.portcle=false
        }
        if(this.gadget_type=="stylot"){
          this.tasse=false
          this.stylo=true
          this.portcle=false
        }
        if(this.gadget_type=="porte clé"){
          this.tasse=false
          this.stylo=false
          this.portcle=true
        }
      }
    )
  
  }
 onchange(event:any){
   let id=event.target.value
   for(let item of this.printing){
     if(item.printing_id==id){
       this.Price= this.publicprice + item.printing_price
       this.impr=item.printing_type_label
     }
   }
 }
 onchange1(event:any){
  let id=event.target.value
  for(let item of this.tasse_print){
    if(item.printing_id==id){
      this.Price= item.printing_price
      this.impr=item.printing_type_label
    }
  }
}
onchange2(event:any){
  let id=event.target.value
  for(let item of this.porte_print){
    if(item.printing_id==id){
      this.Price= item.printing_price
      this.impr=item.printing_type_label
    }
  }
}
OnRadioClick(event:any){
  this.gadget_type=event
  console.log(event)
}


ChangeComponent(eve:any){
  this.showme=!this.showme
}


Uplode_front(event:any){
  var file =event.target.files[0]
  if(!this.uplod.UpleadImage(file)){
  const reader = new FileReader();
  reader.onload = () => {
  this.url = reader.result;

}
reader.readAsDataURL(file);


}
}



  Uplode(event:any){
    this.file2 =event.target.files[0]
    if(!this.uplod.UpleadImage(this.file2)){
    const reader = new FileReader();
  reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(this.file2);
    console.log(this.file2)
  }else{

  }
   }
  
  Uplade(event:any){
    this.file3 =event.target.files
    const urls:any=[]
    const formdata:any=new FormData()
    for(let i=0; i<this.file3.length;i++){
      if(this.file3[i].type=="application/pdf"){
        urls.push(host+this.file3[i].name)
        formdata.append("file",this.file3[i])
      
      }else{
        myalert.fire({
          title: "Désolé!!!",
          text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
          icon: "error",
           button: "Ok"
          });
          setTimeout(() => {
            location.reload()
          }, 300);
       }
       
    }
    this.http.uploads(formdata).subscribe(res=>{
      var file:any=res
      this.filename1=this.file3[0].name
      if(file.status){
        this.viewimage=urls[0]
      }
    },
    err=>{
      console.log(err)
    }
    ) 
   }
   Upladeimage(event:any){
    this.file4 =event.target.files
    const urls:any=[];
    const formdata:any=new FormData()
    for(let i=0; i<this.file4.length; i++){
      if(this.file4[i].type=="application/pdf"){
        urls.push(host+this.file4[i].name)
        formdata.append("file", this.file4[i])
      }else{
        myalert.fire({
          title: "Désolé!!!",
          text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
          icon: "error",
           button: "Ok"
          });
      }
      
    }
    this.http.uploads(formdata).subscribe(res=>{
      var file:any=res
      this.filename2=this.file4[0].name
      if(file.status){
        this.ViewImg=urls[0]
      }
    },
    err=>{
      console.log(err)
    }
    ) 
   
   }
   showoui(){
     this.oui=!this.oui
   }

   //quantite
   qtyplus(){
     this.qnte=this.qnte + 1
   }

   qtyminus(){
     if(this.qnte>50){
      this.qnte=this.qnte - 1
     }else{
      this.qnte=this.qnte
     }
   }
//show price
  showpblicprice(){
    this.Price=this.publicprice
    this.ert=false
    this.pub=true
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=false
  }
 showseriprice(){
   this.er=false
   this.Price= this.publicprice + this.seriprice 
   this.pub=true
    this.serie=true
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=false
 }

 showtrftprice(){
  this.Price= this.publicprice + this.trftprice
  this.er=false
  this.pub=true
    this.serie=false
    this.transfert=true
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=false
}
showuvprice(){
  this.er=false
  this.Price= this.publicprice + this.uvprice 
  this.pub=true
    this.serie=false
    this.transfert=false
    this.uv=true
    this.porte=false
    this.magique=false
    this.ordinaire=false
}
showporteprice(){
  this.Price=this.priceporte
  this.pub=false
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=true
    this.magique=false
    this.ordinaire=false
}
showmagprice(){
  this.Price=this.magprice
  this.pub=false
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=true
    this.ordinaire=false
}
showordiprice(){
  this.Price=this.ordiprice
  this.pub=false
    this.serie=false
    this.transfert=false
    this.uv=false
    this.porte=false
    this.magique=false
    this.ordinaire=true
}

addtocart=()=>{
  let cart: any
  if(this.stylo || this.portcle){
   this.qtyunit=this.qnte
  }
  if(this.tasse){
    this.qtyunit=this.quantitetasse
  }
if(this.file2!=undefined && this.file4!=undefined){
 cart={
   type_product:"crea",
   category:"gadget",
   qty:this.qtyunit,
   t:this.Price * this.qtyunit,
   price:this.Price,
   impr:this.impr,
   face1: this.url,
   face2:this.imagepreview ,
   f3: this.viewimage,
   f4: this.ViewImg
 }
if(this.pub){
  Object.assign(cart, {
    nom:"Stylo grand public",
    prixe:this.publicprice
  })
}



}else{
  cart={
    type_product:"crea",
    category:"gadget",
    qty:this.qtyunit,
    t:this.Price * this.qtyunit,
    impr:this.impr,
    type: this.gadget_type,
    price:this.Price,
    face1: this.url,
    face2:null ,
    f3: this.viewimage,
    f4: null
  }
 if(this.pub){
   Object.assign(cart, {
     nom:"Stylo grand public" || null,
     
   })
 }

 
 
try {
  cart=compress(cart)
  if((this.gadget_type=="stylot" && this.pub && this.impr!=undefined && this.file3!=undefined)){
    this.localservice.adtocart(cart);
    myalert.fire({
      title:'<strong>produit ajouté</strong>',
      icon:'success',
      html:
        '<h6 style="color:blue">Felicitation</h6> ' +
        '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
        '<a href="/cart">Je consulte mon panier</a>' 
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
  }
  if((this.impr!=undefined && this.file3!=undefined)){
    this.localservice.adtocart(cart);
    myalert.fire({
      title:'<strong>produit ajouté</strong>',
      icon:'success',
      html:
        '<h6 style="color:blue">Felicitation</h6> ' +
        '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
        '<a href="/cart">Je consulte mon panier</a>' 
        ,
      showCloseButton: true,
      focusConfirm: false,
     
    })
  }
  if(this.pub===false){
    this.error="choisisez le type de stylo"
  }
  if(this.impr==undefined){
      this.err="choisissez un type d'impression"
    }
  if(this.oui==true ){
    if(this.file2==undefined){
      this.errfile="Veillez importer la face arrière du visuel"
    }
    if(this.file4==undefined && this.file3==undefined){
      this.errfile4="Veillez importer la face avant et arrière de la maquette"
    }
  }else{
    if(this.file3==undefined){
      this.errfile4="importer la maquette de la face avant"
    }
  
  }
  if(this.gadget_type=="porte clé" && this.impr==undefined){
    this.err="choissisez le type de porte-clé"
  }
  if(this.gadget_type=="tasse" && this.impr==undefined){
    this.err="choisissez un type de tasse"
  }
} catch (error) {
  console.log(error)
}
 console.log(cart, this.pub)

}


}
}
