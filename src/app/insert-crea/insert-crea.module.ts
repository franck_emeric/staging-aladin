import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsertClothComponent } from './insert-cloth/insert-cloth.component';
import { FormsModule } from '@angular/forms';
import { UploadFormComponent } from './upload-form/upload-form.component';
import { InsertGadgetComponent } from './insert-gadget/insert-gadget.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { InsertPrintComponent } from './insert-print/insert-print.component';
import { InsertPackComponent } from './insert-pack/insert-pack.component';
import { InsertDispComponent } from './insert-disp/insert-disp.component';

@NgModule({
  declarations: [
    InsertClothComponent,
    UploadFormComponent,
    InsertGadgetComponent,
    InsertPrintComponent,
    InsertPackComponent,
    InsertDispComponent
  ],
  imports: [
    CommonModule,
    FormsModule ,
    PdfViewerModule
  ],
  exports:[InsertClothComponent,
    InsertGadgetComponent,InsertDispComponent,InsertPackComponent,InsertPrintComponent
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class InsertCreaModule { }
