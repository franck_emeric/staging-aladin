import { Component, OnInit,Output,EventEmitter, Input,SimpleChanges,OnChanges } from '@angular/core';
import { ListService, ObservableserviceService } from 'src/app/core';
import { environment } from 'src/environments/environment.prod';
const host = environment.baseImagePath

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.scss']
})
export class UploadFormComponent implements OnInit,OnChanges {
 @Input() data:any;
@Output() file_cloth= new EventEmitter<any>();
@Output() file_type= new EventEmitter<any>();
cloth=true
gadget=true
pack=true;
disp=true;
print=true;
tasse=false;
stylot=false;
port_key=false;
polo=false;
t_shirt_ord=false;
t_shirt_blanc=false
t_shirt_poly=false
t_shirt_coul=false
sac=false;
sachet=false;
bache=false;
vynile=false;
etiquette=false;
carnet=false;
kakemono=false
micro_perfore=false
  constructor(private O:ObservableserviceService,private l:ListService) { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {

    
  }

  
   OnRadioClick(event:any){
        let    name= event.target.name;
        //gadgets
        if(name==="tasse"){
         this.tasse=true;
         this.port_key=false
         this.stylot=false

        }
        if(name==="porte clé"){
          this.tasse=false;
          this.port_key=true;
          this.stylot=false
        }
        if(name==="stylot"){
          this.tasse=false;
          this.port_key=false;
          this.stylot=true
        } 
        //Vetements
        if(name==="t_shirt_couleur"){
          this.t_shirt_poly=false
          this.t_shirt_coul=true
          this.polo=false;
          this.t_shirt_ord=false;
          this.t_shirt_blanc=false;
        }
        if(name=="t_shirt_polyesther"){
          this.t_shirt_poly=true
          this.t_shirt_coul=false
          this.polo=false;
          this.t_shirt_ord=false;
          this.t_shirt_blanc=false;
        }

        if(name==="t-shirt-blanc"){
          this.t_shirt_poly=false
          this.t_shirt_coul=false
          this.polo=false;
          this.t_shirt_ord=false;
          this.t_shirt_blanc=true;

        }

        if(name=="t-shirt-ordinaire"){
          this.t_shirt_poly=false
          this.t_shirt_coul=false
          this.polo=false;
          this.t_shirt_ord=true;
          this.t_shirt_blanc=false
        }
        if(name=="polo"){
          this.t_shirt_poly=false
          this.t_shirt_coul=false
          this.polo=true;
          this.t_shirt_ord=false;
          this.t_shirt_blanc=false
        }
        //Emballages
        if(name=="sac"){
          this.sac=true;
          this.sachet=false
        }

        if(name=="sachet"){
          this.sac=false;
          this.sachet=true
        }

        //affichage
        if(name=="vynile"){
         this.vynile=true;
         this.bache=false
         this.kakemono=false
         this.micro_perfore=false
        }
        if(name=="bache"){
          this.vynile=false;
          this.bache=true
          this.kakemono=false
         this.micro_perfore=false
         }
         if(name=="kakemono"){
          this.vynile=false;
          this.bache=false
          this.kakemono=true
         this.micro_perfore=false
         }
         if(name=="micro perforé"){
          this.vynile=false;
          this.bache=false
          this.kakemono=false
         this.micro_perfore=true
         }
         //imprimés
        if(name=="carnet"){
          this.carnet=true;
          this.etiquette=false
        }

        if(name=="etiquette"){
          this.carnet=false;
          this.etiquette=true
        }


        this.file_type.emit(name)
   }

   upload_cloth(event:any){
   const files = event.target.files;
   const urls:any=[]
   const formdata :any= new FormData();
   for (let i = 0; i < files.length; i++) {
     urls.push(host+files[i].name)
     formdata.append("file",files[i])
   }

  this.l.uploads(formdata).subscribe(res=>{
    var file:any=res
    console.log(urls)
    if(file.status){
      this.O.upload_cloth.next(urls)
    }
  },
  err=>{
    console.log(err)
  }
  )   

  }


  upload_print(event:any){
    let file =event.target.files
    const urls:any=[]
    const formdata:any= new FormData();
    for (let i = 0; i < file.length; i++) {
      urls.push(host+file[i].name)
      formdata.append("file",file[i])
    }
    this.l.uploads(formdata).subscribe(res=>{
      var file:any=res
      console.log(urls)
      if(file.status){
        this.O.upload_print.next(urls)
      }
    },
    err=>{
      console.log(err)
    }
    ) 

  }



  upload_gadget(event:any){
    let file =event.target.files
    const urls:any=[]
    const formdata:any=new FormData()
    for(let i=0; i<file.length; i++){
      urls.push(host+file[i].name)
      formdata.append("file",file[i])
    }
    this.l.uploads(formdata).subscribe(res=>{
      var file:any=res
      console.log(urls)
      if(file.status){
        this.O.upload_gadget.next(urls)
      }
    },
    err=>{
      console.log(err)
    }
    ) 
  }

  upload_pack(event:any){
    let file =event.target.files
    const urls:any=[]
    const formdata:any=new FormData()
    for(let i=0; i<file.length;i++){
      urls.push(host+file[i].name);
      formdata.append("file",file[i])
    }
    this.l.uploads(formdata).subscribe(res=>{
      var file:any=res
      console.log(urls)
      if(file.status){
        this.O.upload_pack.next(urls)
      }
    },
    err=>{
      console.log(err)
    }
    ) 
    
  }


  upload_disp(event:any){
    let file =event.target.files
    const urls:any=[]
    const formdata:any=new FormData();
    for(let i=0; i<file.length; i++){
      urls.push(host+file[i].name)
      formdata.append("file",file[i])
    }
    this.l.uploads(formdata).subscribe(res=>{
      var file:any=res
      console.log(urls)
      if(file.status){
        this.O.upload_display.next(urls)
      }
    },
    err=>{
      console.log(err)
    }
    ) 
  }





  



  
}
