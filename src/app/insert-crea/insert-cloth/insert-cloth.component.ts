import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { compress } from 'compress-json';
declare var require:any;
import { ListService, LocalService, } from 'src/app/core';
import { AladinService,ObservableserviceService } from 'src/app/core';
import { environment } from 'src/environments/environment.prod';
const host = environment.baseImagePath
const myalert=require("sweetalert2");
@Component({
  selector: 'app-insert-cloth',
  templateUrl: './insert-cloth.component.html',
  styleUrls: ['./insert-cloth.component.scss']
})
export class InsertClothComponent implements OnInit {


  constructor(private http:ListService,private localservice:LocalService, private uplod:AladinService,private O:ObservableserviceService ) { }

  url:any;
  @Output() newplusMEvent=new EventEmitter<any>();
  @Output() newminusMEvent=new EventEmitter<any>();
  @Output() newplusSEvent=new EventEmitter<any>();
  @Output() newminusSEvent=new EventEmitter<any>();
  @Output() newplusLEvent=new EventEmitter<any>();
  @Output() newminusLEvent=new EventEmitter<any>();
  @Output() newplusXLEvent=new EventEmitter<any>();
  @Output() newminusXLEvent=new EventEmitter<any>();
  @Output() changecomponent=new EventEmitter<boolean>();
  hiderow=false;
 hide=false;
 visible=false
 hidechoice=false;
 hidechildopt=false
 hidepersonopt=false
 imagepreview:any;
 viewimage:any;
 ViewImg:any;
 QtM:any=1;
 QtS:any=0;
 QtL:any=0;
 QtXL:any=0;
 name :any="Imprimer  vetement"
 file2:any;
 file3:any;
 file4:any;
 cptcl=0;
 mybodr=1;
 isbord=false
 public serie=false;
 public flex=false;
 public borderie=false;
 public suble=false
 public transft=false
 nbcolorserie:any=1;
 Price:any;
 prix=2000//2500;
 prixtcolor=2500 //3500
 pricepolo=2500 //3500
 pricetordinary=1500
 pricetpolycoton=1500;
 pricesuble=1000;
 pricetransfert=1000;
 priceAPchild=3600;
 priceSPchild=2600;
 priceAPgrand=5100;
 priceSPgrand=3600;
 pricepromo=1500;
 newprice:any
 cmpt=0
 cpt=0
 cptclikfl=0
 cmpte=0
 cptwithg=0;
 cptwthng=0;
 sgprice=500
 ispricetcolor :boolean=false
 ispricepolo :boolean=false;
 ispricepolycoton :boolean=false
 isprix :boolean=false
 ispriceordinary:boolean=false
 chanecpt :boolean=true
public shirtwhite :boolean=false
public shirtcolor :boolean=false
public shirtordinary :boolean=false
public shirtpolycoton :boolean=false
public polo :boolean=false;
oui :boolean=false;
t:any;
showall :boolean=false;
public sansphotochild :boolean=false;
public avecphotochild :boolean=false;
public enfant :boolean=false;
public grandperson :boolean=false;
public sansphotogrand :boolean=false;
public avecphotogrand=false;
category= {cloth:true,name:this.name}
promo:boolean=false;
promo1:boolean=false;
filename1:any;
filename2:any;
file_type:any;
 print_types:any=[];
 printing:any
 type:any
  ngOnInit(): void {

    this.http.getprinting("vetements").subscribe(
      res=>{
      var data:any=res;
      console.log(data)
      this.printing =data.data

      },
     err=>{
      console.log(err) 
     } 
    )
     
  this.O.upload_cloth.subscribe(
    res=>{
        this.showall=true;
        this.url=res[0];
      if(this.file_type=="t-shirt-blanc"){
        this.affprice(event)
      }
      if(this.file_type=="t_shirt_couleur"){
        this.showpricetcolor(event)
      }
      if(this.file_type=="polo"){
        this.showpricepolo(event)
        
      }
      if(this.file_type=="t-shirt-ordinaire"){
        this.showpricetordi(event)
       
      }
      if(this.file_type=="t_shirt_polyesther"){
        this.showpricecoton(event)
      }
    }
  );



  
}
change(event:any){
  var id =event.target.value
  console.log(id)
  for(let item of this.printing){
  if(item.printing_id==id){
    this.type = item.printing_type_label
    if(this.file_type=="t-shirt-blanc"){
     this.Price= this.prix + item.printing_price
     console.log(this.Price)
    }
    if(this.file_type=="t_shirt_couleur"){
      this.Price=this.prixtcolor +  item.printing_price
    }
    if(this.file_type=="polo"){
      this.Price=this.pricepolo +  item.printing_price
      
    }
    if(this.file_type=="t-shirt-ordinaire"){
      this.Price=this.pricetordinary +  item.printing_price
    }
    if(this.file_type=="t_shirt_polyesther"){
      this.Price=this.pricetpolycoton +  item.printing_price
    }
  }
  }
}


OnRadioClick(event:any){
  this.file_type=event
 }


  InputChange(){

 if(this.nbcolorserie){
  if(parseInt(this.nbcolorserie)==1 && this.sgprice==1000){
    this.Price = (parseInt(this.Price)-500) 
    this.sgprice=500
  }

  if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
    if(this.Price==this.prix){
      this.Price=+this.Price + 500
      this.sgprice=500
    }

    if(this.Price==this.prixtcolor){
      this.Price=+this.Price + 500
      this.sgprice=500
    }

    if(this.Price==this.pricepolo){
       this.Price=+this.Price + 500
       this.sgprice=500
  
    }
    if(this.Price==this.pricetpolycoton){
      this.Price=+this.Price + 500
      this.sgprice=500
 
   }
   if(this.Price==this.pricetordinary){
    this.Price=+this.Price + 500
    this.sgprice=500

 }

  }

  if(parseInt(this.nbcolorserie)>1 && this.sgprice==500){
    if(this.isprix){
      if(this.Price==this.prix){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000;
      }

    }
  
    if(this.ispriceordinary){
      if(this.Price==this.pricetordinary){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
   
    if(this.ispricepolycoton){
      if(this.Price==this.pricetpolycoton){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
   


    if(this.ispricepolo){
      if(this.Price==this.pricepolo){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
  
    if(this.ispricetcolor){
      if(this.Price==this.prixtcolor){
        this.Price=parseInt(this.Price) + 1000
        this.sgprice=1000
      }else {
        this.Price=(parseInt(this.Price)-500) + 1000
        this.sgprice=1000
      }
    }
   


  }

 

 }else{
   if(this.isprix){
    this.Price=this.prix;

   }
   if(this.ispricepolo){
    this.Price=this.pricepolo;

   }
   if(this.ispricetcolor){
    this.Price=this.prixtcolor;

   }

   if(this.ispricepolycoton){
    this.Price=this.pricetpolycoton;

   }

   if(this.ispriceordinary){
    this.Price=this.pricetordinary;

   }
   this.sgprice=500
 }
   
  
  }

cacher(){

  if(this.transft){
    if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
      // this.isprix=!this.isprix
      this.Price=(parseInt(this.Price) - this.pricetransfert) + 500;
      this.cmpt=0;
  
   }

  }
  

  if(this.suble)
  {
    if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
      // this.isprix=!this.isprix
      this.Price=(parseInt(this.Price) - this.pricesuble) + 500;
      this.cmpt=0;
  
   }


   if(parseInt(this.nbcolorserie)>1){
    this.Price=(parseInt(this.Price) - this.pricesuble) + 1000;
    this.cmpt=0;

 }
  }

  if(this.borderie){  

    if(parseInt(this.nbcolorserie)==1){
      this.Price= (+this.Price-((+this.mybodr)*500)) + 500;
      this.cmpt=0;
  
   }

   if(parseInt(this.nbcolorserie)>1 ){
    this.Price= (+this.Price-((+this.mybodr)*500)) +1000;
    this.cmpt=0;

 }

}

  
  if(!this.flex && !this.transft && !this.suble && !this.borderie && !this.serie){
    this.Price= parseInt(this.Price) + 500;

  }

  if(this.flex){
    if(this.isprix){
      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price=+this.prix+ 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price=+this.prix+ 1000;
      this.cmpt=0;
  
   }

      
   
    }


    if(this.ispricetcolor){

      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price=+this.prixtcolor+ 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.prixtcolor+ 1000;
      this.cmpt=0;
  
   }
  

    }

    if(this.ispricepolo){
      
      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price= +this.pricepolo+ 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.pricepolo+ 1000;
      this.cmpt=0;
  
   }
     

    }

    if(this.ispricepolycoton){

      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price= +this.pricetpolycoton + 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.pricetpolycoton+ 1000;
      this.cmpt=0;
  
   }

     

    }
    

    if(this.ispriceordinary){


      if(parseInt(this.nbcolorserie)==1 && this.sgprice==500){
        this.Price= +this.pricetordinary + 500;
        this.cmpt=0;
    
     }

     if(parseInt(this.nbcolorserie)>1){
      this.Price= +this.pricetordinary+ 1000;
      this.cmpt=0;
  
   }
    

    }





  }
 
  this.hidechildopt=false
  this.hidepersonopt=false
  this.hide=true;
  this.isbord=false;
  this.borderie=false
  this.hidechoice=false
  this.flex=false
  this.serie=true
  this.transft=false
    this.suble=false
   }


   viewchildopt(){
  this.cpt=0;
  this.cmpte=0;
  this.cptwithg=0;
  this.cptwthng=0;
  this.avecphotochild=false;
  this.avecphotogrand=false;
  this.sansphotogrand=false;
  this.sansphotochild=false;

    if(this.isprix){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.prix + 500
    }


    if(this.ispricetcolor){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.prixtcolor + 1000
    }


    if(this.ispricepolo){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.pricepolo +1000
    }



    if(this.ispricepolycoton){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.pricetpolycoton 
    }



    if(this.ispriceordinary){

      this.enfant=true
      this.grandperson=false
      this.hidechildopt=true
      this.hidepersonopt=false
      this.Price=this.pricetordinary
    }

      

   }
   viewpersonopt(){

    this.cpt=0;
    this.cmpte=0;
    this.cptwithg=0;
    this.cptwthng=0;
    this.avecphotochild=false;
    this.avecphotogrand=false;
    this.sansphotogrand=false;
    this.sansphotochild=false;
    if(this.isprix){
      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.prix +500
    }

    if(this.ispricetcolor){

      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.prixtcolor +1000
    }
     if(this.ispriceordinary){

      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.pricetordinary 
    }


    if(this.ispricepolycoton){

      
      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.pricetpolycoton
    }


    if(this.ispricepolo){
    
      this.enfant=false
      this.grandperson=true
       this.hidepersonopt=true
       this.hidechildopt=false
      this.Price=this.pricepolo + 1000
    }


   }

InputChangeB(event:any){
  if(this.mybodr==null){
    this.mybodr=1
    

  }

  if(+this.mybodr>=1){
    if(this.isprix){
      if(this.Price>this.prix){
        this.Price=this.prix + ((+this.mybodr)*500)
      }
  
    }

    if(this.ispriceordinary){
      if(this.Price>this.pricetordinary){
        this.Price=this.pricetordinary + ((+this.mybodr)*500)
      }
  
    }


    if(this.ispricepolycoton){
      if(this.Price>this.pricetpolycoton){
        this.Price=this.pricetpolycoton + ((+this.mybodr)*500)
      }
  
    }

    if(this.ispricepolo){
      if(this.Price>this.pricepolo){
        this.Price=this.pricepolo + ((+this.mybodr)*500)
      }
  
    }

    if(this.ispricetcolor){
      if(this.Price>this.prixtcolor){
        this.Price=this.prixtcolor + ((+this.mybodr)*500)
      }
  
    }
  }
 
}

   cachebrd(){
     if(!this.suble && !this.transft && !this.serie && !this.flex && !this.borderie){
      this.Price=+this.Price + (+this.mybodr)*500
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true
     }

    if(this.serie){

      if(this.nbcolorserie>1){
        this.Price=(+this.Price -1000)+ ((+this.mybodr)*500)

      }

      if(this.nbcolorserie==1){
        this.Price=(+this.Price -500)+ ((+this.mybodr)*500)

      }
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true
    }


    if(this.flex){
   if(this.isprix){
     this.Price=+this.prix+ ((+this.mybodr)*500)
    this.transft=false
    this.suble=false
    this.hidechildopt=false
    this.hidepersonopt=false
     this.hide=false
     this.borderie=true
     this.flex=false
     this.serie=false
     this.hidechoice=false
     this.isbord=true
   }


   if(this.ispricetcolor){
    this.Price=+this.prixtcolor+ ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }


  if(this.ispricepolycoton){
    this.Price=+this.pricetpolycoton+ ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }


  if(this.ispricepolo){
    this.Price=+this.pricepolo+ ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }

  if(this.ispriceordinary){
    this.Price=+this.pricetordinary + ((+this.mybodr)*500)
   this.transft=false
   this.suble=false
   this.hidechildopt=false
   this.hidepersonopt=false
    this.hide=false
    this.borderie=true
    this.flex=false
    this.serie=false
    this.hidechoice=false
    this.isbord=true
  }
    }


     if(this.transft){

      this.Price=(+this.Price-this.pricetransfert) + (+this.mybodr)*500
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true
     }
     if(this.suble){

      this.Price=(+this.Price-this.pricesuble) + (+this.mybodr)*500
      this.transft=false
      this.suble=false
      this.hidechildopt=false
      this.hidepersonopt=false
       this.hide=false
       this.borderie=true
       this.flex=false
       this.serie=false
       this.hidechoice=false
       this.isbord=true

     }


  
     
   }
   cacheflx(){
     if(this.shirtwhite){
       
       this.Price=this.Price + 500
     }
     if(this.shirtcolor){
      this.Price=this.Price + 1000
    }
    if(this.polo){
      this.Price=this.Price + 1000
    }
    if(this.suble){
      this.Price= +this.Price-this.pricesuble;
      this.hide=false
      this.isbord=false;
      this.flex=true
      this.transft=false
      this.suble=false
      this.borderie=false
      this.serie=false
      this.hidechoice=true
      this.hidechildopt=false
      this.hidepersonopt=false
    }

    if(this.serie){
      if(this.nbcolorserie==1){
        this.Price= +this.Price-500;
        this.hide=false
        this.isbord=false;
        this.flex=true
        this.transft=false
        this.suble=false
        this.borderie=false
        this.serie=false
        this.hidechoice=true
        this.hidechildopt=false
        this.hidepersonopt=false
      }

      if(this.nbcolorserie>1){
        this.Price= +this.Price-1000;
        this.hide=false
        this.flex=true
        this.isbord=false;

        this.transft=false
        this.suble=false
        this.borderie=false
        this.serie=false
        this.hidechoice=true
        this.hidechildopt=false
        this.hidepersonopt=false

      }
    }

   if(this.borderie){
    this.Price= +this.Price-((+this.mybodr)*500);
    this.hide=false
    this.flex=true
    this.transft=false
    this.isbord=false;

    this.suble=false
    this.borderie=false
    this.serie=false
    this.hidechoice=true
    this.hidechildopt=false
    this.hidepersonopt=false
   }
    if(this.transft){
      this.Price= +this.Price-this.pricetransfert;
      this.hide=false
      this.flex=true
      this.transft=false
      this.isbord=false;

      this.suble=false
      this.borderie=false
      this.serie=false
      this.hidechoice=true
      this.hidechildopt=false
      this.hidepersonopt=false
    }



   if(!this.serie && !this.flex && !this.borderie && !this.transft && !this.suble){
    this.hide=false
    this.flex=true
    this.transft=false
    this.suble=false
    this.borderie=false
    this.serie=false
    this.hidechoice=true
    this.hidechildopt=false
    this.isbord=false;

    this.hidepersonopt=false
   }


   }
  
   Uplode(event:any){
    this.file2 =event.target.files[0]
    if(!this.uplod.UpleadImage(this.file2)){
    const reader = new FileReader();
  reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(this.file2);
    console.log(event)
  }else{

  }  
   }
  View(){
    this.visible=true;
    this.oui=!this.oui
  }
  view2(){
    this.visible=false
  }

  Uplade(event:any){

    const files = event.target.files;
   const urls:any=[]
   const formdata :any= new FormData();
   for (let i = 0; i < files.length; i++) {
     if(files[i].type=="application/pdf"){
      urls.push(host+files[i].name)
      formdata.append("file",files[i])
     }else{
      myalert.fire({
        title: "Désolé!!!",
        text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
        icon: "error",
         button: "Ok"
        });
        setTimeout(() => {
          location.reload()
        }, 300);
    }   
     
   }
  this.http.uploads(formdata).subscribe(res=>{
    var file:any=res
    this.filename1=files[0].name
    if(file.status){
      this.viewimage=urls[0]
    }
  },
  err=>{
    console.log(err)
  }
  )   
      
   }

   

   Upladeimage(event:any){
    const files = event.target.files;
    const urls:any=[]
    const formdata :any= new FormData();
    for (let i = 0; i < files.length; i++) {
      if(files[i].type=="application/pdf"){
       urls.push(host+files[i].name)
       formdata.append("file",files[i])
      }else{
       myalert.fire({
         title: "Désolé!!!",
         text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
         icon: "error",
          button: "Ok"
         });
         setTimeout(() => {
           location.reload()
         }, 300);
     }   
      
    }
   this.http.uploads(formdata).subscribe(res=>{
     var file:any=res
     this.filename2=files[0].name
     if(file.status){
       this.ViewImg=urls[0]
     }
   },
   err=>{
     console.log(err)
   }
   )   
   }
   //quantite de la taille M
   QtplusM(event:any){
     this.QtM =this.QtM +1;
    console.log(this.QtM)
     
   }
   QtminusM(event:any){
     if(this.QtM>0){
    this.QtM =this.QtM-1
   
     }else{
      this.QtM=+this.QtM
     
     }

   }
   //quantité de la taille S
   QtplusS(event:any){
    this.QtS =+this.QtS+1
    console.log(event)
  }
  QtminusS(event:any){
    if(this.QtS>0){
   this.QtS =+this.QtS-1
    }else{
     this.QtS=+this.QtS
    }
  }
   //quantité de la taille L
   QtplusL(event:any){
    this.QtL =+this.QtL+1
    console.log(event)
  }
  QtminusL(event:any){
    if(this.QtL>0){
   this.QtL =+this.QtL-1
    }else{
     this.QtL=+this.QtL
    }
  }
   //quantité de la taille XL
   QtplusXL(event:any){
    this.QtXL =+this.QtXL+1
    console.log(event)
  }
  QtminusXL(event:any){
    if(this.QtXL>0){
   this.QtXL =+this.QtXL-1
    }else{
     this.QtXL=+this.QtXL
    }
  }

  Addtocart =()=>{
    var cart:any;
    var qtys=this.QtM + this.QtL +this.QtS + this.QtXL
  
    if(this.oui && this.viewimage!=undefined && this.ViewImg!=undefined){
       cart ={ 
        type_product:"crea",
        t:+this.Price*qtys,
        category:"vetement",
        name:this.file_type,
        face1:this.url,
        face2:this.imagepreview,
        f3:this.viewimage,
        f4:this.ViewImg,
        type:this.type,
        size:({
          m:this.QtM,
          l:this.QtL,
          s:this.QtS,
          xl:this.QtXL,
          crea:true
        }),
        qty:qtys,
        price:this.Price
      }
      //type tee-shirt
     
  
      console.log(qtys);
    }else{
      

       cart ={
        type_product:"crea",
        category:"vetement",
        name:this.file_type,
         t:+this.Price * qtys,
        face1:this.url,
        face2:null,
        f3:this.viewimage,
        f4:null,
        size:({
          m:this.QtM,
          l:this.QtL,
          s:this.QtS,
          xl:this.QtXL
        }),
        type:this.type,
        qty:qtys,
        price:this.Price
      }
  
      }
      
  
     
    
    try{
      cart = compress(cart)
     if(this.viewimage!=undefined && this.type!=undefined){
          this.localservice.adtocart(cart).then((res)=>{

            if(res==false){
              myalert.fire({
                title:'<strong>Désolé !!!!</strong>',
                icon:'error',
                html:
                  '<h6 style="color:blue">votre fichier est tro lourd</h6> ' 
                  ,
                showCloseButton: true,
                focusConfirm: false,
               
              })
            }else{
              myalert.fire({
                title:'<strong>produit ajouté</strong>',
                icon:'success',
                html:
                  '<h6 style="color:blue">Felicitation</h6> ' +
                  '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
                  '<a href="/cart">Je consulte mon panier</a>' 
                  ,
                showCloseButton: true,
                focusConfirm: false,
               
              })

            }
            
          }).catch(
            (err)=>{
              console.log(err)
              

              
            }
          );
         
        
      
      
     }else{
       if(this.viewimage==undefined){
        myalert.fire({
          title: "Désolé!!!",
          text: "Veuillez importer votre maquette" ,
          icon: "error",
           button: "Ok"
          }); 
       }
     }
    }catch(e:any){
      console.log(e)
    }
    //location.reload()
  }

 //prix fix
 affprice(event:any){
    this.hiderow=true 
   this.Price=this.prix
   this.promo1=true
   this.promo=false
   this.shirtwhite=true
   this.shirtcolor=false
   this.polo=false
   this.shirtordinary=false
   this.shirtpolycoton=false
   this.ispricetcolor=false;
   this.isprix=true;
    this.ispricepolycoton=false;
    this.ispriceordinary=false;
    this.ispricepolo=false
    if(this.borderie){
      this.Price=+this.Price+((+this.mybodr)*500)
    }
    if(this.flex){
      if(this.shirtwhite){
        this.Price=this.prix + 500
      }
      if(this.shirtcolor){
        this.Price=this.prixtcolor + 1000
      }
      if(this.polo){
        this.Price=this.pricepolo + 1000
      }
      if(this.avecphotochild){
        this.Price=parseInt(this.Price)+this.priceAPchild
      }

      if(this.avecphotogrand){
        this.Price=parseInt(this.Price)+this.priceAPgrand
      }


      if(this.sansphotochild){
        this.Price=parseInt(this.Price)+this.priceSPchild
      }

      if(this.sansphotogrand){
        this.Price=parseInt(this.Price)+this.priceSPgrand
      }
    }

   
    if(this.serie){
      // this.isprix=!this.isprix
      if(parseInt(this.nbcolorserie)>1){
        this.Price=+this.Price + 1000;

      }
      if(parseInt(this.nbcolorserie)==1){
        // this.isprix=!this.isprix
         this.Price=+this.Price + 500;
   
     }

  
   }

   if(this.suble){
    this.Price=+this.Price+this.pricesuble
  }

   if(this.transft){
    this.Price=this.Price+this.pricetransfert
  }
   
 }
 showpricetcolor(event:any){

  this.hiderow=true 

  this.promo1=true
   this.promo=false
   this.Price=this.prixtcolor
   this.shirtwhite=false
   this.shirtcolor=true
   this.polo=false
   this.shirtordinary=false
   this.shirtpolycoton=false

    this.ispricetcolor=true;
    this.isprix=false;
    this.ispricepolycoton=false;
    this.ispriceordinary=false;
    this.ispricepolo=false

    if(this.flex){
      if(this.shirtwhite){
        this.Price=this.prix + 500
      }
      if(this.shirtcolor){
        this.Price=this.prixtcolor + 1000
      }
      if(this.polo){
        this.Price=this.pricepolo + 1000
      }
      if(this.avecphotochild){
        this.Price=parseInt(this.Price)+this.priceAPchild
      }

      if(this.avecphotogrand){
        this.Price=parseInt(this.Price)+this.priceAPgrand
      }


      if(this.sansphotochild){
        this.Price=parseInt(this.Price)+this.priceSPchild
      }

      if(this.sansphotogrand){
        this.Price=parseInt(this.Price)+this.priceSPgrand
      }
    }

    if(this.serie){
      // this.isprix=!this.isprix
      if(parseInt(this.nbcolorserie)>1){
        this.Price=+this.Price + 1000;

      }
      if(parseInt(this.nbcolorserie)==1){
        // this.isprix=!this.isprix
         this.Price=+this.Price + 500;
   
     }

  
   }


 if(this.suble){
  this.Price=+this.Price+this.pricesuble
}

if(this.borderie){
  this.Price=+this.Price+((+this.mybodr)*500)
}
 if(this.transft){
  this.Price=this.Price+this.pricetransfert
}
  
 }
 showpricepolo(event:any){
  this.hiderow=true 
  this.promo1=true
   this.promo=false
  this.Price=this.pricepolo 
  this.polo=true
  this.shirtordinary=false
  this.shirtpolycoton=false
  this.shirtwhite=false
  this.shirtcolor=false
    this.ispricetcolor=false;
    this.isprix=false;
    this.ispricepolycoton=false;
    this.ispriceordinary=false;
    this.ispricepolo=true
    if(parseInt(this.nbcolorserie)==1 && this.serie){
      // this.isprix=!this.isprix
       this.Price=+this.Price + 500;
 
   }

   if(this.borderie){
    this.Price=+this.Price+((+this.mybodr)*500)
  }

   if(this.flex){
    if(this.shirtwhite){
      this.Price=this.prix + 500
    }
    if(this.shirtcolor){
      this.Price=this.prixtcolor + 1000
    }
    if(this.polo){
      this.Price=this.pricepolo + 1000
    }
    if(this.avecphotochild){
      this.Price=parseInt(this.Price)+this.priceAPchild
    }

    if(this.avecphotogrand){
      this.Price=parseInt(this.Price)+this.priceAPgrand
    }


    if(this.sansphotochild){
      this.Price=parseInt(this.Price)+this.priceSPchild
    }

    if(this.sansphotogrand){
      this.Price=parseInt(this.Price)+this.priceSPgrand
    }
  }

   if(parseInt(this.nbcolorserie)>1 &&  this.serie){
    // this.isprix=!this.isprix
     this.Price=+this.Price + 1000;


 }

 if(this.suble){
  this.Price=+this.Price+this.pricesuble
}
 if(this.transft){
  this.Price=this.Price+this.pricetransfert
}
   
 }
 showpricetordi(event:any){
  this.hiderow=true 
  this.promo1=true
   this.promo=false
  this.Price=this.pricetordinary
  this.polo=true
  this.shirtordinary=true
  this.shirtpolycoton=false
  this.shirtwhite=false
  this.shirtcolor=false
  this.ispricetcolor=false;
  this.isprix=false;
  this.ispricepolycoton=false;
  this.ispriceordinary=true;
  this.ispricepolo=false

  if(this.borderie){
    this.Price=+this.Price+((+this.mybodr)*500)
  }
   

  if(this.serie){
    // this.isprix=!this.isprix
    if(parseInt(this.nbcolorserie)>1){
      this.Price=+this.Price + 1000;

    }
    if(parseInt(this.nbcolorserie)==1){
      // this.isprix=!this.isprix
       this.Price=+this.Price + 500;
 
   }


 }



   if(this.suble){
     this.Price=+this.Price+this.pricesuble
   }

   if(this.flex){
    if(this.avecphotochild){
      this.Price=parseInt(this.Price)+this.priceAPchild
    }

    if(this.avecphotogrand){
      this.Price=parseInt(this.Price)+this.priceAPgrand
    }


    if(this.sansphotochild){
      this.Price=parseInt(this.Price)+this.priceSPchild
    }

    if(this.sansphotogrand){
      this.Price=parseInt(this.Price)+this.priceSPgrand
    }
  }


 if(this.transft){
  this.Price=this.Price+this.pricetransfert
}

   
 }


 showpricecoton(event:any){
  this.promo1=true
  this.promo=false
  this.hiderow=true 
  this.Price=this.pricetpolycoton
  this.polo=true
  this.shirtordinary=false
  this.shirtpolycoton=true
  this.shirtwhite=false
  this.shirtcolor=false
    this.ispricetcolor=false;
    this.isprix=false;
    this.ispricepolycoton=true;
    this.ispriceordinary=false;
    this.ispricepolo=false;


    if(this.suble){
      this.Price=+this.Price+this.pricesuble;
    }


    if(this.borderie){
      this.Price=+this.Price+((+this.mybodr)*500)
    }

    if(this.flex){
      if(this.avecphotochild){
        this.Price=parseInt(this.Price)+this.priceAPchild;
      }

      if(this.avecphotogrand){
        this.Price=parseInt(this.Price)+this.priceAPgrand;
      }


      if(this.sansphotochild){
        this.Price=parseInt(this.Price)+this.priceSPchild;
      }

      if(this.sansphotogrand){
        this.Price=parseInt(this.Price)+this.priceSPgrand;
      }
    }
    

   


   if(this.transft){
     this.Price=this.Price+this.pricetransfert
   }



   if(this.serie){
    if(parseInt(this.nbcolorserie)>1){
      this.Price=+this.Price + 1000;

    }
    if(parseInt(this.nbcolorserie)==1){
       this.Price=+this.Price + 500;
 
   }


 }


   
 }
 //prix de choix avec photo ou sans
 showpriceAPchild(event:any){
   if(this.shirtcolor || this.shirtwhite){
    this.promo1=false
    this.promo=true
   }else{
    this.promo1=true
    this.promo=false
   }
    
   if(this.cmpte==0 && !this.avecphotochild){
    this.cmpte=this.cmpte+1
    this.avecphotochild=true
    
    if(this.sansphotochild){
      this.Price= (parseInt(this.Price)-this.priceSPchild) + this.priceAPchild 
      this.newprice= this.Price - (this.pricepromo)
      this.sansphotochild=false
    }else{

      this.Price= parseInt(this.Price) + this.priceAPchild 
      this.newprice= this.Price - (this.pricepromo)
     }
   }

   if(this.cpt>=1&& this.sansphotochild){
    this.cmpte=this.cmpte+1
     this.avecphotochild=true
     this.Price= (parseInt(this.Price)-this.priceSPchild) + this.priceAPchild
     this.newprice=this.Price - this.pricepromo
     this.sansphotochild=false
     
  }

 
 }

 showpriceSPchild(event:any){
  if(this.shirtcolor || this.shirtwhite){
    this.promo1=false
    this.promo=true
   }else{
    this.promo1=true
    this.promo=false
   }
   if(this.cpt==0 && !this.sansphotochild){
      this.cpt=this.cpt+1
      this.sansphotochild=true
      if(this.avecphotochild){
        this.Price= (parseInt(this.Price)-this.priceAPchild) + this.priceSPchild 
        this.newprice=this.Price - this.pricepromo
        this.avecphotochild=false
      }else{
  
        this.Price= parseInt(this.Price) + this.priceSPchild ;
        this.newprice=this.Price - this.pricepromo
       }
    
   }

   if(this.cmpte>=1&&this.avecphotochild){
    this.cpt=this.cpt+1;
     this.sansphotochild=true
     this.Price= (parseInt(this.Price)-this.priceAPchild) + this.priceSPchild
     this.newprice=this.Price - this.pricepromo
     this.avecphotochild=false
     
  }
  
 }


 //prix de choix avec photo ou sans
 showpriceAPGperson(event:any){
  if(this.shirtcolor || this.shirtwhite){
    this.promo1=false
    this.promo=true
   }else{
    this.promo1=true
    this.promo=false
   }
   if(this.cptwithg==0 && !this.avecphotogrand){
     this.cptwithg=this.cptwithg+1
     this.avecphotogrand=true
    if(this.sansphotogrand){
      this.Price= (parseInt(this.Price)-this.priceSPgrand) + this.priceAPgrand;
      if(this.shirtwhite || this.shirtcolor){
        this.newprice=this.Price - this.pricepromo
      }
     
      this.sansphotogrand=false;
     }else{

      this.Price= parseInt(this.Price)+ this.priceAPgrand;
      if(this.shirtwhite || this.shirtcolor){
        this.newprice=this.Price - this.pricepromo
      }
     
     }
   }

   if(this.cptwthng>=1&& this.sansphotogrand){
    this.cptwithg= this.cptwithg+1;
     this.avecphotogrand=true;
     this.Price= (parseInt(this.Price)-this.priceSPgrand) + this.priceAPgrand;
     if(this.shirtwhite || this.shirtcolor){
      this.newprice=this.Price - this.pricepromo
    }
     this.sansphotogrand=false;   
  }

 }


 showpriceSPGperson(event:any){
  if(this.shirtcolor || this.shirtwhite){
    this.promo1=false
    this.promo=true
   }else{
    this.promo1=true
    this.promo=false
   }
   if(this.cptwthng==0 && !this.sansphotogrand){
    this.cptwthng=this.cptwthng+1
    this.sansphotogrand=true
    if(this.avecphotogrand){
     this.Price= (+this.Price-this.priceAPgrand) + this.priceSPgrand
     if(this.shirtwhite || this.shirtcolor){
      this.newprice=this.Price - this.pricepromo
    }
     this.avecphotogrand=false
    }else{
     this.Price=parseInt(this.Price) + this.priceSPgrand
     if(this.shirtwhite || this.shirtcolor){
      this.newprice=this.Price - this.pricepromo
    }
    }
   
   }
 
   if(this.cptwithg>=1 && this.avecphotogrand){
    this.cptwthng=this.cptwthng+1;
    this.sansphotogrand=true
    this.Price= (parseInt(this.Price)-this.priceAPgrand) + this.priceSPgrand
    if(this.shirtwhite || this.shirtcolor){
      this.newprice=this.Price - this.pricepromo
    }
     this.avecphotogrand=false
   
   
   }
 
 }


 //type dimpression
showtransfert(){
  
    if(!this.flex && !this.serie && !this.borderie && !this.suble && !this.transft){ 
      this.Price=this.Price + this.pricetransfert    
      this.transft=true
      this.suble=false
      this.hide=false
      this.serie=false
      this.isbord=false
      this.hidechoice=false
      this.flex=false
      this.borderie=false
      this.hidechildopt=false
      this.hidepersonopt=false


    }
    

    if(this.suble){
      
      if(this.isprix){
        this.Price= +this.prix + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.isbord=false

        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false
      }
  

      if(this.ispricetcolor){
        this.Price=+this.prixtcolor + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.isbord=false

        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false

      }

      if(this.ispricepolo){
        this.Price=+this.pricepolo + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.isbord=false

        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false

      }

      if(this.ispricepolycoton){
        this.Price= +this.pricetpolycoton + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.isbord=false

        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false

      }
      

      if(this.ispriceordinary){
        this.Price=+this.pricetordinary + this.pricetransfert;
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.isbord=false

        this.hidechoice=false
        this.flex=false
        this.borderie=false 
        this.hidechildopt=false
         this.hidepersonopt=false

      }


    }
    
    if(this.flex){
      if(this.isprix){
        this.Price= +this.prix + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false
       this.isbord=false

      }
  

      if(this.ispricetcolor){
        this.Price=+this.prixtcolor + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false
        this.isbord=false


      }

      if(this.ispricepolo){
        this.Price=+this.pricepolo + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
        this.hidepersonopt=false
        this.isbord=false


      }

      if(this.ispricepolycoton){
        this.Price= +this.pricetpolycoton + this.pricetransfert    
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false
        this.hidechildopt=false
       this.hidepersonopt=false
       this.isbord=false


      }  

      if(this.ispriceordinary){
        this.Price=+this.pricetordinary + this.pricetransfert;
        this.transft=true
        this.suble=false
        this.hide=false
        this.serie=false
        this.hidechoice=false
        this.flex=false
        this.borderie=false 
        this.hidechildopt=false
         this.hidepersonopt=false
         this.isbord=false


      }

    }

  

   if(this.serie){
      if(this.nbcolorserie>1){
        this.Price= (parseInt(this.Price)-1000) + this.pricetransfert; 
        this.transft=true;
        this.suble=false;
        this.hide=false;
        this.serie=false;
        this.hidechoice=false;
        this.flex=false;
        this.borderie=false;
        this.isbord=false

      }

      if(this.nbcolorserie==1){
        this.Price= (parseInt(this.Price)-500) + this.pricetransfert;
        this.transft=true;
        this.suble=false;
        this.hide=false;
        this.serie=false;
        this.hidechoice=false;
        this.flex=false;
        this.isbord=false
        this.borderie=false;
      }
    
    }
    
    if(this.borderie){
      this.Price= (parseInt(this.Price)-((+this.mybodr)*500)) + this.pricetransfert;
        this.transft=true;
        this.suble=false;
        this.hide=false;
        this.serie=false;
        this.hidechoice=false;
        this.flex=false;
        this.isbord=false
        this.borderie=false;

    }

  
}


ChangeComponent(value:any){
  this.showall=!this.showall;
  
}

showsublime(){
  
  if(this.ispriceordinary){
    this.Price=+this.pricetordinary + this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }


  if(this.ispricepolo){
    this.Price=+this.pricepolo+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }


  if(this.isprix){
    this.Price=+this.prix+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }



  if(this.ispricetcolor){
    this.Price=+this.prixtcolor+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }


  if(this.ispricepolycoton){
    this.Price=+this.pricetpolycoton+ this.pricesuble
    this.transft=false
    this.suble=true
    this.hidechoice=false
    this.serie=false
    this.flex=false,
    this.hide=false
    this.borderie=false
    this.hidepersonopt=false;
    this.hidechildopt=false;
    this.isbord=false

  }
  
}



}
