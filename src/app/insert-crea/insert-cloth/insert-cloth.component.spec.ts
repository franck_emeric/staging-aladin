import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertClothComponent } from './insert-cloth.component';

describe('InsertClothComponent', () => {
  let component: InsertClothComponent;
  let fixture: ComponentFixture<InsertClothComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertClothComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertClothComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
