import { SharedModule } from './../shared/shared.module';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImprimerRoutingModule } from './imprimer-routing.module';
import { ImprimeComponent } from './imprime/imprime.component';
import { InsertCreaModule } from '../insert-crea/insert-crea.module';
import { SharededitorModule } from '../sharededitor/sharededitor.module';


@NgModule({
  declarations: [
    ImprimeComponent,
  ],
  imports: [
    CommonModule,
    ImprimerRoutingModule,
    SharedModule,
    InsertCreaModule,
    SharededitorModule
  ],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ImprimerModule { }
