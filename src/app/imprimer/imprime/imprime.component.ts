import { Component, OnInit } from '@angular/core';
import { ListService , AladinService, ObservableserviceService} from 'src/app/core';

import {decompress} from "compress-json"
@Component({
  selector: 'app-imprime',
  templateUrl: './imprime.component.html',
  styleUrls: ['./imprime.component.scss']
})
export class ImprimeComponent implements OnInit {
  printed:any;
  cacheprinted=true
  cacheimport=false
  hideprice:Boolean=true
  hidepromo:Boolean=false
  imagepreview :any
  list_of_prod:any
  CacheCrea=false
 cacheClothes=true;
 details:any={};
 dt:any;
 editor=false;
 list_data:any
  constructor(private l :ListService, private uplod:AladinService,private O:ObservableserviceService) { }

  ngOnInit(): void {
    this.l.getDesigns("imprimes").subscribe(
      res=>{
      this.list_of_prod=res
      if(this.list_of_prod.status){
        this.list_of_prod=this.list_of_prod.data
        for (let item of this.list_of_prod){
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls= JSON.parse(item.urls);
          this.list_of_prod[this.list_of_prod.indexOf(item)].urls=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].urls)
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=JSON.parse(item.product_design_data);
          this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data=decompress(this.list_of_prod[this.list_of_prod.indexOf(item)].product_design_data)

        }

      }
      },
      err=>{
        console.log(err)
      }
    )
    this.uplod.ShowEditor.subscribe(res=>{
      if(res.data.show){
         this.editor = res.data.show;
         this.list_data= res.list;
         this.dt= res.data
      }
    })

    this.O.pr_m.subscribe(res=>{
      if(res){
        this.editor=false;
      }
    })
/*
    this.l.getPrints().subscribe(
      res=>{
        this.printed=res
      },
      error=>{
        console.log(error);
      }
    )*/
  }
  showcacheimport(){
    this.cacheprinted=false
    this.cacheimport=true
  }
  View(){
    let view = document.getElementById('view');
    view?.scrollIntoView({behavior:"smooth"})
   }

   Upload(event:any){
    let file =event.target.files[0]
    if(!this.uplod.UpleadImage(file)){
    const reader = new FileReader();
     reader.onload = () => {
  
   this.imagepreview = reader.result;
   
   };
   
   reader.readAsDataURL(file);
   //this.showimportcrea()
   this.showcacheimport()
    console.log(file)
  }else{
   
    }
  
   }
   
showDetails(data:any){
  console.log(data)
  Object.assign(data,{show:true})
  this.details=data
  this.cacheprinted=false
  this.cacheimport=false
  
 }
}
