import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ObservableserviceService } from 'src/app/core';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
data:any
searchItem=false
@Input()editor:any
@Output() ChangeBackground = new EventEmitter<any>()
  list_for_editor=false;
  constructor(private o:ObservableserviceService) { }

  ngOnInit(): void {
  if(this.editor==true){
  this.list_for_editor=true;
  }
    this.o.searchResult.subscribe(res=>{
      this.data=res;
      this.searchItem=true
    })
  }

  changeIt(item:any){
    this.ChangeBackground.emit(item)
  }

}
