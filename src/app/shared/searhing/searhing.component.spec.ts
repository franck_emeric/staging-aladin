import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearhingComponent } from './searhing.component';

describe('SearhingComponent', () => {
  let component: SearhingComponent;
  let fixture: ComponentFixture<SearhingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearhingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearhingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
