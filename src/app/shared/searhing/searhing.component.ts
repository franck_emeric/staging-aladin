import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Input, OnInit ,SimpleChanges,OnChanges} from '@angular/core';
import { ObservableserviceService } from 'src/app/core';
@Component({
  selector: 'app-searhing',
  templateUrl: './searhing.component.html',
  styleUrls: ['./searhing.component.scss']
})
export class SearhingComponent implements OnInit,OnChanges {
  product:any=[];
  @Input() data:any=[];
  search:any="";
  searchItem=false
  search_bar=true;
  constructor(private o:ObservableserviceService) { }

  ngOnInit(): void {
    this.o.out_detail.subscribe(rs=>{
      this.search_bar=true;
      this.search=""
      
    })
    this.o.detail.subscribe(res=>{
      this.search_bar=false;
      this.search=""
    })
  }
  

 

  OnInput(){
    if(this.search!=""){
      this.product=this.data.filter((product:any) => {
        if (
          product.product_name.toLowerCase().includes(this.search.toLowerCase()) ||
          product.productype_label.toLowerCase().includes(this.search.toLowerCase())||
          product.category_label.toLowerCase().includes(this.search.toLowerCase())
  
        ) {
          return product
        
        }
      });
        this.o.searchResult.next(this.product)
      
    }
  
  }


  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    this.data=changes.data.currentValue;
    
  }
}
