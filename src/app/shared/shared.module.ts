import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { Header3Component } from './layout/header3/header3.component';
import { Home1Component } from './layout/home1/home1.component';
import { HeaderUPComponent } from './layout/header-up/header-up.component';
import { FormsModule } from '@angular/forms';
import { FooterDashboardComponent } from './layout/footer-dashboard/footer-dashboard.component';
import { CartitemsComponent } from './cartitems/cartitems.component';
import { DescclothesComponent } from './layout/descclothes/descclothes.component';
import { DescpackagesComponent } from './layout/descpackages/descpackages.component';
import { DescriptionprintComponent } from './layout/descriptionprint/descriptionprint.component';
import { DescriptiongadgetComponent } from './layout/descriptiongadget/descriptiongadget.component';
import { DescriptiondispsComponent } from './layout/descriptiondisps/descriptiondisps.component';
import { SlidingComponent } from './sliding/sliding.component';
import { SearhingComponent } from './searhing/searhing.component';
import { InsertCreaModule } from '../insert-crea/insert-crea.module';
import { ListProductComponent } from './list-product/list-product.component';
import { SearchResultComponent } from './search-result/search-result.component';


@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    Header3Component,
    Home1Component,
    HeaderUPComponent,
    FooterDashboardComponent,
    CartitemsComponent,
    DescclothesComponent,
    DescpackagesComponent,
    DescriptionprintComponent,
    DescriptiongadgetComponent,
    DescriptiondispsComponent,
    SlidingComponent,
    SearhingComponent,
    ListProductComponent,
    SearchResultComponent,
  
  ],
  imports: [
    CommonModule,
    FormsModule,
    InsertCreaModule
  ],
  exports:[
    FooterComponent,
    HeaderComponent,
    Header3Component,
    Home1Component,
    HeaderUPComponent,
    DescpackagesComponent,
    FormsModule,
    CommonModule,
    FooterDashboardComponent,
    CartitemsComponent,
    SlidingComponent,
    SearhingComponent,
    ListProductComponent,
    SearchResultComponent


  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
