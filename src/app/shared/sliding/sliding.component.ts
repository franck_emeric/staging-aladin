import { Component, Input, OnInit ,SimpleChanges,OnChanges} from '@angular/core';

@Component({
  selector: 'app-sliding',
  templateUrl: './sliding.component.html',
  styleUrls: ['./sliding.component.scss']
})
export class SlidingComponent implements OnInit,OnChanges {
  slideIndex = 0;
  timer = 7; // sec
  _timer = this.timer;
  @Input() data:any=[];
  constructor() { }

  ngOnInit(): void {
   
    setTimeout(() => { 

      this.showSlides();
      this.timer--;
      if (this.timer < 1) {
        this.nextSlide();
        this.timer = this._timer; // reset timer
      }

    },10);   
  
  }
  nextSlide() {
    this.slideIndex++;
    this.showSlides();
    this.timer = this._timer; // reset timer
  }

  currentSlide(n:any) {
    this.slideIndex = n - 1;
    this.showSlides();
    this.timer = this._timer;
  }
  prevSlide() {
    this.slideIndex--;
   this. showSlides();
    this.timer = this._timer;
  }

  showSlides() {
    let slides = document.querySelectorAll(".mySlides");
    let dots = document.querySelectorAll(".dots");
     console.log(slides)
    if (this.slideIndex > slides.length - 1) this.slideIndex = 0;
    if (this.slideIndex < 0) this.slideIndex = slides.length - 1;
    
    // hide all slides
    slides.forEach((slide) => {
      slide.setAttribute("style","display:none")
    });
    
    // show one slide base on index number
    if(slides[this.slideIndex])
    slides[this.slideIndex].setAttribute("style","display:block");
    
    dots.forEach((dot) => {
      dot.classList.remove("active");
    });
    if(dots[this.slideIndex])
    dots[this.slideIndex].classList.add("active");
  }
  
  
  
 ngOnChanges(changes: SimpleChanges): void {
   //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
   //Add '${implements OnChanges}' to the class.
   for(let i=0;i<this.data.length;i++){
    if(this.data[i]==null){
      this.data.splice(i,1)
    }
  }
   
 }

}
