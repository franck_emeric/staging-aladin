import { Component, OnInit,Input } from '@angular/core';
import { LoginService,FormvalidationService ,AuthinfoService,LocalService,ListService,CartService, ObservableserviceService} from 'src/app/core';
import { Router } from '@angular/router';
import {decompress} from "compress-json"
import { TokenService } from 'src/app/core/auth/tokens/token.service';
declare var require: any
var $ = require("jquery");
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
@Input() list_of_prod:any=[]
 
  data={
   email:"",
   password:"",
   erroremail:false,
   errorpwd:false,
   pwdmsg:"",
   emailmsg:""
 }
 URL="/users/"
 message="";
 user:any;
 name:any;
 ID:any
 @Input() cart_items:number=0;
 isauth=false;
 show:boolean=false;
 auth_data:any;
 @Input() fromhome=false;
 
 friday:Boolean=false
  constructor(private loginservice:LoginService,private valideform:FormvalidationService,private route:Router,private authuser:AuthinfoService,private ls :LocalService,private triger:ListService,private cartInfo: CartService,private o:ObservableserviceService,private tokenService:TokenService) { }
  jday= new Date().getDay()
  ngOnInit(): void {
    this.o.onload.subscribe(res=>{
      this.list_of_prod=res;
    })
       if(this.jday==5){
      this.friday=true
    }
    this.cart_items=this.ls.cart_items.length;
     this.plus();

     this.tokenService.getUser().then(res=>{
       this.name= res.first_name + " " + res.last_name
       this.isauth=true;
     })
     
  }


  toggleSpinner(){
    this.show = !this.show;

  }


  plus(){
    this.cartInfo.cartUpdated.subscribe(
      cart_length=>{
          this.cart_items=cart_length
       
      }
    )
  }

  login(event:Event){
    if(this.valideform.validateEmail(this.data.email)){
      if(this.data.password!=null){
        if(this.show){
          this.toggleSpinner()
        }
        this.toggleSpinner();
        this.loginservice.login(this.data).subscribe(res=>{
        var resp:any=res
          if(res.status){    
            var data:any={
              user_id:resp.data.user_id,
              email:resp.data.email,
              first_name:resp.data.first_name||" ",
              last_name:resp.data.last_name|| " ",
              code:resp.data.code,
              code_status:resp.data.code_status,
              phone:resp.data.phone
          }

          try{
            this.tokenService.saveUser(data)
            this.tokenService.saveRefreshToken(resp.data.Refresh_Token)
            this.tokenService.saveToken(resp.data.Access_Token)
            this.name=data.first_name +" "+ data.last_name
            this.isauth=true
             location.href= "/"
          }catch(err){
            console.log(err)
          }

         }

         this.toggleSpinner();
         this.data.erroremail=!this.data.erroremail;
         this.message=res.message;   
        
        },err=>{
          this.toggleSpinner();

           if(err.error.text!=undefined){
           this.data.erroremail=!this.data.erroremail;
           this.data.emailmsg=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.data.errorpwd=!this.data.errorpwd;
            this.data.pwdmsg=err.error.password;   
          }
          if(this.data.erroremail!=true&&this.data.errorpwd!=true){
          console.log("Une erreur  s'est produite veuillez contactez le service client au 27 23 45 73 02")
          }   
        });
      }
    }else{
      this.data.emailmsg="invalide email";
      this.data.erroremail=!this.data.erroremail;

    }

  }

  logout(event:any){
     this.tokenService.signOut()
  }

 Scroll(){
   let view=document.getElementById('sommes')
   view?.scrollIntoView({behavior:'smooth'})
 }


 gotocart(){
  this.ls.activatecart.next(true);

 }



 gohome(){
   this.ls.activatecart.next(false);
 }

 

}
