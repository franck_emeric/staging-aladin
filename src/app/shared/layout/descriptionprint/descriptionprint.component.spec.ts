import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionprintComponent } from './descriptionprint.component';

describe('DescriptionprintComponent', () => {
  let component: DescriptionprintComponent;
  let fixture: ComponentFixture<DescriptionprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescriptionprintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
