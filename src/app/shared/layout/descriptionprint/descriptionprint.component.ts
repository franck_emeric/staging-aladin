import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { compress } from 'compress-json';
import { AladinService, ListService, ObservableserviceService } from 'src/app/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-descriptionprint',
  templateUrl: './descriptionprint.component.html',
  styleUrls: ['./descriptionprint.component.scss']
})
export class DescriptionprintComponent implements OnInit {
  @Output() changeComponent=new EventEmitter<boolean>();
  @Input() data:any;
  Changevalue=true;
  hideprice:Boolean=true
  hidepromo:Boolean=false
  chap=true
  qualite:any=["Standard","Superieur"]
  pellicule:any=["Mat","Brillant"]
  bordure:any=["carrée","rond"]
  type_impr:any
  pelli:any
  bord:any
  qty:any=100;
  t:any
  a4=false
  a5=false
  a6=false
  souches1=false
  souches2=false
  souches3=false
  format:any
  @Input() list:any
  souche:any
  prnt:any=[]
  shw=false;
  total_cost=0.0
  price=0.0
  fmt:any=[]
  type:any=" "
  constructor(private aladin:AladinService,private O:ObservableserviceService,private api:ListService) { }

  ngOnInit(): void {
   if(this.data.productype_label!="carnet"){
    this.api.getprinting("imprimes").subscribe(
      res=>{
       var dt:any=res;
       if(dt.status){
         this.prnt= dt.data;
       }
      }
    )
   }

   if(this.data.productype_label==="carnet"){
    this.api.getFormat("imprimes").subscribe(
      res=>{
       var dt:any=res;
       this.qty=1

       if(dt.status){
         this.fmt= dt.data;
       }
      }
    )
   }
   
  }

  ngOnChanges(): void {
    if(this.data.productype_label=="carte de visite"){
      this.qty=100
    }
}


reload(value:boolean){
  this.changeComponent.emit(value);
  this.data.show=false;
}

getFormat(item:any){
  if(parseInt(this.qty)>0){
    this.total_cost= item.product_price_format*parseInt(this.qty)
    this.price =item.product_price_format
    this.shw=true
    this.type="carnet"
    this.format= item.other_format
  }

}
showa4(){
  this.a4=true
  this.a5=false
  this.a6=false
  this.format="A4"
}
showa5(){
  this.a4=false
  this.a5=true
  this.a6=false
  this.format="A5"
}
showa6(){
  this.a4=false
  this.a5=false
  this.a6=true
  this.format="A6"
}
showsouche1(){
  this.souches1=true
  this.souches2=false
  this.souches3=false
  this.souche="2 souches"
}
showsouche2(){
  this.souches1=false
  this.souches2=true
  this.souches3=false
  this.souche="3 souches"
}
showsouche3(){
  this.souches1=false
  this.souches2=false
  this.souches3=true
  this.souche="4 souches"
}


onloadqualite(item:any){
  this.shw=true;
  for(let el of this.prnt){
    if(el.printing_id==item.target.value){
      this.type_impr=el.printing_type_label
      this.price=el.printing_price
      this.total_cost= (this.qty/100)*el.printing_price
      this.total_cost=Number((this.total_cost).toFixed(2));
      break
    }
  }
  
}

onloadbordure(event:any){
  if(event.target.value==this.bordure[0]){
    this.bord=this.bordure[0]
  }
  if(event.target.value==this.bordure[1]){
    this.bord=this.bordure[1]
  }
}

onloadpelicule(event:any){
  if(event.target.value==this.pellicule[0]){
    this.pelli=this.pellicule[0]
  }
  if(event.target.value==this.pellicule[1]){
    this.pelli=this.pellicule[1]
  }
}

  go(event:any){
      Object.assign(this.data,{
        show:true,
        category:"printed",
        price:this.price,
        type_impr:this.type_impr,
        pelicule:this.pelli,
        bordure: this.bord,
        qty:this.qty,
        format: this.format||null + " " + this.souche||" ",
        t:this.total_cost
     
    })
    if(!this.format){
      this.data.format=null
    }

    if(this.data.t>0 && this.qty>0&&  this.price){
      this.aladin.ShowEditor.next({data:this.data,list:this.list})

    }else{
      if(this.data.t==0 || this.qty==0 ){
        Swal.fire({
          icon:"warning",
          text:"renseignez les caracteristiques du produit svp !!"
        })
      }
    }
    
  }
  plus(){
    if(this.type!="carnet"){
      this.qty=this.qty+100
      this.total_cost= this.price * (this.qty/100);
      this.total_cost=Number((this.total_cost).toFixed(2));
  
    }

   if(this.type==="carnet"){
      this.qty=this.qty+1
      this.total_cost= this.price*this.qty;

    }

  }
  moins(){
  if(this.type!="carnet"){
    if(this.qty>100){
      this.qty=this.qty - 100
    }else{
      this.qty=this.qty
    }
    this.total_cost= this.price * (this.qty/100);
    this.total_cost=Number((this.total_cost).toFixed(2));
  }
    if(this.type==="carnet"){

      if(this.qty>1){
       this.qty=this.qty-1
      }
      this.total_cost= this.price * this.qty;

    }
  }
  
  back(){
    this.O.out_detail.next({print:true})
  }
}
