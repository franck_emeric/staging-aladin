import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
import { Subscription, interval } from 'rxjs';
import {decompress} from 'compress-json';
import { ObservableserviceService } from 'src/app/core';
import { AladinService } from 'src/app/core';
import scrollIntoView from 'smooth-scroll-into-view-if-needed'
declare var require:any
var $ =require("jquery") 
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
  subscription:any= Subscription
   products:any={}
   tops:any;
   clothes:any=[]
   gadget:any
   packs:any
   disps:any
   print:any
   details:any={}
   produit: any = [];
   packages:any=[]
   display:any=[]
   gadgets:any=[]
   printed:any=[]
   hide=false
   hidepack=false
   hideprint=false
   hidebody=true
   hidedisps:Boolean=false
   hidegadgets:Boolean=false
   Jour = new Date().getDay()
   cacheBulle:Boolean=false
   hidepromo:Boolean=false
   hideprice:Boolean=true
   dateNow = new Date();
   dDay = new Date('Nov 24 2021 00:00:00');
   milliSecondsInASecond = 1000;
   hoursInADay = 24;
   minutesInAnHour = 60;
   SecondsInAMinute  = 60;
   timeDifference:any
   secondsToDday:any
   minutesToDday:any
   hoursToDday:any
   daysToDday:any
   urls:any=[];
   alldata:any=[];
   CacheCrea=false;
   url:any;
   imagepreview:any;
   iscloth=true;
   ispack=true;
   isdisp=true;
   isgad=true;
   isprint=true;
   issearch=false;
   show_menu=true;
   cloths=true
   packags=true
   dip=true
   gadge=true
  printe=true
  model=true
  hidecrea=true
  constructor(private l : ListService,private o:ObservableserviceService,private uplod:AladinService) { }

  ngOnInit(): void {
   this.o.out_detail.subscribe(
     res=>{
           this.iscloth=true
           this.isprint=true
           this.ispack=true;
           this.isgad=true;
           this.isdisp=true;
           this.issearch=false

     }
   )
  
  this.o.upload_cloth.subscribe(res=>{
    this.cloths=true
   this.packags=false
   this.dip=false
   this.gadge=false
   this.printe=false
   this.model=false
   var widt:any= document.getElementById('col') 
   widt.style.width='100%';
   widt.style.flex='1 0 auto'

  }
  
  )
  this.o.upload_display.subscribe(res=>{
    this.cloths=false
   this.packags=false
   this.dip=true
   this.gadge=false
   this.printe=false
   this.model=false
   var widt:any= document.getElementById('col') 
   widt.style.width='100%';
   widt.style.flex='1 0 auto'

  })
this.o.upload_gadget.subscribe(res=>{
  this.cloths=false
  this.packags=false
  this.dip=false
  this.gadge=true
  this.printe=false
  this.model=false
  var widt:any= document.getElementById('col') 
  widt.style.width='100%';
  widt.style.flex='1 0 auto'
})
this.o.upload_pack.subscribe(res=>{
  this.cloths=false
  this.iscloth=false
  this.packags=true
  this.dip=false
  this.gadge=false
  this.printe=false
  this.model=false
  var widt:any= document.getElementById('col') 
  widt.style.width='100%';
  widt.style.flex='1 0 auto';

})
this.o.upload_print.subscribe(res=>{
  this.cloths=false
  this.packags=false
  this.dip=false
  this.gadge=false
  this.printe=true
  this.model=false
  var widt:any= document.getElementById('col') 
  widt.style.width='100%' ;
  widt.style.flex='1 0 auto'

})

   this.o.detail.subscribe(res=>{
     var an:any=res;
     if(an.category=="vetements" &&  this.clothes.length>0){
       if(!this.issearch){
        this.isprint=false;
        this.isdisp=false;
        this.isgad=false;
        this.ispack=false;
       }
      


     }

     if(an.category=="emballages"){
       if(!this.issearch){
        this.isprint=false;
        this.isdisp=false;
        this.isgad=false;
       }
    }

    if(an.category=="affichages"){
      if(!this.issearch){
        this.isprint=false;
        this.isgad=false;
        this.iscloth=false;
        this.ispack=false
      }

    }

    if(an.category=="imprimes"){
      if(!this.issearch){
        this.isdisp=false;
        this.isgad=false;
        this.iscloth=false;
        this.ispack=false
      }

    }
    if(an.category=="gadgets"){
    
      if(!this.issearch){
        this.isprint=false;
        this.isdisp=false;
        this.iscloth=false;
        this.ispack=false
      }
     

    }


   })
   this.o.out_detail.subscribe(rs=>{
     if(rs){
       this.iscloth=true;
       this.isgad=true;
       this.ispack=true;
       this.isdisp=true;
       this.isprint=true
     }
   })

this.o.searchResult.subscribe(res=>{
  var r:any= res;
  if(r.length>0){
    this.isprint=false;
    this.isdisp=false;
    this.iscloth=false;
    this.ispack=false
    this.issearch=true
    this.isgad=false
  }
})
  
    if(this.Jour==5){
      this.hideprice=false
      this.hidepromo=true
    }

    this.subscription = interval(1000).subscribe(x => { 
      this.getTimeDifference(); 
    });

    this.l.getDesigns("vetements").subscribe(
      res=>{
        var data:any=res
        for(let item of data.data){
          item.urls= JSON.parse(item.urls)
          item.urls = decompress(item.urls)
          item.product_design_data=JSON.parse(item.product_design_data);
          item.product_design_data=decompress(item.product_design_data);
          item.design_title=JSON.parse(item.design_title)
          if(data.data.indexOf(item)<8){
          this.clothes.push(item)
          }
        }

        this.alldata=this.alldata.concat(this.clothes)
       
      },
      err=>{
        console.log(err)
      }
    )

    this.l.getDesigns("emballages").subscribe(
      res=>{
        var data:any=res
        for(let item of data.data){
          item.urls= JSON.parse(item.urls)
          item.urls = decompress(item.urls)
          item.product_design_data=JSON.parse(item.product_design_data);
          item.product_design_data=decompress(item.product_design_data);
          item.design_title=JSON.parse(item.design_title)

          if(data.data.indexOf(item)<8){
          this.packages.push(item)
          }
        }
        this.alldata=this.alldata.concat(this.packages)

       
      },
      err=>{
        console.log(err)
      }
    )
    this.l.getDesigns("affichages").subscribe(
      res=>{
        var data:any=res
        for(let item of data.data){
          item.urls= JSON.parse(item.urls)
          item.urls = decompress(item.urls)
          item.product_design_data=JSON.parse(item.product_design_data);
          item.product_design_data=decompress(item.product_design_data);
          item.design_title=JSON.parse(item.design_title)

          if(data.data.indexOf(item)<8){
          this.display.push(item)
          }
        }

        this.alldata=this.alldata.concat(this.display)

       
      },
      err=>{
        console.log(err)
      }
    )
    this.l.getDesigns("gadgets").subscribe(
      res=>{
        var data:any=res
        for(let item of data.data){
          item.urls= JSON.parse(item.urls)
          item.urls = decompress(item.urls)
          item.product_design_data=JSON.parse(item.product_design_data);
          item.product_design_data=decompress(item.product_design_data);
          item.design_title=JSON.parse(item.design_title)

          if(data.data.indexOf(item)<8){
          this.gadgets.push(item)
          }
        }

        this.alldata=this.alldata.concat(this.gadgets)
       
      },
      err=>{
        console.log(err)
      }
    )
    this.l.getDesigns("imprimes").subscribe(
      res=>{
        var data:any=res
        for(let item of data.data){
          item.urls= JSON.parse(item.urls)
          item.urls = decompress(item.urls)
          item.product_design_data=JSON.parse(item.product_design_data);
          item.product_design_data=decompress(item.product_design_data);
          item.design_title=JSON.parse(item.design_title)

          if(data.data.indexOf(item)<8){
          this.printed.push(item)
          }
        }
        this.alldata=this.alldata.concat(this.printed)
     //   console.log(this.alldata)

      },
      err=>{
        console.log(err)
      }
    )
    
    setTimeout(() => {
      this.o.onload.next(this.alldata);
    }, 1000);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  getTimeDifference () {
    if(this.Jour==5){
      this.timeDifference = this.dDay.getTime() - new  Date().getTime();
      this.allocateTimeUnits(this.timeDifference);
      this.cacheBulle=true
    }else{
      this.cacheBulle=false
    }

    
  }

  close(){
    this.show_menu=true
  }

  allocateTimeUnits (timeDifference:any) {
    this.secondsToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond) % this.SecondsInAMinute);
    this.minutesToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour) % this.SecondsInAMinute);
    this.hoursToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute) % this.hoursInADay);
    this.daysToDday = Math.floor((timeDifference) / (this.milliSecondsInASecond * this.minutesInAnHour * this.SecondsInAMinute * this.hoursInADay));
  }

  description(data:any){

    var a:any=document.getElementById("scroller");
     a.hidden=true;

    if(data.category_label=="vetements"){
      this.hide=true
      this.hidebody=false
      this.hidepack=false
      this.hideprint=false
      this.hidedisps=false
      this.hidegadgets=false
      this.hidecrea=false
      this.details=data;
    }
    if(data.category_label=="emballages"){
      this.hide=false
      this.hidebody=false
      this.hidepack=true
      this.hideprint=false
      this.hidedisps=false
      this.hidegadgets=false
      this.hidecrea=false
      this.details=data;
    }
    if(data.category_label=="imprimes"){
      this.hide=false
      this.hidebody=false
      this.hidepack=false
      this.hideprint=true
      this.hidedisps=false
      this.hidegadgets=false
      this.hidecrea=false
      this.details=data;
    }
    if(data.category_label=="affichages"){
      this.hide=false
      this.hidebody=false
      this.hidepack=false
      this.hideprint=false
      this.hidedisps=true
      this.hidegadgets=false
      this.hidecrea=false
      this.details=data;
    }
    if(data.category_label=="gadgets"){
      this.hide=false
      this.hidebody=false
      this.hidepack=false
      this.hideprint=false
      this.hidedisps=false
      this.hidecrea=false
      this.hidegadgets=true
      this.details=data;
    }

 
  }


  Upload(event:any){
    let file =event.target.files[0]
    if(!this.uplod.UpleadImage(file)){
    const reader = new FileReader();
  reader.onload = () => { 
   this.imagepreview = reader.result;
   this.CacheCrea=true;

   
   };
   
   reader.readAsDataURL(file);
   //this.affichecrea()
      }else{
      
    }
  }

  ChangeComponent(value:boolean){
    this.hidebody=value
    this.hide=false
    this.hidepack=false
    this.hideprint=false
    this.hidedisps=false
    this.hidegadgets=false
    
  }


  mymenu(){
    this.show_menu=!this.show_menu
    this.cloths=true
    this.packags=true
    this.dip=true
    this.gadge=true
    this.printe=true
    this.model=true
  }

  smooth(event:any){
    const node:any=document.getElementById('customize');
     scrollIntoView(node,
      { 
      behavior: 'smooth',
      scrollMode: 'if-needed', 
     
    })


  }
 
}
