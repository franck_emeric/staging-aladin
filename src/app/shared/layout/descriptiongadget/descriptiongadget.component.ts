import { Component, OnInit,Output,Input,EventEmitter,OnChanges,SimpleChanges } from '@angular/core';
import { compress } from 'compress-json';
import { AladinService, ListService, ObservableserviceService } from 'src/app/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-descriptiongadget',
  templateUrl: './descriptiongadget.component.html',
  styleUrls: ['./descriptiongadget.component.scss']
})
export class DescriptiongadgetComponent implements OnInit ,OnChanges{
@Input() data:any
@Output() changeComponent=new EventEmitter<boolean>();
hideprice:Boolean=true
hidepromo:Boolean=false
type_impr:any
serie=false
transf=false
uv=false
plast=false
metal=false
quantite=1;
matiere:any
chap=true;
detail:any=[];
total_cost=0.0;
shw=false
price=0.0
qty=0
@Input() list:any;

constructor(private aladin:AladinService,private O:ObservableserviceService,private api:ListService) { }

  ngOnInit(): void {
    this.api.getprinting("gadgets").subscribe(
      res=>{
        var data:any=res
        if(data.status){
          this.detail=data.data
        }
      }
    )
  }

  ngOnChanges(changes:SimpleChanges): void {
    this.data=changes.data.currentValue
    if(this.data.productype_label=="tasse"){
      this.quantite=1
      this.shw=true;
      this.total_cost=this.data.type_price
    }
    if(this.data.productype_label=="porte cle"){
     this.quantite=50
   }
   if(this.data.productype_label=="stylot"){
     this.quantite=50
   }
   
 }

 print_type(item:any){
   
  this.shw=true
  this.type_impr=item.printing_type_label
  this.price= item.printing_price
  if(this.quantite>0){
  this.total_cost= item.printing_price*this.quantite
  }else{
    this.total_cost= item.printing_price
  }

  this.total_cost=Number((this.total_cost).toFixed(2));

 }




go(event:any){
   Object.assign(this.data,{
     show:true,
     qty:this.quantite,
     t:this.total_cost,
     type:this.type_impr,
     matiere:this.matiere,
     price:this.price
  
 })
 if(this.data.productype_label.toLowerCase()==="tasse"){
  this.data.price=this.data.type_price
  this.data.t= this.data.price*this.quantite
}
if(this.data.qty && this.data.t && this.data.price){
  this.aladin.ShowEditor.next({data:this.data,list:this.list})

}else{
  Swal.fire({
    icon:"warning",
    text:"renseignez les caracteristiques du produit"
  })
}

 
}


plus(){
 if(this.data.productype_label=="tasse"){
   this.quantite=this.quantite+1
   this.qty=this.qty+1
   this.shw=true
   this.total_cost= this.quantite*this.data.type_price;
 }

 if(this.data.productype_label=="porte cle"){
   this.quantite=this.quantite+1
   this.qty=this.qty+1
    this.total_cost=  this.quantite*this.price;

   

 }


 if(this.data.productype_label=="stylot"){
   this.quantite=this.quantite+1
   this.qty=this.qty+1
   this.total_cost= this.quantite*this.price;

 }
 this.total_cost=Number((this.total_cost).toFixed(2));

}
moins(){

 if(this.data.productype_label=="tasse"){
   if(this.quantite>1){
     this.quantite=this.quantite - 1
     this.qty=this.qty-1
   }else{
     this.quantite=this.quantite
   }
   if(this.quantite>0){
    this.shw=true
    this.total_cost= this.quantite*this.data.type_price;

   }
 }
 if(this.data.productype_label=="porte cle"){
   if(this.quantite>50){
     this.quantite=this.quantite - 1
     this.qty=this.qty-1
   }

   if(this.quantite>0){
    this.total_cost= this.quantite*this.price;
   }

 }

 if(this.data.productype_label=="stylot"){
   if(this.quantite>50){
     this.quantite=this.quantite - 1
     this.qty=this.qty-1
   }
   if(this.quantite>0){
    this.total_cost= this.quantite*this.price;
   }

 }

 this.total_cost=Number((this.total_cost).toFixed(2));

}  


ChangeComponent(value:boolean){
  this.changeComponent.emit(value)

}

back(){
  this.O.out_detail.next({gadget:true})
}


}
