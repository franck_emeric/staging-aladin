import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptiongadgetComponent } from './descriptiongadget.component';

describe('DescriptiongadgetComponent', () => {
  let component: DescriptiongadgetComponent;
  let fixture: ComponentFixture<DescriptiongadgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescriptiongadgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptiongadgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
