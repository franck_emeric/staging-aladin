import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptiondispsComponent } from './descriptiondisps.component';

describe('DescriptiondispsComponent', () => {
  let component: DescriptiondispsComponent;
  let fixture: ComponentFixture<DescriptiondispsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescriptiondispsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptiondispsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
