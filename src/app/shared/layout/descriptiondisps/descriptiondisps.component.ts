import { Component, OnInit,Input,Output,EventEmitter, OnChanges,SimpleChanges } from '@angular/core';
import { compress } from 'compress-json';
import { AladinService, ListService, ObservableserviceService } from 'src/app/core';

@Component({
  selector: 'app-descriptiondisps',
  templateUrl: './descriptiondisps.component.html',
  styleUrls: ['./descriptiondisps.component.scss']
})
export class DescriptiondispsComponent implements OnInit,OnChanges {
  @Input() data:any;
  @Output() changeComponent=new EventEmitter<boolean>();
  Changevalue=true;
  hideprice:Boolean=true
  hidepromo:Boolean=false
  superieur:Boolean=false
  stanbard:Boolean=false
  quantite:any=1;
  shw=false
  total_cost=0.0
 price=0
  support=["Petit bas Stantard","Petit bas Superieur","Large bas Stantard","Large bas Superieur"];
  type_impr:any;
  hauteur:any;
  largeur:any;
  qty:any;
  surface:any;
  dimension:any;
  @Input() list:any;
  list_prnt:any=[]
   type=""
    constructor(private aladin:AladinService,private O:ObservableserviceService,private api:ListService) { }
  
    ngOnInit(): void {
     this.api.getprinting("affichages").subscribe(
       res=>{
         var dt:any=res;
         if(dt.status){
           this.list_prnt=dt.data
         }
       }
     )

    }

    back(){
      this.O.out_detail.next({disp:true})
    }

  ngOnChanges(changes:SimpleChanges): void {
    this.data=changes.data.currentValue
  }
   
    OnChange(event:any){
      var id = event.target.value;
      console.log(id)
      for(let item of this.list_prnt){
            if(id==item.printing_id){
               this.type_impr= item.printing_type_label
               this.total_cost= parseInt(this.quantite)*item.printing_price
               this.shw=true;
               this.type=item.productype_label;
               this.price=item.printing_price;
               break
            }
      }
    }

    go(event:any){
      Object.assign(this.data,{
        show:true,
        category:"display",
        type:this.type_impr,
        size:this.hauteur+"cm" +"/"+ this.largeur+"cm",
        qty: this.quantite,
        t:this.total_cost
     
    })
    console.log(this.total_cost,this.quantite)
    if(this.total_cost && this.quantite){

      this.aladin.ShowEditor.next({data:this.data,list:this.list})

    }

  }


  onINput(){
    if(this.price>0 && parseInt(this.quantite)){
      this.shw=true;
      if(this.surface){
        this.total_cost= Math.ceil(parseInt(this.quantite)*this.surface*this.price);
  
      }else{
        this.total_cost= this.price*parseInt(this.quantite);
      }
    }else{
      this.quantite=1
      this.total_cost= this.price
    }

    if(this.type==="kakemono" ){
      if(this.price>0 && parseInt(this.quantite)){
        this.total_cost= this.price*this.quantite;
        this.shw=true;
      }else{
        this.quantite=1
        this.total_cost= this.price
      }
  
    }

  }

  printng(item:any){
    this.shw=true
    this.price= item.printing_price;
    this.type_impr=item.printing_type_label
    if(this.surface){
      this.total_cost= Math.ceil(this.quantite*this.surface*this.price);

    }else{
      this.total_cost= item.printing_price*this.quantite;

    }
  }

  change1(event:any){
    if(this.largeur && this.quantite && this.hauteur){
      this.surface=Math.ceil(((this.largeur)/100)*((this.hauteur)/100));
      this.total_cost= Math.ceil(this.quantite*this.surface*this.price);
    }

  }
  change2(event:any){
    if(this.largeur && this.quantite && this.hauteur){
      this.surface=Math.ceil(((this.largeur)/100)*((this.hauteur)/100));
      this.total_cost= Math.ceil(this.quantite*this.surface*this.price);
    }
  }

}
