import { Component, OnInit,Input,OnChanges ,EventEmitter,Output,SimpleChanges} from '@angular/core';
import { compress } from 'compress-json';
import { AladinService, ListService, ObservableserviceService } from 'src/app/core';
declare  var require:any;
var myalert=require('sweetalert2');
@Component({
  selector: 'app-descclothes',
  templateUrl: './descclothes.component.html',
  styleUrls: ['./descclothes.component.scss']
})
export class DescclothesComponent implements OnInit,OnChanges {
@Input() data:any;
@Input() list :any;
@Output() changeComponent=new EventEmitter<boolean>();
Changevalue=true
chantp=true
QtM=0;
 QtS=0;
 QtL=0;
 QtXL=0;
 QtXXL=0;
 QtXXXL=0;
 QTY:any
 Qtotal:any=0
 typimpr=["Flexographie","Seriegraphie","Sublimation","Borderie","Transfert"]
 size=["xl","xxl","s","m","l","xxxl"]
 impr:any
 head=true
 hideprice:Boolean=true
 hidepromo:Boolean=false
jday= new Date().getDay()
printing:any
mode:any
total_cost=0.0
prod_cost=0.0;
print_cost=0.0
total=0.0
total_cost_xl=0.0
total_cost_s=0.0
total_cost_xxl=0.0
total_cost_l=0.0
total_cost_xxxl=0.0
total_cost_m=0.0
hastype=false;
shw=false
ngOnChanges(changes:SimpleChanges):void{
  this.data=changes.data.currentValue

}

constructor(private aladin:AladinService,private O:ObservableserviceService,private api:ListService) { }

  ngOnInit(): void {

   
    
    if(this.jday==5){
      this.hidepromo=true
      this.hideprice=false
    }
   
    
   setTimeout(()=>{
    this.api.getmode(this.data.productype_label).subscribe(
      res=>{
        var data= res;
        this.mode=data.data
      },
    err=>{
     console.log(err) 
    } 
    )
   },20)
    
    this.api.getprinting("vetements").subscribe(
      res=>{
      var data:any=res;
      this.printing =data.data

      },
     err=>{
      console.log(err) 
     } 
    )
     
  
  }

  back(){
    this.O.out_detail.next({cloth:true})
  }
  onchange(event:any){

    var id = event.target.value;
    for (let item of this.printing){
      if (item.printing_id==id){
        this.impr= item.printing_type_label;
        this.print_cost=  item.printing_price
        this.total_cost= (this.prod_cost+item.printing_price)*this.QtS+(this.prod_cost+item.printing_price)*this.QtM+(this.prod_cost+item.printing_price)*this.QtXL+(this.prod_cost+item.printing_price)*this.QtXXL+(this.prod_cost+item.printing_price)*this.QtL+(this.prod_cost+item.printing_price)*this.QtXXXL
        this.total_cost=Number((this.total_cost).toFixed(2));
        this.hastype=true;

        if(this.total_cost>0){
          this.shw=true;
    
        }
        break
      }
    }
  }

  go(event:any){
    this.QTY={
      ql:this.QtL,
      qs:this.QtS,
      qxl:this.QtXL,
      qxxl:this.QtXXL,
      qxxxl:this.QtXXXL,
      qm:this.QtM

    }
     this.Qtotal=this.QtM + this.QtL +this.QtS + this.QtXL + this.QtXXL+this.QtXXXL
  
     if(this.Qtotal>0 && this.impr){
    Object.assign(this.data,{
      show:true,
      type_product:"editor",
      categoty:"clothes",
      type_impr:this.impr,
      size_type:this.QTY,
      price:this.total_cost,
      t: this.total_cost,
      qty:this.Qtotal

    })
     
    this.aladin.ShowEditor.next({data:this.data,list:this.list})
  
  }else{
    myalert.fire({
      icon:'warning',
      html:
        
        '<p style="color:green">definissez les caracterique de votre design svp !!!</p> ' 
        ,
      showCloseButton: true,
      focusConfirm: false,
    
    })
  }

  }

  QtplusM(item:any){
    var item =item
    this.prod_cost=item.product_mode_price
    if(item.product_sizes.toLowerCase()=="xxl"){
      var span:any= document.getElementById(item.product_mode_id)
      this.QtXXL =this.QtXXL +1;
      span.innerText=this.QtXXL;
      this.total_cost_xxl=  (this.prod_cost+this.print_cost)*this.QtXXL
     
      
    }

    if(item.product_sizes.toLowerCase()=="xl"){
      var span:any= document.getElementById(item.product_mode_id)
      this.QtXL =this.QtXL +1;
      span.innerText=this.QtXL;
      this.total_cost_xl=((this.prod_cost+this.print_cost)*this.QtXL)
        
    }

    if(item.product_sizes.toLowerCase()=="m"){
      var span:any= document.getElementById(item.product_mode_id)
      this.QtM =this.QtM +1;
      span.innerText=this.QtM;
        this.total_cost_m= this.total_cost+ ((this.prod_cost+this.print_cost)*this.QtM)
        
    }

    if(item.product_sizes.toLowerCase()=="s"){
      var span:any= document.getElementById(item.product_mode_id)
      this.QtS =this.QtS +1;
      span.innerText=this.QtS;
        this.total_cost_s=((this.prod_cost+this.print_cost)*this.QtS)
       
    }

    if(item.product_sizes.toLowerCase()=="xxxl"){
      var span:any= document.getElementById(item.product_mode_id)
      this.QtXXXL =this.QtXXXL +1;
      span.innerText=this.QtXXXL;
        this.total_cost_xxxl= ((this.prod_cost+this.print_cost)*this.QtXXXL)
       
    }
    if(item.product_sizes.toLowerCase()=="l"){
      var span:any= document.getElementById(item.product_mode_id)
      this.QtL =this.QtL +1;
      span.innerText=this.QtL;
        this.total_cost_l=  ((this.prod_cost+this.print_cost)*this.QtL)
       
    }
    
    this.total_cost= this.total_cost_l+this.total_cost_m+this.total_cost_xxl+this.total_cost_s+this.total_cost_xxxl+this.total_cost_xl
    this.total_cost=Number((this.total_cost).toFixed(2));
    
    if(this.total_cost>0){
      this.shw=true
    }else{
      this.shw=false
    }
  }


  QtminusM(item:any){

    this.prod_cost=item.product_mode_price
    this.shw=true
    if(item.product_sizes.toLowerCase()=="xxl"){
      if(this.QtXXL>0){
        this.QtXXL =this.QtXXL-1 
        }else{
         this.QtXXL=+this.QtXXL
        
        }
        var span:any= document.getElementById(item.product_mode_id)
        span.innerText=this.QtXXL;
          this.total_cost_xxl=  ((this.prod_cost+this.print_cost)*this.QtXXL)
       
    }
   

    if(item.product_sizes.toLowerCase()=="xxxl"){
      if(this.QtXXXL>0){
        this.QtXXXL =this.QtXXXL-1 
        }else{
         this.QtXXXL=+this.QtXXXL
        
        }
        var span:any= document.getElementById(item.product_mode_id)
        span.innerText=this.QtXXXL;
          this.total_cost_xxxl=  ((this.prod_cost+this.print_cost)*this.QtXXXL)
        
    }
    if(item.product_sizes.toLowerCase()=="xl"){
      if(this.QtXL>0){
        this.QtXL =this.QtXL-1 
        }else{
         this.QtXL=+this.QtXL
        
        }
        var span:any= document.getElementById(item.product_mode_id)
        span.innerText=this.QtXL;
          this.total_cost_xl=  ((this.prod_cost+this.print_cost)*this.QtXL)
          
    }

    if(item.product_sizes.toLowerCase()=="l"){
      if(this.QtL>0){
        this.QtL =this.QtL-1 
        }else{
         this.QtL=+this.QtL
        
        }
        var span:any= document.getElementById(item.product_mode_id)
        span.innerText=this.QtL;
          this.total_cost_l=  ((this.prod_cost+this.print_cost)*this.QtL)
        
    }

    if(item.product_sizes.toLowerCase()=="s"){
      if(this.QtS>0){
        this.QtS =this.QtS-1 
        }else{
         this.QtS=+this.QtS
        
        }
        var span:any= document.getElementById(item.product_mode_id)
        span.innerText=this.QtS;
          this.total_cost_s= ((this.prod_cost+this.print_cost)*this.QtS)
         
           
    }

    if(item.product_sizes.toLowerCase()=="m"){
      if(this.QtM>0){
        this.QtM =this.QtM-1 
        }else{
         this.QtM=+this.QtM
        
        }
        var span:any= document.getElementById(item.product_mode_id)
        span.innerText=this.QtM;
        this.total_cost_m= ((this.prod_cost+this.print_cost)*this.QtM)
         

          
    }
   this.total_cost= this.total_cost_l+this.total_cost_m+this.total_cost_xxl+this.total_cost_s+this.total_cost_xxxl+this.total_cost_xl
   this.total_cost=Number((this.total_cost).toFixed(2));
   if(this.total_cost>0){
    this.shw=true
  }else{
    this.shw=false
  }

  }
 
 
}
