
import { Component, OnInit,Input,Output,EventEmitter ,OnChanges,SimpleChanges} from '@angular/core';
import { compress } from 'compress-json';
import { AladinService, ListService, ObservableserviceService } from 'src/app/core';
import Swal from 'sweetalert2';
declare var require: any
var $ = require("jquery");
var myalert=require('sweetalert2');
@Component({
  selector: 'app-descpackages',
  templateUrl: './descpackages.component.html',
  styleUrls: ['./descpackages.component.scss']
})
export class DescpackagesComponent implements OnInit ,OnChanges{
@Input() data:any;
@Input() list:any
@Output() changeComponent=new EventEmitter<boolean>();
@Output() showaladin=new EventEmitter<any>()
quantite=100;
chantp=true
size1=["20/30","30/30","33/40","40/45","50/60"];
size2=["15/20","20/25","25/30","28/35","35/45","45/50","50/55"];
cpt=1
head=true
size:any=null
hideprice:Boolean=true
hidepromo:Boolean=false
p_sizes:any=[]
jday= new Date().getDay()
total_cost=0.0
shw=false
price= 0.0;
Changevalue=true;
  constructor(private aladin:AladinService,private O:ObservableserviceService,private api:ListService) { 

  }
  ngOnInit(): void {
    if(this.jday==5){
      this.hidepromo=true
      this.hideprice=false
    }
    this.api.getFormat("emballages").subscribe(
      res=>{
        var data:any=res
        console.log(data)
        if(data.status){
          for(let item of data.data){
            if(item.productype_label=="sac"){
              this.p_sizes.push(item)
            }
          }
         
        }
      }
    )

  }
ngOnChanges(changes:SimpleChanges) : void{
  this.data=changes.data.currentValue
  this.data.productype_label=this.data.productype_label.toLowerCase()
 
}

back(){
  this.O.out_detail.next({pack:true})
}

go(event:any){
  Object.assign(this.data,{
    show:true,
    category:"packs",
    qty:this.quantite,
    t: this.total_cost,
    size:this.size,
    price:this.price
  })


  if(this.data.qty>0 && this.data.size && this.data.price>0 && this.data.t>0){
    this.aladin.ShowEditor.next({data:this.data,list:this.list})

  }else if(this.data.qtys==0 || this.data.size==null || this.data.price==0){
    Swal.fire({
      icon:"warning",
      text:"renseigner les caracteristiques du produit"
    })
  }

}

plusqty(){
this.quantite=this.quantite + 100;
if(this.price!=0.0){
  this.total_cost= (this.quantite/100)*this.price
  this.total_cost=Number((this.total_cost).toFixed(2));
  
}
}

minusqty(){
if(this.quantite>100){
  this.quantite=this.quantite - 100
}else{
  this.quantite=this.quantite
}
if(this.price!=0.0){
  this.total_cost= (this.quantite/100)*this.price
  this.total_cost=Number((this.total_cost).toFixed(2));
  
}

}

onchange(id:any){
  var id= id.target.value
  for (let item of this.p_sizes){
    if(item.product_format_id==id){
      this.size=item.product_width+"/"+item.product_height;
      this.price= item.product_price_format;
      this.shw=true
      this.total_cost= (this.quantite/100)*this.price
      this.total_cost=Number((this.total_cost).toFixed(2));
      break;

    }
  }
 
}

onchange2(id:any){
  var id= id.target.value
  console.log(id)
  for (let item of this.p_sizes){
    if(item.product_format_id==id){
      this.size=item.product_width+"/"+item.product_height;
      this.price= item.product_price_format;
      this.shw=true
      this.total_cost= (this.quantite/100)*this.price
      this.total_cost=Number((this.total_cost).toFixed(2));
      break;

    }
  }
     }

}

