import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';
import { ObservableserviceService } from 'src/app/core';
 declare var require:any;
 var scrollToElement = require('scroll-to-element');
@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {
@Output() ChangeBackground = new EventEmitter<any>()
@Input() list_of_prod:any;
@Input()editor:any
cloth=false;
pack= false;
gadget=false;
disp=false
print=false
data:any
default=true
list:any=[];
list_for_editor=false;

constructor(private O:ObservableserviceService) { }

  ngOnInit(): void {
    if(this.editor==true){
     this.default=false
    this.list_for_editor=true
    }
   this.O.out_detail.subscribe(res=>{
     var ans= res
     if(ans.print){
      this.default=true
      this.print=false
     }
     if(ans.disp){
       this.default=true;
       this.disp=false
     }

     if(ans.gadget){
       this.default=true;
       this.gadget=false

     }

     if(ans.cloth){
       this.default=true;
       this.cloth=false
     }

     if(ans.pack){
       this.default=true;
       this.pack=false
     }

   })
   

  }

  showDetails(item:any){
    this.data=item;
    var l=[]
    for(let i of this.list_of_prod){
      if(item.productype_label===i.productype_label){
        console.log(i)
        l.push(i)
      }
    }
    this.list=JSON.stringify(l)
    this.O.detail.next({category:item.category_label});

    if(item.category_label==="vetements"){    
      this.cloth=true;
      this.default=false
    }

    if(item.category_label==="gadgets"){
      this.gadget=true;
      this.default=false

      
    }


    if(item.category_label==="imprimes"){
      this.default=false;
      this.print=true;

    }
    if(item.category_label==="affichages"){
      this.default=false;
      this.disp=true
      
    }
    
    if(item.category_label==="emballages"){
      this.pack=true;
      this.default=false
    }
   
  }
 
  changeBack(item:any){
    this.ChangeBackground.emit(item)
  }
  

scrollTo(event:any){
  var el:any=event.target
  scrollToElement(el);
}


}
