import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
import { charAtIndex } from 'pdf-lib';
import { LocalService,CartService, ObservableserviceService} from 'src/app/core';
import Swal from 'sweetalert2';
import { compress,decompress } from 'compress-json';

@Component({
  selector: 'app-cartitems',
  templateUrl: './cartitems.component.html',
  styleUrls: ['./cartitems.component.scss']
})
export class CartitemsComponent implements OnInit {
  cart_items:any;
  sub_total=0;
  id:any;
  isempty=true;
  diliver=0
  update=true;
  inpnmb=0;
  unit_price=1;
  price=0;
  editor_items:any=[];  
  crea_pack_items:any=[];
  crea_cloth_item:any=[];
  crea_disp_items:any=[];
  crea_print_items:any=[];
  crea_gadget_items:any=[]
  @Input() showbtn=true;
  cart:any=[]
 @Output() mymoney=new EventEmitter<any>()
 
  lot:any=[]
  constructor(private item:LocalService,private cartInfo:CartService,private O:ObservableserviceService) { }
  ngOnInit(): void {

    this.cart_items= this.item.cart_items;
     console.log(this.cart_items)
    if(this.cart_items.length>0){
     this.isempty=!this.isempty;
    
     for(let item of this.cart_items){
       item= decompress(item)
       this.cart.push(item)
       this.sub_total=this.sub_total+(parseInt(item.t));

 
       if(item.type_product=="crea" && item.category=="vetement"){  
         this.crea_cloth_item.push(item);
         console.log( this.crea_cloth_item)
     
       }
     
         if(item.type_product=="editor"){
           
           this.editor_items.push(item);
          
         }
         if(item.type_product=="crea" && item.category=="emballage"){
           
           this.crea_pack_items.push(item);
           let lot=(item.qty/100)
           this.lot.push(lot);
           
         }
         if(item.type_product=="crea" && item.category=="gadget"){
          this.crea_gadget_items.push(item)
        }
        if(item.type_product=="crea" && item.category=="imprimer"){
    

          this.crea_print_items.push(item)
        }
        if(item.type_product=="crea" && item.category=="affichage"){
    
          this.crea_disp_items.push(item)
        }
        }
 
    }

    

  }

  homepage(){
    this.O.cart.next(true)
    location.href="/"
  }


  modal(event:any){
    Swal.fire({
      title: 'Etes-vous sure?',
      text: "De vouloir supprimer cet élément?",
      icon: 'warning',
      showCancelButton:true,
      
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui',
    
    }).then((result) => {
     
      if (result.isConfirmed) {
        console.log(event)

        this.removeItem(event)
        Swal.fire(
          'Supprimé!',
          'Votre élément a bien été supprimé.',
          'success'
        )
      }else{
         console.log(result)
      }
    })
  }


  removeItem(event:any){
  
    console.log(event)
    let id= event.target.id;
    console.log(id)
    let item:any;
    let id_cart:any= charAtIndex(id,0)
    let id_origin:any=charAtIndex(id,2);
    let category:any=id.slice(4)
     
    if(id_cart[0]!="-1"){
      item=this.cart[+id_cart[0]]

      this.sub_total = this.sub_total - parseInt(item.t)

      this.item.removeItem(+id_cart[0]);

      this.mymoney.emit(this.sub_total);

      if(category=="emballage"){
        let element:any= id_origin[0]
        if(element!="-1"){
          element= parseInt(element)
         if(this.crea_pack_items.splice(element,1)){
          console.log(this.crea_pack_items)
         }

        }

      }

      if(category=="vetement"){
        let element:any= id_origin[0]
    
        if(element!="-1"){
          element= parseInt(element)
          this.crea_cloth_item.splice(+element,1)
        }
      }

      if(category=="affichage"){

        let element:any= id_origin[0]
    
       
        if(element!="-1"){
          element= parseInt(element)
          this.crea_disp_items.splice(+element,1)
         
        }
      }
      if(category=="gadget"){
        let element:any= id_origin[0]
        
        if(element!="-1"){
          element= parseInt(element)
          this.crea_gadget_items.splice(+element,1)
        }
      }
      if(category=="imprimer"){
        let element:any= id_origin[0]
   
        if(element!="-1"){
          element= parseInt(element)
          this.crea_print_items.splice(+element,1)
        }
      }

      if(category=="editor"){
        let element:any= id_origin[0]   
        if(element!="-1"){
          this.editor_items.splice(+element,1)
        }
      }
    }
  
    if(this.cart_items.length==0){
      this.isempty=!this.isempty;

    }
  }


  emitMoney(value:any){
    this.mymoney.emit(value)
  }


  finale_step(event:any){
    location.href="/users/login"
  }



}
