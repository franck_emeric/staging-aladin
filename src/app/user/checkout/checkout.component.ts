import { Component, OnInit,Input,Output,EventEmitter,OnChanges,AfterContentInit } from '@angular/core';
import {ListService, ObservableserviceService} from 'src/app/core';
import {  ActivatedRoute } from '@angular/router';
import { TokenService } from 'src/app/core/auth/tokens/token.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit,OnChanges,AfterContentInit {
 @Input() data:any;
 @Input() isauth:any;
 @Output() neworder=new EventEmitter<any>();

 total:any;
 city:any;
 address:any;
 date:any;
 today:any;
 hidebtn=false;
 t:any
 errCode=false
 code:any=" "
 hasCode=false
message:any=" " 
 tab=["Abidjan","Bassam","Bingerville","Dabou","Jacqueville","Autre"]
 ntabv=false
 choice=true
 price_zone={
   abjd: 0, //1000,
   other:0//2000
 }
 isother=false
 delivery_cost:any=0;
 cpt:number=0;
 hasdeliverycost:boolean=false;
 order_data:any;
 payment=false
 hasOm=false;
 hasmn = false
 discouted:any
 adr:any=[]
 selected=false
 address_name:any
 dlver:any
 errinput=false
 code_expire=false
 not_available=""
 ntab=false
 options = {weekday: "long", year: "numeric", month: "long", day: "numeric",hour:"2-digit",minute:"2-digit",second:"2-digit"};
  constructor(private item:ListService,private activeroute:ActivatedRoute,private tokenService:TokenService,private o:ObservableserviceService) { }

  ngOnInit(): void {  
  
 
   
    this.tokenService.getUser().then(res=>{
      this.data=res
      if(this.data.code_status==0){
        this.code_expire=true
      }
    })
   this.item.getAdress().subscribe(
  res=>{
    var resp:any=res;
    if(resp.status){
      this.adr=resp.data;
    }
  })


  }

  ngAfterContentInit(){
    setTimeout(()=>{
      let m:any=document.querySelector('#money');
      this.item.triggerMouse(m);
    },1000);
  }

  ngOnChanges(){
   
  
  }


  HasCode(){
   this.hasCode=!this.hasCode 
  }


  getTotal(value:any){
    this.total=value
    this.t=this.total;

  }

visa(){
  this.ntab=true
  this.message="Service non disponible"
}


moov(){
  this.ntabv=true
  this.message="Service non disponible"
}
  ChangeHandler(event:any){
    this.hasdeliverycost=true;
    this.total=parseInt(this.total)
    var id= event.target.value
    for(let item of this.adr){
      if(item.aladin_dlivery_adress_id==id){
       this.delivery_cost=item.delivery_cost
       this.address_name=item.delivery_city
       this.selected=true;
       this.city=this.address_name;
       this.dlver=id;
       if(this.address_name=="autres"){
         this.isother=true
       }else{
        this.isother=false

       }
        break
      }else{
        this.selected=false
      }
    }

   
  }

  oncode(){
    if(JSON.stringify(this.code.trim().toLowerCase()) == JSON.stringify(this.data.code.toLowerCase())){
      var t:any= parseInt(this.total)
      this.discouted= parseInt(t)-(parseInt(t)*40/100)
      this.message="code valide vous béneficiez de 40% de reduction sur votre achat du jour"
      this.errCode=true;
   }else
   {
     if(this.code!=" "){
      this.message=" code invalide"
      this.errCode=true
     }else{
       this.errCode=false
     }
     
    
   }
  }
 
  
  followp(){


    if(JSON.stringify(this.code.trim().toLowerCase()) == JSON.stringify(this.data.code.toLowerCase())){
      var t:any= parseInt(this.total)
      this.discouted= parseInt(t)-(parseInt(t)*40/100)
   }else
   {
    
      this.message=" code invalide"
      this.errCode=true
    
   }
     
    if(this.address && this.city){
      this.order_data={
          user:this.data,
          city:this.city,
          address:this.address,
          total:this.total,
          discount:this.discouted,
          addres_id:this.dlver,
          delivery:parseInt(this.delivery_cost)
      }

      if(this.hasOm){
        this.order_data.pmode="orange"
        this.o.om.next(true)
      }
      if(this.hasmn)
      {
        this.order_data.pmode="mtn"
        this.o.mtn.next(true)
      }
    console.log(this.order_data)
    document.getElementById('checkout')?.setAttribute('hidden',"true");
     this.payment=true

    }else
    {
     if(this.address==undefined || this.city==undefined)
     {
      this.message= "saisissez les addresses de livraison svp !!"
      this.errinput=true
      this.errCode=false
      this.ntab=false;
     this.ntabv=false;
     } 
    }

  }

momo(){
  this.choice=false;
  this.hasmn=true
  this.hasOm=false

}

om()
{
this.choice=false
this.hasOm=true
this.hasmn=false

}

 
  toggle(){
    this.errCode=false
    this.errinput=false
    this.ntab=false;
    this.ntabv=false;
  } 


  backcheck(){
    if(this.payment){
      let check=document.getElementById('checkout')
      if(check)
       check.hidden=!check?.hidden
       this.payment=!this.payment;
       this.choice=!this.choice
       this.hasOm=false
       this.hasmn=false
    }
  }




}
