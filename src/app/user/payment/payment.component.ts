import { Component, OnInit,Input ,OnChanges,EventEmitter,Output} from '@angular/core';
import { compress, decompress } from 'compress-json';
import { timeout } from 'rxjs/operators';
import { LoginService,ListService,LocalService, ObservableserviceService } from 'src/app/core';
import { TokenService } from 'src/app/core/auth/tokens/token.service';
import Swal from 'sweetalert2';
declare var require:any
var $ = require("jquery");
var myalert=require('sweetalert2')

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

constructor(private o:ObservableserviceService,private auth:LoginService,private l:ListService,private cart:LocalService,private tokenService:TokenService) { }
auth_data:any;
code=225;
phone:any;

paystatus=false;
status=false;
show_success=false;
order_id:any;
cart_items:any
@Input() data:any;
@Output() newbackcheck=new EventEmitter<any>()
timer:any;
amount:any=0
today:any
date:any;
vendors:any=[]
shwSpinner=true
hastoken=false
isom:any;
options = {weekday: "long", year: "numeric", month: "long", day: "numeric",hour:"2-digit",minute:"2-digit",second:"2-digit"};

  ngOnInit(): void { 

    this.today= new Date(new Date().getTime()+(5*24*60*60*1000))
    this.today= this.today.toLocaleString("fr",{weekday: "long", year: "numeric", month: "long", day: "2-digit"});
  


    setInterval(()=>{
      this.date=new Date();
      this.date= this.date.toLocaleString("fr",this.options);
      if(this.order_id&&this.hastoken){
        this.shwSpinner=false;

      }else{
        this.shwSpinner=true;

      }
    },1000);

    if(this.data.discount!=undefined){
      this.amount= this.data.delivery+parseInt(this.data.discount)
    }else{
      this.amount=this.data.delivery + parseInt(this.data.total)
    }
    this.cart_items=this.cart.cart_items

    this.saveOrder()

    if(this.data.pmode=="orange"){
      this.auth.getToken(this.data.pmode).subscribe(
        res=>{
          var resp:any=res
          if(resp.status){
            resp.data=JSON.parse(resp.data)
          this.tokenService.save_pay_token(resp.data.access_token)
          console.log(resp.data)
          this.hastoken=true
          this.isom=true
          }
        },
        error=>{
          console.log(error)
          location.reload()
        }
      )
    }

    if(this.data.pmode=="mtn"){
      this.auth.getToken().subscribe(
        res=>{
          var resp:any=res
          if(resp.code==200){
          this.tokenService.save_pay_token(resp.token.access_token)
          this.hastoken=true
          console.log(resp)
          this.isom=false
          }
        },
        error=>{
          console.log(error)
          location.reload()

        }
      )
    }

   }

ClearTimer(){
  if(this.status){
    clearInterval(this.timer)
  } 
}




om(){
 this.auth.pay({amount:this.amount,order:JSON.stringify(this.order_id)},"orange").subscribe(res=>{
  var resp:any=res
  resp.data=JSON.parse(resp.data)
  this.tokenService.save_pay_token(resp.data.pay_token) 
   location.href=resp.data.payment_url    
},
err=>{
  console.log(err)
  location.reload()
}
)

}


momo(){
  if(this.phone && this.phone.length==10 && this.amount){
    let data={
       amount:JSON.stringify(this.amount),
       phone:this.code+this.phone,   
      }

      if(this.paystatus){
        this.paystatus=!this.paystatus
      }

      this.toggle()

      this.auth.pay(data).subscribe(
        res=>{
          if(res.code==202){
            this.timer= setInterval(()=>{
              this.getpaymentstatus(res.ref); 
               
            },10000)   
          }else{
            this.toggle()
            myalert.fire({
              title: "echec de transaction",
              text: "La transaction a echoué verifiez votre solde.",
              icon: "error",
               button: "Ok"
              })
            console.log(res.code)
          }
        },
        err=>{
          this.toggle()
           location.reload()
        }
      )
    }
}

toggle(){
  this.paystatus=!this.paystatus
}

getpaymentstatus(ref:any){ 
    this.auth.getpaymentStatus(ref).subscribe(res=>{
      if(res.code==200){
        if(res.data.status=="SUCCESSFUL"){
          this.status=true
          var ok = document.getElementById("ok")
          this.l.triggerMouse(ok)
          this.l.setCode({user:this.data.user.user_id}).subscribe(
            res=>{
              //console.log(res)
              this.show_success=true
              this.tokenService.signOut()
              window.localStorage.removeItem('cart')
            },
            er=>{
              console.log(er)
            }
          )

        }
        if(res.data.status=="FAILED"){
          this.status=true
          var ok = document.getElementById("ok")
          this.l.triggerMouse(ok)
          this.toggle()
          myalert.fire({
            title: "echec de transaction",
            text: "La transaction a echoué verifiez votre solde.",
            icon: "error",
             button: "Ok"
            })
        }
      }
    },err=>{
      myalert.fire({
        text: "La transaction a echoué verifiez votre solde.",
        icon: "warning",
         button: "Ok"
        })
      this.toggle()

    })

  
}



saveOrder(){
 let data={
  user:this.data.user.user_id,
  address:parseInt(this.data.addres_id),
  area:JSON.stringify(this.data.address),
  total:this.amount,
  data:""
 }
 var cart:any=[]

 for(let item of this.cart_items){
   item=decompress(item)
   cart.push({design:item.design,urls:compress(item.urls),obj:compress(item.obj),desc:compress({size:item.size,type:item.type,size_type:item.size_type}),qty:item.qty,total:item.t})
   this.vendors.push({email:item.vendor_email,product:item.design})

 }

data.data=JSON.stringify(compress(cart))

this.l.saveOrder(data).subscribe(
   res=>{
     var resp:any=res
     if(resp.status){
      this.order_id=resp.data.order
   
     }
   },
   err=>{
     console.log(err)
   }
 )


}



backcheck(){
this.newbackcheck.emit();
}


}
