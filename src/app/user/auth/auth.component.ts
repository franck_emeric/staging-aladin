import { Component, OnInit,OnChanges } from '@angular/core';
import { TokenService } from 'src/app/core/auth/tokens/token.service';
import { LoginService } from '../../core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit,OnChanges {
password:any=""
email:any=""
message={
  email:"",
  password:""
}
epwd=false
maile=false
show=false
isauth=false;
userdata:any;
payment=false;
prog=false;
loginform=true
auth_data:any

  constructor(private loginservice:LoginService,private tokenService:TokenService) { }
ngOnChanges(){
  
}
  ngOnInit(): void {
   
  }

toggle(){
  this.show=!this.show;
}
  login(){
    if(this.password && this.email){
      if(this.show){
        this.toggle();
      }
      this.toggle()
      this.loginservice.login({email:this.email,password:this.password}).subscribe(
        res=>{
          this.toggle()
          var resp :any= res
          if(resp.status){
          var data:any={
              user_id:resp.data.user_id,
              email:resp.data.email,
              first_name:resp.data.first_name||" ",
              last_name:resp.data.last_name||" ",
              code:resp.data.code,
              code_status:resp.data.code_status

          }
          try{
            this.tokenService.saveUser(data)
            this.tokenService.saveRefreshToken(resp.data.Refresh_Token)
            this.tokenService.saveToken(resp.data.Access_Token)
            
          }catch(err){
            console.log(err)
          }

       }
        },
        err=>{
          this.toggle();
          console.log(err);
          if(err.error.text!=undefined){
           this.maile=!this.maile;
           this.message.email=err.error.text;   
          }
          if(err.error.password!=undefined){
            this.epwd=!this.epwd
            this.message.password=err.error.password;   
          }  
        }
      )
    }
  }


  

 


}


