import { Component, Input, OnInit } from '@angular/core';
import { RegisterService } from '../../core';
import { AuthinfoService } from 'src/app/core/storage';
import { TokenService } from 'src/app/core/auth/tokens/token.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
info="Bienvenue chez ALADIN";
data={
  fname:"",
  lname:"",
  email:"",
  phone:"",
  password:"",
  cpwd:"",
  is_partner:0,
  city:"",
  whatapp:""

}
user_sex=[{id:0,value:"Sex"},{id:1,value:"Homme"},{id:2,value:"Femme"}]
sex:any=1
errmsg={
  password:false,
  email:false,
  phone:false,
  infoemail:"",
  infophone:"",
  infopwd:""
}
auth_data:any;
show:boolean=false;
ishttpLoaded:boolean=false;
isLoaded:boolean=false;

  constructor(private tokenService:TokenService,private registerservice: RegisterService,private l:AuthinfoService) { }

  ngOnInit(): void {
    
  }

  toggleSpinner(){
    this.show = !this.show;

  }

  register(event:Event){
    if(this.data.fname!=null&&this.data.lname!=null&&this.data.phone!=null&&this.data.email){
      if(this.matchpwd(this.data.password,this.data.cpwd)==false){
        this.errmsg.password=!this.errmsg.password;
        this.errmsg.infopwd="passwords do not macth";
      }else{

        if(this.show){
          this.toggleSpinner();
        }

        this.toggleSpinner();
        this.registerservice.saveUser({user_first_name:this.data.fname,user_last_name: this.data.lname,user_email:this.data.email,user_phone:this.data.phone,user_password:this.data.password,usersex_id:this.sex}).subscribe(res=>{
         var resp:any= res
         if(resp.status){
          var data:any={
            user_id:resp.data.user_id,
            email:resp.data.email,
            first_name:resp.data.first_name,
            last_name:resp.data.last_name,
            code:resp.data.code,
            code_status:resp.data.code_status,
            user_phone:resp.data.phone


        }
        try{
          this.tokenService.saveUser(data)
          this.tokenService.saveRefreshToken(resp.data.Refresh_Token)
          this.tokenService.saveToken(resp.data.Access_Token)
          location.href="/"
          
        }catch(err){
          console.log(err)
        }

         }
        },
        err=>{
          this.toggleSpinner();
            this.errmsg.phone=!this.errmsg.phone;
            this.errmsg.infophone="telephone ou email exist déjà";
        }); 
      
    } 
} 
}

userSEX(event:any){

  if(event.target.value!=0)
  {
    this.sex= event.target.value

  }
  console.log(this.sex)

}


matchpwd(pwd:string,cpwd:string):boolean{
    if(pwd===cpwd){
      return true;
    }else{
      return false;
    }
}

}
