export const environment = {
 
  production: true,
  baseApi:"https://aladindesigns.aladin.ci/",
  apiBaseUrl:"http://localhost:3300/api/v1/",   
  //test:"https://api.aladin.ci/",
  //testgil:"http://localhost:5000/api/v1",
  baseUrlDev:"http://localhost:3400/v1/",
  baseImagePath:"http://localhost:3300/",
  //apiBaseUrl:"https://staging-api.aladin.ci/v1/",
  //baseImagePath:"https://staging-api.aladin.ci/",
  baseInvite:"https://staging-api.aladin.ci/",
  //baseUrlDev:"https://staging-api.aladin.ci/v1/"
};
